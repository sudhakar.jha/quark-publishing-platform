/*********************************************************************
Quark Publishing 
 
©2018 Quark Software Inc. All rights reserved.

Script Name:
	CheckOutAsset
Objective : 
	The script will mark the given asset as checked out.
Prerequisite: 
	- Variable "assetId" should refer to the QPP Server's Asset to be checked out. 
	- Varibale "targetFolder" should refer to the location where the asset highres shall be copied when required. 
	  Target folder should have write permissions.
**************************************************************************/

//Import the required pacakages
importPackage(Packages.java.lang);
importPackage(Packages.java.io);
importPackage(Packages.com.quark.qpp.core.asset.service.constants);

//load QppFunctions Script for helper methods
load("QppFunctions");

//Specify name of the QPP Server asset that is to be checked out
var assetId = 10308;

//Specify the folder where the asset is to be copied on checkout.
var targetFolder = "D:/target";

//Get reference of QPP Server's Asset serviceApi.
var assetServiceApi = runtimeBeanManager.lookupBean("assetServiceApi")

var stringBuffer = new StringBuffer();
try{
	
	var assetName = getAssetNameFromId(assetId);
	
	//declare variables map
	var attributeValuesMap = {"Checked out file path":targetFolder+"/"+assetName, "Checkedout Application":"QPP SCRIPT MANAGER"};
 
	var checkoutAttributeValuesArray = createAttributeValueArray(attributeValuesMap);
	
	//checkout the asset
	assetServiceApi.checkOut(assetId, checkoutAttributeValuesArray);
	
	stringBuffer.append("Asset : "+assetName+" successfully marked as checked out.");
}catch(exception){
	stringBuffer.append("Error : "+assetName+" checkout failed.\n");
	stringBuffer.append(exception);
}
stringBuffer.toString();
