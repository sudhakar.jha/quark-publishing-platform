/*******************************************************************************
** Quark Publishing 
** 
** ©2018 Quark Software Inc. All rights reserved. 

The script contains common helper/utility methods pertaining to QPP Server 
that can be used in other javascripts. 
********************************************************************************/

//Import the required packages
//importPackage(Packages.com.quark.qpp.service.xmlBinding);
importPackage(Packages.com.quark.qpp.core.query.service.constants);
importPackage(Packages.com.quark.qpp.core.attribute.service.dto)
importPackage(Packages.com.quark.qpp.common.dto)
importPackage(Packages.com.quark.qpp.core.attribute.service.constants)

//Get reference of AttributeServiceApi
var attributeServiceApi = runtimeBeanManager.lookupBean("attributeServiceApi");

//Get reference of AssetServiceApi
var assetServiceApi = runtimeBeanManager.lookupBean("assetServiceApi");

//Get reference of AttributeDomainServiceApi
var attributeDomainServiceApi = runtimeBeanManager.lookupBean("attributeDomainServiceApi");

//Get reference of WorkflowServiceApi
var workflowServiceApi = runtimeBeanManager.lookupBean("workflowServiceApi");

//Get reference of CollecitionServiceApi
var collectionService = runtimeBeanManager.lookupBean("collectionService");

//var sb = new StringBuffer();
/** Get the file extension from the filename.
filename : Name of the file.
**/
function getFileExtension(filename){
 	var index = filename.lastIndexOf('.');
	var extension = "";
	if(index != -1){
		extension = filename.substring(index + 1);
		info(" "+extension+" "+filename );
		return extension;
	}
	return extension;
 }

function getFileExtensionFromMimeType(filename){
 	var index = filename.lastIndexOf('/');
	var extension = "";
	if(index != -1){
		extension = filename.substring(index + 1);
		info(" "+extension+" "+filename );
		return extension;
	}
	return extension;
 }


/**
Returns com.quark.qpp.core.attribute.service.dto.AttributeValue Array for the given two dimensional array of type [object Object].
attributeValuesMap : two dimensional array of type [object Object] 
example : var map ={"name":"sample.jpg","workflow":"abc",...}
**/

function createAttributeValueArray(attributeValuesMap){
	attributeValuesMap = convertKeysToUpperCase(attributeValuesMap);
	var attributeValues = [];
	var count = 0;
	var workflowName = attributeValuesMap["WORKFLOW"];
	var collectionPath = attributeValuesMap["COLLECTION PATH"];
	for (var key in attributeValuesMap) {
		var attribute = attributeServiceApi.getAttributeByName(key);
		
		var attributeValue = new AttributeValue();
		attributeValue.setAttributeId(attribute.getId());
		attributeValue.setType(attribute.getValueType());
		var isMultiValued = attribute.isMultiValued();
		attributeValue.setMultiValued(attribute.isMultiValued().booleanValue());
		var valueType = attribute.getValueType();
		switch(valueType.intValue()) {
  			case AttributeValueTypes.TEXT:
			{
   		 		var textValue = new TextValue();
				textValue.setValue(attributeValuesMap[key]);
				attributeValue.setAttributeValue(textValue);
   		 		break;
  			}
			case AttributeValueTypes.DATE:
			{
    			var dateValue = new DateValue();
				dateValue.setValue(attributeValuesMap[key]);
				attributeValue.setAttributeValue(dateValue);
   			 	break;
			}
			case AttributeValueTypes.TIME:
			{
    			var timeValue = new TimeValue();
				timeValue.setValue(attributeValuesMap[key]);
				attributeValue.setAttributeValue(timeValue);
   			 	break;
			}
			case AttributeValueTypes.NUMERIC:
			{
				var numericValue = new NumericValue();
				numericValue.setValue(attributeValuesMap[key]);
				attributeValue.setAttributeValue(numericValue);
   			 	break;
			}
			case AttributeValueTypes.MEASUREMENT:
			{
   		 		var measurementValue = new MeasurementValue();
				measurementValue.setValue(attributeValuesMap[key]);
				attributeValue.setAttributeValue(measurementValue);
   		 		break;
  			}
			case AttributeValueTypes.BOOLEAN:
			{
    			var booleanValue = new BooleanValue();
				booleanValue.setValue(attributeValuesMap[key]);
				attributeValue.setAttributeValue(booleanValue);
   			 	break;
			}
			case AttributeValueTypes.DOMAIN:
			{
				var attrPrefs = null;
				if(attribute.isMultiValued().booleanValue()){
					attrPrefs = attribute.getDomainValueListPreferences();
				} else {
					attrPrefs = attribute.getDomainValuePreferences();
				}
				var domain = attributeDomainServiceApi.getDomain(attrPrefs.getDomainId());
				var domainValue = new DomainValue();
				if(key.equals("STATUS")){
					info("status inside  ");
					if(workflowName == null){
						error("Workflow not given for status");
					} else{
						domainValue = workflowServiceApi.getStatusByName(workflowName,attributeValuesMap[key]);
					}
				} else if(key.equals("JOB JACKET")){
					if(collectionPath == null){
						error("collection not provided for job jacket");
					} else {
						var collection = attributeDomainServiceApi.getDomainValueFromHierarchy(domain.getId(),collectionPath);
						var jobJackets = collectionService.getCollectionJobJackets(collection.getId());
						for (var i = 0; i < jobJackets.length ; i++) {
    							if(jobJackets[i].getName().toUpperCase().equals(attributeValuesMap[key].toUpperCase())){
								domainValue = jobJacket[i];
								break;
							}	
						}
					}
				} 
				else {
					domainValue = attributeDomainServiceApi.getDomainValueFromHierarchy(domain.getId(),attributeValuesMap[key]);
				}
				
				domainValue.setDomainId(domain.getId());
				if(!attribute.isMultiValued().booleanValue()){
					attributeValue.setAttributeValue(domainValue);
				} else {
					var domainValueList = new DomainValueList();
					var domainValues = [];
					domainValues[0] = domainValue;
					domainValueList.setDomainValues(domainValues);
					attributeValue.setAttributeValue(domainValueList);
				}
   			 	break;
			}
			case AttributeValueTypes.DATETIME:
    			{	var dateTimeValue = new DateTimeValue();
				dateTimeValue.setValue(attributeValuesMap[key]);
				attributeValue.setAttributeValue(dateTimeValue);
   			 	break;
			}
		}
		attributeValues[count] = attributeValue;
		count++;
	}
	return attributeValues;
}

function convertKeysToUpperCase(attributeValuesMap) {
	info("converting attribute map keys to upperCase");
	var newobj={}
	for (var key in attributeValuesMap) {
		newobj[key.toUpperCase()] = attributeValuesMap[key];
	}
	return newobj;
}

function getAssetNameFromId(assetId) {
	var assetName = null;
	var nameAttributeId = 2;
	var attributeIdsArray = [nameAttributeId];
	var attributesArray = assetServiceApi.getAttributeValues(assetId, attributeIdsArray);
	var i;
	for(i=0; i < attributesArray.length; i++) {
		if(attributesArray[i].getAttributeId() == nameAttributeId){
			assetName = attributesArray[i].getAttributeValue().getValue();
			break;
		}
	}
	
	return assetName;
}

function getcontentTypeHierarchyFromId(contentTypeId) {
	var contentTypeInfos = contentStructureServiceApi.getContentTypeHierarchy(contentTypeId);
	var hierarchy = "";
	for(var index = 0; index < contentTypeInfos.length; index++){
		if(index == 0)
	   		hierarchy = contentTypeInfos[index].getName();
		else
			hierarchy = contentTypeInfos[index].getName()+';'+hierarchy;
	}
	return hierarchy;
}