/*********************************************************************
Quark Publishing 
 
©2018 Quark Software Inc. All rights reserved.

Script Name:
	GetAssetHighres
Objective : 
	The script will return the content of a QPP asset, with the given asset id, at the location specified 
	in variable "targetFolder".  
Prerequisite: 
	- Variable "assetId" should refer to the QPP Server's Asset. 
	- Varibale "targetFolder" should refer to the location where the asset highres is to be copied. 
	  Target folder should have write permissions.
**************************************************************************/

//Import the required pacakages
importPackage(Packages.java.lang);
importPackage(Packages.com.quark.qpp.core.asset.service.constants);

//load QppFunctions Script for helper methods
load("QppFunctions");

//Specify id of the QPP Server asset whose highres is to be retrieved.
var assetId = 10308;
	
//Specify name of the QPP Server asset whose highres is to be retrieved.
//var assetName = "asset.jpg"

//Specify the version of asset that is to be retrieved.
//In case major and minor version are set null, then the highres of the latest version of asset is returned. 
var majorVersion = null;
var minorVersion = null;

//Specify the folder where the asset HIGHRES is to be delivered.
var targetFolder = "D:/target";

//Get reference of QPP Server's Asset serviceApi.
var assetServiceApi = runtimeBeanManager.lookupBean("assetServiceApi")

//Get reference of blobLocalStreamingService in QPP Server.
var blobLocalStreamingService = runtimeBeanManager.lookupBean("blobLocalStreamingService");

var stringBuffer = new StringBuffer();

try{
	var assetName = getAssetNameFromId(assetId);
	
 	var highResFile = new File(targetFolder, assetName);

	//declare an output stream where the asset highres will be written
	var fileOutputStream = new FileOutputStream(highResFile);
	var assetDownloadInfo;
	if(majorVersion != null &&  minorVersion != null) {
		assetDownloadInfo = assetServiceApi.getAssetDownloadInfo(assetId, majorVersion, minorVersion);
	}else {
		assetDownloadInfo = assetServiceApi.getAssetDownloadInfo(assetId, 0, 0);
	}
	
	blobLocalStreamingService.download(assetDownloadInfo.getDownloadUrl(), fileOutputStream);
	
	stringBuffer.append(assetName+" successfully copied at location : "+targetFolder);
}catch(exception){
	stringBuffer.append("Error : Couldnot get highres of asset:"+assetName+"\n");
	stringBuffer.append(exception);
}finally{
	if(fileOutputStream != undefined){
		fileOutputStream.close();
	}
}
stringBuffer.toString();
