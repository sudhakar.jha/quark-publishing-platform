/*********************************************************************
Quark Publishing 
 
©2018 Quark Software Inc. All rights reserved.

Script Name:
	UpdateAssetMetadata
Objective : 
	The script will update the metadata of an asset with a given id.
Prerequisite: 
	- Variable "assetId" should refer to the QPP Server's Asset Id. 
**************************************************************************/

//Import the required pacakages
importPackage(Packages.java.lang);
importPackage(Packages.com.quark.qpp.core.asset.service.constants);

//load QppFunctions Script for helper methods
load("QppFunctions");

//Specify id of the QPP Server asset whose metadata is to be updated.
var assetId = 10308;
//= "Project.qxp";

//specify the attribute values to be set
var newAssetName = "asset.qxp";
var collectionPath = "Education";
var workflow = "Publishing Workflow";
var status = "Draft";
var routeToUserName = "Admin";

var newAttributesMap = {"name": newAssetName, "collection" : collectionPath ,"workflow" : workflow, "status": status, "routed to":routeToUserName};	

//Get reference of QPP Server's AssetServiceApi.
var assetServiceApi = runtimeBeanManager.lookupBean("assetServiceApi")

var stringBuffer = new StringBuffer();

try{
		//create com.quark.qpp.core.attribute.service.dto.AttributeValue Array from the above attributeValuesMap.
		var newAttributeValuesArray = createAttributeValueArray(newAttributesMap);
		
		//set asset Attribute Values (update status)
		assetServiceApi.setAttributeValues(assetId, newAttributeValuesArray);

		stringBuffer.append("Asset metadata updated successfully. ");

}catch(exception){
	stringBuffer.append("Error while setting asset metadata. \n");
	stringBuffer.append(exception);
}
stringBuffer.toString();
