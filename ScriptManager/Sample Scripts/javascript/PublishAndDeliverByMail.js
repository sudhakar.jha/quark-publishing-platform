/*********************************************************************
Quark Publishing 
 
©2018 Quark Software Inc. All rights reserved. 

Script Name: 
	PublishAndDeliverAnAsset
Objective : 
	The script will publish a QuarkXPress project as PDF and the output is delivered using sendEmail delivery channel.
Prerequisite:
	Variable "assetPath" refers to the QPP Asset on the basis of its collection path. 
**************************************************************************/

//Import the required packages
importPackage(Packages.java.lang);
importPackage(Packages.java.util);

//specify the collection path of the QPP Asset that is to be published and delivered.
//Specify asset path of the QPP Asset that is to be delivered.
var assetPath = "Home/asset.qxp";
var qppUri = "qpp://assetsbypath/"+ assetPath; 

//Specify publishing channel id
var publishingChannelId = "qxpPdf";

//Specify delivery channel id
var deliveryChannelId = "sendEmail";

//For sendEmail deliver channel, following parameter values are required :

//Specify exchange server
var exchangeServer = "exchange-server"; 

//Specify mail address of the sender.
var senderMailId = "abc@abc.com";

//Specify mail address of user to whom the email will be sent
var recipientMailId ="xyz@xyz.com"; 

//Specify subject of the email
var mailSubject = "QPP Asset delivered.";

//Specify message of the email
var mailMessage = "PDF of QPP asset : " +assetPath+" has been delivered using sendEmail deliver channel.";

//Declare QPP Service Apis
var publishingServiceApi = runtimeBeanManager.lookupBean("publishingServiceApi");

//declare a publishing parameters map. Add parameters required by publishing channel
var publishingParametersMap = new HashMap();
//publishingParametersMap.put("SPREAD_VIEW", "false"); 

//Add parameters required by delivery channel (Use the same map to add publishing and delivery channel parameters)
publishingParametersMap.put("EXCHANGE_SERVER", exchangeServer); 
publishingParametersMap.put("SENDER_EMAIL_ID", senderMailId);
publishingParametersMap.put("RECIPIENT_EMAIL_IDS", recipientMailId);
publishingParametersMap.put("SEND_AS_ATTACHMENT", "true");
publishingParametersMap.put("SUBJECT", mailSubject);
publishingParametersMap.put("MESSAGE", mailMessage);

//Declare a string buffer
var stringBuffer = new StringBuffer();

try{
	publishingServiceApi.publishAndDeliverBasedOnURI(qppUri, publishingChannelId, deliveryChannelId, publishingParametersMap);
	stringBuffer.append("Asset : "+assetPath+" successfully published and delivered.\n");
}catch(exception){
	stringBuffer.append("Error while publishing the asset : "+assetPath+"\n");
	stringBuffer.append(exception);		
}
stringBuffer.toString();

