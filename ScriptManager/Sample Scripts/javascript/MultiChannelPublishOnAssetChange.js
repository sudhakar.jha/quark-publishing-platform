/*********************************************************************
Quark Publishing 
 
©2018 Quark Software Inc. All rights reserved. 

Script Name: 
	PublishAssetOnStatusChange
Objective : 
	The script will publish an asset on status change. using the specified publishing channels.
	The published output will be checked in to the specified colelction. 
	The asset status shall be changed to the status desired after the published asset is checked-in. 
	
Prerequisite: 
	Script should  be configured as event based with "OBJECT TYPE" as ASSET 
	and "CHANGE TYPE" as ASSET CHANGED.
	- Variable "assetId" refers to the id of the asset to be published.
	- Variable "desiredSourceStatusName" refers to the desired status value for an asset to be published.
	- Variable "contentTypeToChannelsMap" refers to the content type of asset to comma separated set of publishing channels to be used for publishing.
	- Variable publishedAssetCollectionPath refers to the complete collection value specified by collection path separated by "/" in which asset is to be checked in after publishing.
	- Variable publishedAssetRouteToUser refers to the user to whom the checked in asset needs to be routed.
	- Variable publishedAssetStatus refers to the status value to be set on an asset after publishing.
	- Variable publishedAssetCreateMinorVersion refers to the whether the checkin needs to be associated with a minor version or a major version.

Note:
	To provide any attribute value for an attribute of user defined heirarchial domain type, use ';' as separator.
	example - "place":"World;Asia;India;Punjab"
**************************************************************************/

importPackage(Packages.com.quark.qpp.core.asset.service.constants);
importPackage(Packages.com.quark.qpp.core.messaging.service.constants);
importPackage(Packages.java.lang);
importPackage(Packages.java.io);
importPackage(Packages.java.util);
importPackage(Packages.com.quark.qpp.core.content.service.constants);
importPackage(Packages.com.quark.qpp.core.asset.service.dto);
importPackage(Packages.com.quark.qpp.core.attribute.service.dto)
importPackage(Packages.com.quark.qpp.common.dto)
importPackage(Packages.com.quark.qpp.core.attribute.service.constants)

//load the QPPFunctions script for helper methods.
load("QppFunctions");

//name of the status, such that when an asset is in given status of the given workflow, it should be published.
//var desiredSourceWorkflow = "Research Document";
var desiredSourceStatusName = "Initiate Publishing";

//Content Type to Publishign Channels map for defining comma separated publishing channels to be used for publishing
var contentTypeToChannelsMap = 
{
"System;Structured Content;Smart Content;Document;Company Report":"smartDocHtmlZip,CompanyReportPdf", 
"System;Structured Content;Smart Content;Document;Smart Document":"smartDocPdfEx,smartDocHtmlZip", 
"System;Structured Content;Business Document":"busDocPdf",
"System;Asset;Document;Microsoft Word":"wordDocPdf",
"System;Asset;Project;QuarkXPress Project":"qxpPdf",
"System;Asset;Document;Microsoft PowerPoint":"pptPdf"
};

//Specify status to be set for the of published asset
var publishedAssetStatus = "Published";

//Specify complete collection path, from root collection, in which published asset should be checked-in. 
//Collection names in collection path should be separated by "/" .
var publishedAssetCollectionPath = "Rivacorp/Published";

//Specify the user to whom the published asset has to be routed to.
var publishedAssetRouteToUser = "Admin@rivacorp.com";


//Map used to lookup the appropriate content type of the published output
var mimeTypeToContentTypeMap = {"application/zip":"System;Asset", "application/pdf":"System;Asset;Picture", "image/jpeg":"System;Asset;Picture"};

//Set the flag depending on the revision control settings for the given collection
var publishedAssetCreateMinorVersion = false;

//asset Id of the asset on which event took place.
var assetId = ASSET.get(DefaultMessagePropertyNames.OBJECT_ID);

// Destination folder for published content
var publishedAssetDestinationFolder = java.lang.System.getProperty("java.io.tmpdir");

//Get reference of PublishingServiceApi
var publishingServiceApi = runtimeBeanManager.lookupBean("publishingServiceApi");

//Get reference of AssetServiceApi
var assetServiceApi = runtimeBeanManager.lookupBean("assetServiceApi");

//Get reference of AttributeServiceApi
var attributeServiceApi = runtimeBeanManager.lookupBean("attributeServiceApi");

//Get reference of blobLocalStreamingService
var blobLocalStreamingService = runtimeBeanManager.lookupBean("blobLocalStreamingService");

//Get reference of contentStructureServiceApi
var contentStructureServiceApi = runtimeBeanManager.lookupBean("contentStructureServiceApi");

var sb = new StringBuffer();

info("assetID = " + assetId);


var requiredAttributeIds = [DefaultAttributes.WORKFLOW,DefaultAttributes.STATUS,DefaultAttributes.CONTENT_TYPE, DefaultAttributes.NAME];
var publishedAssetAttributeValues = [];
var sourceRequiredAssetAttributeValues = assetServiceApi.getAttributeValues(assetId, requiredAttributeIds);
var sourceAssetWorkFlow = null;
var sourceAssetStatus = null;
var sourceAssetContentType = null;
var sourceAssetContentTypeHierarchy = null;
var sourceAssetName = null;
var i;
for (i = 0; i < sourceRequiredAssetAttributeValues.length; i++) { 
	if(sourceRequiredAssetAttributeValues[i].getAttributeId() == DefaultAttributes.WORKFLOW){ //workflow
		sourceAssetWorkFlow = sourceRequiredAssetAttributeValues[i].getAttributeValue();
		
	} else if(sourceRequiredAssetAttributeValues[i].getAttributeId() == DefaultAttributes.STATUS){ //status
		sourceAssetStatus = sourceRequiredAssetAttributeValues[i].getAttributeValue();
	} else if(sourceRequiredAssetAttributeValues[i].getAttributeId() == DefaultAttributes.CONTENT_TYPE){ //content type
		sourceAssetContentType = sourceRequiredAssetAttributeValues[i].getAttributeValue();
		sourceAssetContentTypeHierarchy = getcontentTypeHierarchyFromId(sourceAssetContentType.getId());
	} else if(sourceRequiredAssetAttributeValues[i].getAttributeId() == DefaultAttributes.NAME){ //name
		sourceAssetName = sourceRequiredAssetAttributeValues[i].getAttributeValue().getValue();
	}
} 
info("sourceAssetName = " + sourceAssetName +"   sourceAssetContentTypeHierarchy:"+ sourceAssetContentTypeHierarchy);
//var desiredSourceContentTypeName = contentStructureServiceApi.getContentTypesInfoByHierarchy(desiredSourceContentType).getName();

var dateTime = System.currentTimeMillis();

if(sourceAssetContentType != null && sourceAssetContentTypeHierarchy in contentTypeToChannelsMap){

	if(sourceAssetWorkFlow != null && sourceAssetStatus != null && desiredSourceStatusName.equalsIgnoreCase(sourceAssetStatus.getName())){	
		//get asset Attributes
		var sourceAssetAttributes = assetServiceApi.getAsset(assetId).getAttributeValues();
		
		var channelIds = contentTypeToChannelsMap[sourceAssetContentTypeHierarchy];
		var channelIdsArray = channelIds.split(',');
		
		for (var ii = 0; ii < channelIdsArray.length ; ii++) {							
			var aPublishingChannelId = channelIdsArray[ii];
			info("Publishing started for channel: "+ aPublishingChannelId);
			publishAndCheckinDocument(aPublishingChannelId, sourceAssetAttributes);				
			info("Publishing finished for channel: "+ aPublishingChannelId);
		}
		updateStatusOfSourceAsset(sourceAssetAttributes);						
	}
}

function updateStatusOfSourceAsset(sourceAssetAttributes) {
 	try {
		var aWorkflowName = null;
		for (n = 0; n < sourceAssetAttributes.length; n++) {
			if(sourceAssetAttributes[n].getAttributeId() == DefaultAttributes.WORKFLOW){
				aWorkflowName = sourceAssetAttributes[n].getAttributeValue().getName();
				break;
			}
		}		

		//set asset Attribute Values (update status) - Workflow name required later to get the id of status
		var statusAttributeValuesMap = {"STATUS":publishedAssetStatus,"WORKFLOW":aWorkflowName};
		var sourceAssetStatusAttributes = createAttributeValueArray(statusAttributeValuesMap);
		assetServiceApi.setAttributeValues(assetId ,sourceAssetStatusAttributes);
		info("\Success : source asset's status marked to " + publishedAssetStatus +"\n");
		} catch(exception){
			info("\nError : while source asset's status marking to " + aWorkflowName+"::"+publishedAssetStatus +"\n" + exception);
		}
}

function publishAndCheckinDocument(aChannelId, sourceAssetAttributes){
		var _channelDef = publishingServiceApi.getChannel(aChannelId);
		var channelMimeType = _channelDef.getOutputType();
		var publishedOutputFileExtension = getFileExtensionFromMimeType(channelMimeType);
		var publishedContentType = mimeTypeToContentTypeMap[channelMimeType];
		info("sourceAssetName = "+sourceAssetName +"  \n");	
		var publishedAssetName = sourceAssetName + "_" + dateTime + "."+ publishedOutputFileExtension;
		info("publishedAssetName = "+publishedAssetName +"  \n");
		var outputFile = new File(publishedAssetDestinationFolder + "/"+ publishedAssetName+"."+publishedOutputFileExtension);
		var fileOutputStream = new FileOutputStream(outputFile);		

		//Map of publishing channel paramaters
		var publishingParametersMap = new HashMap();

		//publish Asset		
		try{
			info("enter");
			var assetDonInfo = publishingServiceApi.publishAsset(assetId, aChannelId, publishingParametersMap);
			blobLocalStreamingService.download(assetDonInfo[0].getDownloadUrl(), fileOutputStream);
			info("asset published in " + publishedAssetDestinationFolder + "\n");
			sb.append("published \n");
		}catch(exception){
			info("Error while publishing the asset with name : "+publishedAssetName+"\n" + exception)
			sb.append(exception);
		}finally{
			if(fileOutputStream != undefined){		
				fileOutputStream.close();
			}
		}
		
		var filename = outputFile.getName();

		//get the file extension 
		var filextension = getFileExtension(filename);

		var attributeValuesMap = {"name":publishedAssetName, "file extension":filextension, "content type":publishedContentType, "original filename":filename, "routed to":publishedAssetRouteToUser, "Collection":publishedAssetCollectionPath};
		
		//create com.quark.qpp.core.attribute.service.dto.AttributeValue Array from the above attributeValuesMap.
		var attributeValuesArray = createAttributeValueArray(attributeValuesMap);
		
		// initialise counter for publishedAssetAttributeValues array
		var k=0;
		
		//get user defined attributes
		for (i = 0; i < sourceAssetAttributes.length; i++) {
			if(attributeServiceApi.getAttribute(sourceAssetAttributes[i].getAttributeId()).getModificationLevel() == AttributeModificationLevels.USER_MODIFIABLE){
				if(sourceAssetAttributes[i].getAttributeId() != DefaultAttributes.STATUS && sourceAssetAttributes[i].getAttributeId() != DefaultAttributes.WORKFLOW){
					publishedAssetAttributeValues[k] = sourceAssetAttributes[i];
					k++;
				}
			}
		}

		info("merge attributes");
		//get merged attributeValues to checkin published asset
		var j;
		for (i = 0; i < attributeValuesArray.length; i++) {
			 for (j = 0; j < publishedAssetAttributeValues.length; j++){
				if(attributeValuesArray[i].getAttributeId() == publishedAssetAttributeValues[j].getAttributeId()){
					publishedAssetAttributeValues[j] = attributeValuesArray[i];
					break;
				}
			}
			if(j == publishedAssetAttributeValues.length){
				publishedAssetAttributeValues[j] = attributeValuesArray[i];
			}
		}
		try{
			var fileInputStream = new FileInputStream(outputFile);
			var asset = new Asset();
			asset.setAttributeValues(publishedAssetAttributeValues);

			var publishedAssetId;
			//initiate check in of new asset using assetServiceApi's initNewCheckin API.
			var assetUploadInfo = assetServiceApi.initNewCheckin(asset,publishedAssetCreateMinorVersion);
			if(assetUploadInfo != null){
				info("\nThe asset context ="+ assetUploadInfo.getContextId() + " url = " + assetUploadInfo.getUploadUrl());
				publishedAssetId = assetServiceApi.getAssetStatus(assetUploadInfo.getContextId()).getAssetId();
			}
			blobLocalStreamingService.upload(assetUploadInfo.getUploadUrl(), fileInputStream);
			info("\nThe asset " + "'"+ filename + "' publishedAssetId ="+ publishedAssetId + "  has been successfully checked into QPP Server.");
			sb.append("\nThe asset " + "'"+ filename + "' publishedAssetId ="+ publishedAssetId + "  has been successfully checked into QPP Server.");
		
		}catch(exception){
			info("\nError : The asset " + "'"+ filename + "'" + " could not be checked into QPP Server.\n" + exception);
		}finally{
			if(fileInputStream != undefined){
				
				fileInputStream.close();
			}
		}
}

sb.toString();
