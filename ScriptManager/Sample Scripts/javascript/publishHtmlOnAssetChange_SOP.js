/*********************************************************************
Quark Publishing 
 
©2018 Quark Software Inc. All rights reserved. 

Script Name: 
                PublishAssetOnStatusChange
Objective : 
                The script will publish an asset on status change. The provided channel will be used for publishing.
                The file generated will have the provided asset name.
                The asset status shall be changed to the status desired to be set on published asset. 
                The published asset will be checked in the platform in the provided collection.
Prerequisite: 
                Script should  be configured as event based with "OBJECT TYPE" as ASSET 
                and "CHANGE TYPE" as ASSET CHANGED.
                - Variable "assetId" refers to the id of the asset to be published.
                - Variable "desiredSourceWorkflow" refers to the desired workflow value for an asset to be published.
                - Variable "desiredSourceStatusName" refers to the desired status value for an asset to be published.
                - Variable "publishedAssetDestinationFolder" refers to the file system location from where published output is to be retrieved for checkin.
                - Variable "publishingChannelId" refers to the publishing channel to be used for publishing.
                - Map "publishingParametersMap" refers to the map of publishing paramaters to be used while publishing.
                - Variable "desiredSourceContentType" refers to desired content type of the asset to be published.
                - Variable "publishedOutputFileExtension" refers to publishing Output's file extension.
                
                Checkin Variables
                ------------------
                - Variable publishedAssetStatus refers to the status value to be set on an asset after publishing.
                - Variable publishedAssetCollectionPath refers to the complete collection value specified by collection path separated by "/" in which asset is to be checked in after publishing.
                - Variable publishedAssetName refers to the name to be given to the published asset which will further be checkin in collection.
                - Variable publishedAssetRouteToUser refers to the user to whom the checked in asset needs to be routed.
                - Variable publishedAssetContentType refers to the content type of the published asset that is required attribute while checkin of the asset.
                - Variable publishedAssetCreateMinorVersion refers to the whether the checkin needs to be associated with a minor version or a major version.

Note:
                To provide any attribute value for an attribute of user defined heirarchial domain type, use ';' as separator.
                example - "place":"World;Asia;India;Punjab"
**************************************************************************/

importPackage(Packages.com.quark.qpp.core.asset.service.constants);
importPackage(Packages.com.quark.qpp.core.messaging.service.constants);
importPackage(Packages.java.lang);
importPackage(Packages.java.io);
importPackage(Packages.java.util);
importPackage(Packages.com.quark.qpp.core.content.service.constants);
importPackage(Packages.com.quark.qpp.core.asset.service.dto);
importPackage(Packages.com.quark.qpp.core.attribute.service.dto)
importPackage(Packages.com.quark.qpp.common.dto)
importPackage(Packages.com.quark.qpp.core.attribute.service.constants)
importPackage(Packages.org.apache.commons.httpclient);
importPackage(Packages.org.apache.commons.httpclient.methods);
importPackage(Packages.org.springframework.util);

//load the QPPFunctions script for helper methods.
load("QppFunctions");

//name of the status, such that when an asset is in given status of the given workflow, it should be published.
var desiredSourceWorkflow = "SOP Authoring";
var desiredSourceStatusName = "Approved For Publishing";

// desired content type for the asset to be published
var desiredSourceContentType = "System;Structured Content;Smart Content;Document;Standard Operating Procedure";


//Channel to be used for publishing
var publishingChannelId = "SOPHTML";

//Publishing Output's file extension
var publishedOutputFileExtension = "zip";

//Map of publishing channel paramaters
var publishingParametersMap = new HashMap();

//Checkin attributes of published asset
//*********************************
var publishedAssetStatus = "Published";

//Specify complete collection path, from root collection, in which published asset should be checked-in. 
//Collection names in collection path should be separated by "/" .
var publishedAssetCollectionPath = "Home/FinGR/Published";

//Specify the user to whom the published asset has to be routed to.
var publishedAssetRouteToUser = "Admin@cedevsop.com";

//Set the flag depending on the revision control settings for the given collection
var publishedAssetCreateMinorVersion = false;

//Content type of the published asset that needs to be checked in after publishing
var publishedAssetContentType = "System;Asset";

//asset Id of the asset on which event took place.
var assetId = ASSET.get(DefaultMessagePropertyNames.OBJECT_ID);

// Destination folder for published content
var publishedAssetDestinationFolder = java.lang.System.getProperty("java.io.tmpdir");

//Get reference of PublishingServiceApi
var publishingServiceApi = runtimeBeanManager.lookupBean("publishingServiceApi");

//Get reference of AssetServiceApi
var assetServiceApi = runtimeBeanManager.lookupBean("assetServiceApi");

//Get reference of AttributeServiceApi
var attributeServiceApi = runtimeBeanManager.lookupBean("attributeServiceApi");

//Get reference of blobLocalStreamingService
var blobLocalStreamingService = runtimeBeanManager.lookupBean("blobLocalStreamingService");

//Get reference of contentStructureServiceApi
var contentStructureServiceApi = runtimeBeanManager.lookupBean("contentStructureServiceApi");

var sb = new StringBuffer();

info("assetID = " + assetId);


var requiredAttributeIds = [DefaultAttributes.WORKFLOW,DefaultAttributes.STATUS,DefaultAttributes.CONTENT_TYPE, DefaultAttributes.NAME];
var publishedAssetAttributeValues = [];
var sourceAssetStatusAttributes = [];
var sourceRequiredAssetAttributeValues = assetServiceApi.getAttributeValues(assetId, requiredAttributeIds);
var sourceAssetWorkFlow = null;
var sourceAssetStatus = null;
var sourceAssetContentType = null;
var sourceAssetName = null;
var i;
for (i = 0; i < sourceRequiredAssetAttributeValues.length; i++) { 
                if(sourceRequiredAssetAttributeValues[i].getAttributeId() == DefaultAttributes.WORKFLOW){ //workflow
                                sourceAssetWorkFlow = sourceRequiredAssetAttributeValues[i].getAttributeValue();
                                
                } else if(sourceRequiredAssetAttributeValues[i].getAttributeId() == DefaultAttributes.STATUS){ //status
                                sourceAssetStatus = sourceRequiredAssetAttributeValues[i].getAttributeValue();
                } else if(sourceRequiredAssetAttributeValues[i].getAttributeId() == DefaultAttributes.CONTENT_TYPE){ //content type
                                sourceAssetContentType = sourceRequiredAssetAttributeValues[i].getAttributeValue();
                } else if(sourceRequiredAssetAttributeValues[i].getAttributeId() == DefaultAttributes.NAME){ //name
                                sourceAssetName = sourceRequiredAssetAttributeValues[i].getAttributeValue().getValue();
                }
} 

var desiredSourceContentTypeName = contentStructureServiceApi.getContentTypesInfoByHierarchy(desiredSourceContentType).getName();

var dateTime = System.currentTimeMillis();

if(sourceAssetContentType != null && desiredSourceContentTypeName.equalsIgnoreCase(sourceAssetContentType.getName())){
                if(sourceAssetWorkFlow != null && desiredSourceWorkflow.equalsIgnoreCase(sourceAssetWorkFlow.getName()) && sourceAssetStatus != null && desiredSourceStatusName.equalsIgnoreCase(sourceAssetStatus.getName())){     
                                info("html sourceAssetName = "+sourceAssetName +"  \n");        
                                var publishedAssetName = sourceAssetName + "_" + dateTime + "."+ publishedOutputFileExtension;
                                info("publishedAssetName = "+publishedAssetName +"  \n");
                                var outputFile = new File(publishedAssetDestinationFolder + "/"+ publishedAssetName+"."+publishedOutputFileExtension);
                                var fileOutputStream = new FileOutputStream(outputFile);                          

                                //publish Asset                 
                                try{
                                                info("enter html");
                                                //var assetDonInfo = publishingServiceApi.publishAsset(assetId, publishingChannelId, publishingParametersMap);
                                                
                                                var cfoFile = getCFOZip();
												info("asset collected for output: " + cfoFile + "\n input size = " + cfoFile.length());
												var inputStream = new FileInputStream(cfoFile);
												try {
													publish(inputStream, publishingChannelId, fileOutputStream);
												} finally {
													if(inputStream != undefined){                          
                                                        inputStream.close();
													}
												}
						//blobLocalStreamingService.download(assetDonInfo[0].getDownloadUrl(), fileOutputStream);
                                                info("html asset published in " + publishedAssetDestinationFolder + "\n");
                                                sb.append("published html \n");
                                }catch(exception){
                                                sb.append("html Error while publishing the asset with name : "+publishedAssetName+"\n");
                                                sb.append(exception);
                                }finally{
                                                if(fileOutputStream != undefined){                          
                                                                fileOutputStream.close();
                                                }
                                }
                                
                                var filename = outputFile.getName();

                                //get the file extension 
                                var filextension = getFileExtension(filename);

                                var attributeValuesMap = {"name":publishedAssetName, "workflow":desiredSourceWorkflow , "status":publishedAssetStatus, "file extension":filextension, "content type":publishedAssetContentType, "original filename":filename, "routed to":publishedAssetRouteToUser, "Collection":publishedAssetCollectionPath};
                                
                                //create com.quark.qpp.core.attribute.service.dto.AttributeValue Array from the above attributeValuesMap.
                                var attributeValuesArray = createAttributeValueArray(attributeValuesMap);
                                
                                //get asset Attributes
                                var sourceAssetAttributes = assetServiceApi.getAsset(assetId).getAttributeValues();
                                
                                // initialise counter for publishedAssetAttributeValues array
                                var k=0;
                                
                                //get user defined attributes
                                for (i = 0; i < sourceAssetAttributes.length; i++) {
                                                if(attributeServiceApi.getAttribute(sourceAssetAttributes[i].getAttributeId()).getModificationLevel() == AttributeModificationLevels.USER_MODIFIABLE){
                                                                if(sourceAssetAttributes[i].getAttributeId() != DefaultAttributes.STATUS){
                                                                                publishedAssetAttributeValues[k] = sourceAssetAttributes[i];
                                                                                k++;
                                                                }
                                                }
                                }
                                //getStatus AttributeValue 
                                for (i = 0; i < attributeValuesArray.length; i++) {
                                                if(attributeValuesArray[i].getAttributeId() == DefaultAttributes.STATUS){
                                                                sourceAssetStatusAttributes[0] = attributeValuesArray[i];
                                                }
                                }
                                
                               
                                //get merged attributeValues to checkin published asset
                                var j;
                                for (i = 0; i < attributeValuesArray.length; i++) {
                                                for (j = 0; j < publishedAssetAttributeValues.length; j++){
                                                                if(attributeValuesArray[i].getAttributeId() == publishedAssetAttributeValues[j].getAttributeId()){
                                                                                publishedAssetAttributeValues[j] = attributeValuesArray[i];
                                                                                break;
                                                                }
                                                }
                                                if(j == publishedAssetAttributeValues.length){
                                                                publishedAssetAttributeValues[j] = attributeValuesArray[i];
                                                }
                                }
                                try{
                                                var fileInputStream = new FileInputStream(outputFile);
                                                var asset = new Asset();
                                                asset.setAttributeValues(publishedAssetAttributeValues);

                                                //initiate check in of new asset using assetServiceApi's initNewCheckin API.
                                                var assetUploadInfo = assetServiceApi.initNewCheckin(asset,publishedAssetCreateMinorVersion);
												var publishedAssetId;
                                                if(assetUploadInfo != null){
                                                                sb.append("\nThe asset context ="+ assetUploadInfo.getContextId() + " url = " + assetUploadInfo.getUploadUrl());
                                                                publishedAssetId = assetServiceApi.getAssetStatus(assetUploadInfo.getContextId()).getAssetId();
                                                }
                                                blobLocalStreamingService.upload(assetUploadInfo.getUploadUrl(), fileInputStream);
                                                info("\nThe asset " + "'"+ filename + "'  publishedAssetId ="+  publishedAssetId + "  has been successfully checked into QPP Server.");
                                                sb.append("\nThe asset " + "'"+ filename + "' publishedAssetId ="+  publishedAssetId + "  has been successfully checked into QPP Server.");
                                }catch(exception){
                                                sb.append("\nError : The asset " + "'"+ filename + "'" + " could not be checked into QPP Server.\n");
                                                sb.append(exception);
                                }finally{
                                                if(fileInputStream != undefined){
                                                                
                                                                fileInputStream.close();
                                                }
                                }
								
								try{
									//set asset Attribute Values (update status)
									assetServiceApi.setAttributeValues(assetId ,sourceAssetStatusAttributes);
									info("\nSuccess : source asset's status marked to " + publishedAssetStatus +"\n");
								} catch(exception){
									info("\nError : while source asset's status marking to " + publishedAssetStatus +"\n" + exception);
								}
                                                
                }
}

function publish(inputStream, publishingChannelId, outputStream){
                info("new started\n");   
                var publishingUrl ="https://qppsop.app.quark.com/rest/service/publish?publishingchannel="+publishingChannelId+"&loginname=Admin&loginpassword=Admin";
				info("publishing url = " + publishingUrl+"\n");
				//"https://neeraj.india.quark.com:61400/rest/service/publish?publishingchannel=smartDocHTML&loginname=Admin&loginpassword=Admin";

				// "https://qppsop.app.quark.com/rest/service/publish?loginname=Admin&loginpassword=Admin&publishingchannel=";

                var method = new PostMethod(publishingUrl);
                var entity = new InputStreamRequestEntity(inputStream);
                method.setRequestEntity(entity);
                                                                
                var client = new HttpClient();
                var status = client.executeMethod(method);       
                                info("status = "+status );
                if (status == 200) {
                                var inStream = null;
                                try {
                                                inStream = method.getResponseBodyAsStream();
                                                StreamUtils.copy(inStream, outputStream);
                                } finally {
                                                inStream.close();
                                }
                }
		method.releaseConnection();
                return status;
}

function getCFOZip() {
	var fosCfoZip = null;
	try {
		/*
		Channel parameters for publishing channel with id : SOPHTML 
		*/	
		var parametersForSmartDoc = new java.util.HashMap();
		parametersForSmartDoc.put("RESOLVE_QPP_RELATIONS", "true"); 

		/*
			Map of publishing channel id and publishing parameters
		*/	
		var parametersMap = { 'collectSmartDocForOutputEx' : parametersForSmartDoc};

		//Dummy channel param - otherwise it throws error ?????
		var publishingChannelParameters = parametersMap['collectSmartDocForOutputEx'];
		
		var publishingOutputDownloadInfo = publishingServiceApi.publishAsset(assetId, "collectSmartDocForOutputEx", publishingChannelParameters);
		
		var cfoZipFile = new File(publishedAssetDestinationFolder + "/" + sourceAssetName + "_CFO_" + System.currentTimeMillis() +".zip");
		fosCfoZip = new FileOutputStream(cfoZipFile);
		
		blobLocalStreamingService.download(publishingOutputDownloadInfo[0].getDownloadUrl(), fosCfoZip);

		return cfoZipFile;
	} finally{
		if(fosCfoZip != undefined){
			fosCfoZip.close();
		}
	}
}

sb.toString();
 