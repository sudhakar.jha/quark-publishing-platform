/*********************************************************************
Quark Publishing 
 
©2018 Quark Software Inc. All rights reserved.

Script Name:
	GetAssetMetadata
Objective : 
	The script will return the complete metadata of the asset with the given id and asset version.
Prerequisite: 
	- Variable "assetId" should refer to the QPP Server's Asset. 
	- Variable "majorVersion" should refer to the required Asset's major version. Provide null to fetch metadata of latest version. 
	- Variable "minorVersion" should refer to the required Asset's minor version. Provide null to fetch metadata of latest version.
**************************************************************************/

//Import the required pacakages
importPackage(Packages.java.lang);
importPackage(Packages.com.quark.qpp.core.asset.service.dto);
importPackage(Packages.com.fasterxml.jackson.databind);

//Specify Id of the QPP Server asset whose metadata is to be retrieved.
var assetId = 10308;

//Specify the version of asset that is to be retrieved.
//In case major and minor version are set null, then the metadata of the latest version of asset is returned. 
var majorVersion = null;
var minorVersion = null;

//Get reference of QPP Server's Asset serviceApi.
var assetServiceApi = runtimeBeanManager.lookupBean("assetServiceApi")
var asset = null;
var stringBuffer = new StringBuffer();

try{
	//In case the asset version info is provided.
	if(majorVersion != null &&  minorVersion != null) {
		asset = assetServiceApi.getAssetVersion(assetId, majorVersion, minorVersion);
	}else {
	 	asset = assetServiceApi.getAsset(assetId);
	}
	var attributeValues = asset.getAttributeValues();
	var mapper = new ObjectMapper();
	//Converting the Object to JSONString
  	var jsonAttributeValues = mapper.writeValueAsString(attributeValues);
	stringBuffer.append("The metadata of asset "+assetId +" :\n");
	stringBuffer.append("-------------------------------------\n");
	info(""+jsonAttributeValues);
	stringBuffer.append(""+jsonAttributeValues);
}catch(exception){
	stringBuffer.append("Error while getting the asset metadata.");
	stringBuffer.append(exception);
}
stringBuffer.toString();