/*********************************************************************
Quark Publishing 
 
©2018 Quark Software Inc. All rights reserved. 

Script Name: 
	AssetWorkflowChangedEventLogger
Objective : 
	The script will log asset details on "Asset Workflow Changed" event.
Prerequisite: 
	Script should  be configured as event based with "OBJECT TYPE" as "ASSET" 
	and "CHANGE TYPE" as "ASSET WORKFLOW CHANGED".
**************************************************************************/
importPackage(Packages.com.quark.qpp.core.asset.service.constants);
importPackage(Packages.com.quark.qpp.core.messaging.service.constants);

var assetId = ASSET.get(DefaultMessagePropertyNames.OBJECT_ID);

var objectType = ASSET.get(DefaultMessagePropertyNames.OBJECT_TYPE);

var objectChangeType = ASSET.get(DefaultMessagePropertyNames.OBJECT_CHANGE_TYPE);

var attributesUpdated = ASSET.get(AssetMessageConstants.AssetMessagePropertyNames.ATTRIBUTES_UPDATED);

var oldAttributeValues = ASSET.get(AssetMessageConstants.AssetMessagePropertyNames.OLD_ATTRIBUTE_VALUES);

var newAttributeValues = ASSET.get(AssetMessageConstants.AssetMessagePropertyNames.NEW_ATTRIBUTE_VALUES);

info(" ----- Following message received due to the triggering of AssetWorkflowChangedEventLogger script ----- ");
info("OBJECT ID : "+assetId);
info("OBJECT TYPE : "+objectType);
info("OBJECT CHANGE TYPE : "+objectChangeType);
info("ATTRIBUTES UPDATED : "+attributesUpdated);
info("OLD ATTRIBUTE VALUES : "+oldAttributeValues);
info("NEW ATTRIBUTE VALUES : "+newAttributeValues);
info("---------------------------------------------");
