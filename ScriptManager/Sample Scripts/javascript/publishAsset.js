/*********************************************************************
Quark Publishing 
 
©2020 Quark Software Inc. All rights reserved. 

Script Name: 
	PublishAnAsset
Objective : 
	The script will publish a QuarkXPress project as PDF.
	The file generated will have same name as the that of QXP with desired format as extension.
Prerequisite: 
	- Variable "assetName" refers to the QPP Asset to be published.
	- Variable "publishingChannelId" refers to the publishing channel to be used for publishing QXP as PDF
	- Variable "publishingParametersMap" refers to the publshing channel parameters
	- Variable "destinationFolder" refers to the file system location where published output is to be retrieved.
**************************************************************************/
//Import the required packages
importPackage(Packages.java.lang);
importPackage(Packages.java.io);
importPackage(Packages.java.util);

//Load the QPPFunctions script for helper methods
load("QppFunctions");

//Specify name of the QuarkXPress Project that is to be published as PDF.
var assetName = "TestProject1";

//Specify ID of the publishing channel that holds valid for asset of given content type. 
//For instance to publish a QuarkXPress as PDF, the publishing channel id is qxpPdf whereas to publish a DITA as PDF the publishing channel Id is ditaQpsDocPdf.
var publishingChannelId = "qxpPdf";

//Specify publishing parameters depending on the publishing channel requirements
var publishingParametersMap = new HashMap();
//publishingParametersMap.put("SPREAD_VIEW", "false"); 

// Destination folder for PDF generation
var destinationFolder = "D:/share/target";

//Declare QPP Service Apis
var publishingServiceApi = runtimeBeanManager.lookupBean("publishingServiceApi");
var blobLocalStreamingService = runtimeBeanManager.lookupBean("blobLocalStreamingService");
//Declare a string buffer
var stringBuffer = new StringBuffer();

//Search for the asset with given name in QPP Server
var assetId = 10419;
//searchAssetWithName(assetName);

//In case asset with given name exists in QPP Server
if(assetId > 0 ){
	try{
		var pdfFile = new File(destinationFolder + "/"+ assetName+".pdf");
		var fileOutputStream = new FileOutputStream(pdfFile);
		
		var assetDonInfo = publishingServiceApi.publishAsset(assetId, publishingChannelId, publishingParametersMap);
		blobLocalStreamingService.download(assetDonInfo[0].getDownloadUrl(), fileOutputStream);
		stringBuffer.append("\n"+assetName+" has been published as PDF at location "+destinationFolder);
	}catch(exception){
		stringBuffer.append("Error while publishing the asset with name : "+assetName+"\n");
		stringBuffer.append(exception);
	}finally{
		if(fileOutputStream != undefined){
			fileOutputStream.close();
		}
	}

}else{
	stringBuffer.append("Error : Asset with name "+assetName+ " doesn't exist in QPP Server.");
}
stringBuffer.toString();

