/*********************************************************************
Quark Publishing 
 
©2018 Quark Software Inc. All rights reserved.

Script Name:
	CheckInNewAsset
Objective : 
	The script will checkIn a local file, specified in variable "filePath", to QPP as new asset.   
Prerequisite: 
	Collection Path, Worklflow name, Status name and route to user for the asset need to 
	specified in the variables "collectionPath", "workflowName", 
	"statusName" and "routeToUser" respectively. Set the createMinorVersion flag depending upon the
	revision control settings on specified collection.
	The asset will be checked in with the same name as that of file.
Note:
	To provide any attribute value for an attribute of user defined heirarchial domain type, use ';' as separator.
	example - "place":"World;Asia;India;Punjab"
**************************************************************************/

//import the required packages
importPackage(Packages.java.lang);
importPackage(Packages.java.io);
importPackage(Packages.com.quark.qpp.core.content.service.constants);
importPackage(Packages.com.quark.qpp.core.asset.service.dto);
importPackage(Packages.com.quark.qpp.core.attribute.service.dto)
importPackage(Packages.com.quark.qpp.common.dto)
importPackage(Packages.com.quark.qpp.core.attribute.service.constants)

//load the QPPFunctions script for helper methods.
load("QppFunctions");

//Specify the path of the file that needs to be checked-in
var filePath ="D:/share/data/TestProject.qxp";  

//Specify complete collection path, from root collection, in which assets should be checked-in. 
//Collection names in collection path should be separated by "/" .
var collectionPath = "Home/API_Created";

//Content type for the asset to be checked in.
var contentType = "System;Asset";
	
var collection = null;

var assetName = "TestProject";

//Specify name of Workflow which should be assigned to new QPP asset.
var workflowName = "New Workflow1";

//Specify name of the status in which file should be checked-in.
//This status should belong to the workflow specified above.
var statusName = "New Status 1";

//Specify the user to whom the asset has to be routed to.
var routeToUser = "Admin";

//Set the flag depending on the revision control settings for the given collection
var createMinorVersion = false;

//Get reference of assetServiceApi in QPP Server.
var assetServiceApi = runtimeBeanManager.lookupBean("assetServiceApi");

//Declare a string buffer
var sb = new StringBuffer();

//Get reference of blobLocalStreamingService in QPP Server.
var blobLocalStreamingService = runtimeBeanManager.lookupBean("blobLocalStreamingService");


try{
	var file = new File(filePath);

	var filename = file.getName();

	//get the file extension 
	var filextension = getFileExtension(filename);
	
	var fileInputStream = new FileInputStream(file);
	
	var attributeValuesMap = {"name":assetName, "workflow":workflowName, "status":statusName, "file extension":filextension, "content type":contentType, "original filename":filename, "routed to":routeToUser, "Collection":collectionPath};
	 
	//create com.quark.qpp.core.attribute.service.dto.AttributeValue Array from the above attributeValuesMap.
	var attributeValuesArray = createAttributeValueArray(attributeValuesMap);

	var asset = new Asset();
	asset.setAttributeValues(attributeValuesArray);

	//initiate check in of new asset using assetServiceApi's initNewCheckin API.
 	var assetUploadInfo = assetServiceApi.initNewCheckin(asset,createMinorVersion);
	if(assetUploadInfo != null){
		sb.append("\nThe asset context ="+ assetUploadInfo.getContextId() + " url = " + assetUploadInfo.getUploadUrl());
		var assetId = assetServiceApi.getAssetStatus(assetUploadInfo.getContextId()).getAssetId();
	}
	blobLocalStreamingService.upload(assetUploadInfo.getUploadUrl(), fileInputStream);

	sb.append("\nThe asset " + "'"+ filename + "' assettId ="+ assetId + "  has been successfully checked into QPP Server.");
}catch(exception){
	sb.append("\nError : The asset " + "'"+ filename + "'" + " could not be checked into QPP Server.\n");
	sb.append(exception);
}finally{
	if(fileInputStream != undefined){
		fileInputStream.close();
	}
}
sb.toString(); 


