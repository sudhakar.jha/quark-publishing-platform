/*********************************************************************
Quark Publishing 
 
©2018 Quark Software Inc. All rights reserved.

Script Name: 
	DeliverAnAsset
Objective : 
	The script will deliver a QPP Asset using the specified delivery channel.
Prerequisite: 
	- Variable "assetPath" refers to the QPP Asset on the basis of collection path.
	- Variable "deliveryChannelId" refers to the id of the delivery channel used to deliver specified asset.
	- Variables "exchangeServer", "senderMailId", "recipientMailId", "mailSubject", "mailMessage" refer to the delivery channel parameters.
**************************************************************************/

//Import the required packages
importPackage(Packages.java.lang);
importPackage(Packages.java.io);
importPackage(Packages.java.util);

//Load the QPPFunctions script for helper methods
load("QppFunctions");

//Specify asset path of the QPP Asset that is to be delivered.
var assetPath = "Home/asset.qxp";
var qppUri = "qpp://assetsbypath/"+ assetPath; 
//Specify ID of the delivery channel
var deliveryChannelId = "sendEmail";

//For sendEmail deliver channel, following parameter values are required :

//Specify exchange server
var exchangeServer = "exchange-server"; 

//Specify mail address of the sender.
var senderMailId = "abc@abc.com";

//Specify mail address of user to whom the email will be sent
var recipientMailId ="xyz@xyz.com"; 

//Specify subject of the email
var mailSubject = "QPP Asset delivered.";

//Specify message of the email
var mailMessage = "QPP asset : " +assetPath+" has been delivered using sendEmail deliver channel.";

//Construct delivery channel parameter's map
var deliveryParametersMap = new HashMap();
deliveryParametersMap.put("EXCHANGE_SERVER", exchangeServer); 
deliveryParametersMap.put("SENDER_EMAIL_ID", senderMailId);
deliveryParametersMap.put("RECIPIENT_EMAIL_IDS", recipientMailId);
deliveryParametersMap.put("SEND_AS_ATTACHMENT", "true");
deliveryParametersMap.put("SUBJECT", mailSubject);
deliveryParametersMap.put("MESSAGE", mailMessage);

//Declare QPP Service Apis
var publishingServiceApi = runtimeBeanManager.lookupBean("publishingServiceApi");

//Declare a string buffer
var stringBuffer = new StringBuffer();

try{		
	publishingServiceApi.deliverBasedOnURI(qppUri, deliveryChannelId, deliveryParametersMap)
	stringBuffer.append("\n Asset at path : "+assetPath+" has been delivered successfully using "+deliveryChannelId+" delivery channel.");
}catch(exception){
	stringBuffer.append("Error while delivering the asset : "+assetPath+"\n");
	stringBuffer.append(exception);
}
stringBuffer.toString();
