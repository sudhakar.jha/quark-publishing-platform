/*********************************************************************
Quark Publishing 
 
©2018 Quark Software Inc. All rights reserved.

Script Name:
	GetSavedSearchResults
Objective :
	The script will execute a query saved on Platform Server and return results.
Prerequisite:
	- Variable "queryId" should refer to a saved search in Platform .
**************************************************************************/


//Import the required packages
importPackage(Packages.com.quark.qpp.core.query.service.constants);
importPackage(Packages.com.quark.qpp.core.query.service.dto);
importPackage(Packages.java.lang);
importPackage(Packages.com.fasterxml.jackson.databind);

//Specify the id of saved search whose results are to be retrieved
var queryId = 104;

//Specify the Query display, in case you want the default saved display attributes for above query. default [], hence saved query display will be used.
var queryDisplay = new QueryDisplay();

//Specify the display mode
var displayMode = DefaultDisplayModes.DEFAULT;

//Specify the explore mode.
var exploreMode = ExploreModeTypes.PLAIN;

queryDisplay.setDisplayMode(displayMode);
queryDisplay.setExploreMode(exploreMode);

//Get reference of QPP Server's Asset Facade.
var queryServiceApi = runtimeBeanManager.lookupBean("queryServiceApi");
//declare a string buffer
var stringBuffer = new StringBuffer();

var response = queryServiceApi.getQueryResult(queryId, queryDisplay);

var mapper = new ObjectMapper();
//Converting the result Object to JSONString
var jsonResult = mapper.writeValueAsString(response);
info(""+jsonResult);
stringBuffer.append(""+jsonResult);
stringBuffer.toString();