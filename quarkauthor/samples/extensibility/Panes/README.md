# Custom Pane

The Custom Pane extensibility allows you to add panes across the authoring canvas.

To add Custom Pane extensibility to Quark Author: 

1. Create a folder __feature__ in the directory **qaservice/ext** in the s3 bucket.
2. Create a folder __custom-links-pane__ in the folder __feature__.
3. Add the Callback Handler plugin to the folder __custom-links-pane__. 

The name of the folder __custom-links-pane__ must be the same as you have specified in the file <i>qa-config.json</i> under the property "extensions."

<b>Sample <i>qa-config.json</i></b> (available for every content type)

```
"extensions": [
    "custom-links-pane"
]
```

The folder __custom-links-pane__ also must contain the file "manifest.json" with the key "modules" with "type" defined as "qa-extensible-pane" along with the related data.

<b>Sample <i>manifest.json</i></b>
```
{
    "version": "0.0.1",
    "modules": [
        {
            "type": "qa-extensible-pane",
            "displayName": "Custom Links Pane",
            "name": "custom-links-pane",
            "iconUrl": "orange.png",
            "frameUrl": "frame_data/index.html",
            "position": "right"
        }
    ]
}
```
---
