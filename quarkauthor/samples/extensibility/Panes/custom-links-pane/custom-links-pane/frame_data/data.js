// Set global variables for API contxt and qaCore
const context = "/components/api/v1";
const qaCore = window.parent.QA.engine.qaCore;

// Method to call server API to get asset relations data and cal createList with the data.
async function getData () {
    const assetId = getAssetId();
    if (assetId !== -1) {
        const response = await fetch(`${context}/assetservice/assetrelations/${assetId}`);
        const data = await response.json();
        createList(data);
    }
}

function refreshData() {
    document.getElementById("accordionFlushParent").innerHTML = "";
    document.getElementById("accordionFlushChildren").innerHTML = "";
    getData();
}

// Method to build UI using the data received from the assetRelations API
async function createList(data) {
    document.getElementById("childrenNumber").innerHTML = `(${data.childRelations.length})`;
    document.getElementById("parentNumber").innerHTML = `(${data.parentRelations.length})`;
    
    if (data.parentRelations && data.parentRelations.length > 0) {
        let parentItemIndex = 1;
        
        for (const item of data.parentRelations) {
            let itemContainer =  document.createElement("div");
            itemContainer.classList.add("accordion-item");
            itemContainer.style.width = "90%";
    
            let header = document.createElement("span");
            header.classList.add("accordion-header", "link-header");
    
            let nameContainer = document.createElement("span");
            nameContainer.style.flex = "1";
    
            let linkName = document.createElement("span");
            linkName.innerHTML = item.relatedAssetMeta.name;
            linkName.setAttribute("style", "font-size: 14px;");
            nameContainer.appendChild(linkName);
    
            if (await isSmartContent(item.relatedAssetMeta.id)) {
                let editIcon = document.createElement("span");
                editIcon.classList.add("material-icons");
                editIcon.innerHTML = "mode";
                editIcon.addEventListener("click", function() {
                    editSmartContent(item.relatedAssetMeta.id, item.relatedAssetMeta.collectionClass.name);
                }, false);
                editIcon.setAttribute("style", "font-size: 14px; margin-left: 5px; color: initial; cursor: pointer;");
                nameContainer.appendChild(editIcon);
            }
    
            let button = document.createElement("button");
            button.classList.add("accordion-button", "collapsed");
            button.setAttribute("data-bs-toggle", "collapse");
            button.setAttribute("data-bs-target", `#flush-collapse-${parentItemIndex}`);
            button.setAttribute("aria-expanded", "false");
            button.setAttribute("aria-controls", `flush-collapse-${parentItemIndex}`);
            button.setAttribute("style", "padding: 10px 0px; border: 0; width: fit-content");
    
            let metaContainer = document.createElement("div");
            metaContainer.classList.add("accordion-collapse", "collapse");
            metaContainer.setAttribute("id", `flush-collapse-${parentItemIndex}`);
            metaContainer.setAttribute("aria-labelledby", `flush-heading-${parentItemIndex}`);
            metaContainer.setAttribute("style", "border: 0");
    
            let metaBody = document.createElement("div");
            metaBody.classList.add("accordion-body", "meta-body");
            
            thumbnail = document.createElement("img");
            thumbnail.setAttribute("src", `${item.relatedAssetMeta.thumbnailUrl}`);
            thumbnail.setAttribute("style", "height: 60px; display: inline-block; margin-top: -25px");
    
            text = document.createElement("div");
            text.setAttribute("style", "display: inline-block; margin-left: 10px");
            text.innerHTML = `<b>Id:</b> ${item.relatedAssetMeta.id}<br><b>Content Type:</b> ${item.relatedAssetMeta.collectionClass.name}
                                <br><b>Ver.</b> ${item.relatedAssetMeta.majorVersion}.${item.relatedAssetMeta.minorVersion}`
    
            metaBody.appendChild(thumbnail);
            metaBody.appendChild(text);
    
            metaContainer.appendChild(metaBody);
    
            header.appendChild(nameContainer);
            header.appendChild(button);
    
            itemContainer.appendChild(header);
            itemContainer.appendChild(metaContainer);
    
            document.getElementById("accordionFlushParent").appendChild(itemContainer);
    
            parentItemIndex++;  
        }
    }

    if (data.childRelations && data.childRelations.length > 0) {
        let childItemIndex = 1;
        
        for (const item of data.childRelations) {

            const _documenReferences = _getChildDocumentReferences();
            const _dcoumentInfo = _documenReferences.find((_reference) =>
                getAssetIdFromUri(_reference.getURI()) === item.relatedAssetMeta.id);
            const domId = this._getDomElementId(_dcoumentInfo.ck$);
            item["contentBookmark"] = domId;

            let itemContainer =  document.createElement("div");
            itemContainer.classList.add("accordion-item");
            itemContainer.style.width = "90%";

            let header = document.createElement("span");
            header.classList.add("accordion-header", "link-header");

            let nameContainer = document.createElement("span");
            nameContainer.style.flex = "1";

            let linkName = document.createElement("span");
            linkName.innerHTML = item.relatedAssetMeta.name;
            linkName.addEventListener("click", function(event) {
                scrollLinkInDocument(item);
            }, false);
            linkName.classList.add("child-link-name")
            nameContainer.appendChild(linkName);
            
            if (await isSmartContent(item.relatedAssetMeta.id)) {
                let editIcon = document.createElement("span");
                editIcon.classList.add("material-icons");
                editIcon.innerHTML = "mode";
                editIcon.addEventListener("click", function(event) {
                    editSmartContent(item.relatedAssetMeta.id, item.relatedAssetMeta.collectionClass.name);
                }, false);
                editIcon.setAttribute("style", "font-size: 14px; margin-left: 5px; color: initial; cursor: pointer;");
                nameContainer.appendChild(editIcon);
            }
            
            let button = document.createElement("button");
            button.classList.add("accordion-button", "collapsed");
            button.setAttribute("data-bs-toggle", "collapse");
            button.setAttribute("data-bs-target", `#flush-collapse-${childItemIndex}`);
            button.setAttribute("aria-expanded", "false");
            button.setAttribute("aria-controls", `flush-collapse-${childItemIndex}`);
            button.setAttribute("style", "padding: 10px 0px; border: 0; width: fit-content");
            

            let metaContainer = document.createElement("div");
            metaContainer.classList.add("accordion-collapse", "collapse");
            metaContainer.setAttribute("id", `flush-collapse-${childItemIndex}`);
            metaContainer.setAttribute("aria-labelledby", `flush-heading-${childItemIndex}`);
            metaContainer.setAttribute("style", "border: 0");

            let metaBody = document.createElement("div");
            metaBody.classList.add("accordion-body", "meta-body");
            
            thumbnail = document.createElement("img");
            thumbnail.setAttribute("src", `${item.relatedAssetMeta.thumbnailUrl}`);
            thumbnail.setAttribute("style", "height: 60px; display: inline-block; margin-top: -25px");

            text = document.createElement("div");
            text.setAttribute("style", "display: inline-block; margin-left: 10px");
            text.innerHTML = `<b>Id:</b> ${item.relatedAssetMeta.id}<br><b>Content Type:</b> ${item.relatedAssetMeta.collectionClass.name}
                                <br><b>Ver.</b> ${item.relatedAssetMeta.majorVersion}.${item.relatedAssetMeta.minorVersion}`

            metaBody.appendChild(thumbnail);
            metaBody.appendChild(text);

            metaContainer.appendChild(metaBody);

            header.appendChild(nameContainer);
            header.appendChild(button);

            itemContainer.appendChild(header);
            itemContainer.appendChild(metaContainer);

            document.getElementById("accordionFlushChildren").appendChild(itemContainer);

            childItemIndex++;  
        }
    }
}

// Method to edit the document in a new window when edit icon is pressed in case the selected asset is a smart content
function editSmartContent(assetId, contentType) {
    window.open(`/#/edit?uri=qpp://assets/${assetId}` +
        `&contentType=${contentType}`, "_blank"
    );
}

// Scroll to the lcoation of the linked asset when the asset name is clicked in the pane.
function scrollLinkInDocument(link) {
    if (link.contentBookmark) {
        qaCore.focusToElement(link.contentBookmark, false);
    }
}

// Get and return document references from the root element.
function _getChildDocumentReferences() {
    // get root element
    const _rootElement = qaCore.getRootSectionElement();
    // get referenes under root section element
    return qaCore.getReferences(_rootElement);
}

// Get the element ID for a particular element.
function _getDomElementId(element) {
    if (element.getId()) {
        return element.getId();
    } else {
        return this._getDomElementId(element.getParent());
    }
}

// Method to check if a given asset is smart content
async function isSmartContent(assetId) {
    const response = await fetch(`/api/v1/assetService/getAttributeValuesByName?assetId=${assetId}&attributes=Content Type Hierarchy`);
    const contentTypeHierarchyAttr = await response.json();
    const contentTypeHierarchyStr = contentTypeHierarchyAttr[0].attributeValue.value;
    const contentTypeHierarchyArr = contentTypeHierarchyStr.split(";");
    if (contentTypeHierarchyArr.includes("Structured Content") && contentTypeHierarchyArr.includes("Smart Content")
        && contentTypeHierarchyArr.includes("Document") || (contentTypeHierarchyArr.includes("Section"))) {
        return true;
    } else {
        return false;
    }
}

// Get asset ID of the currently opened docuemnt from the frame URL params.
function getAssetId () {
    let url = window.location.search.substring(1); //get rid of "?" in querystring
    let pArr = url.split("="); //get key-value pairs
    if (pArr[0] == "assetId") {
        return pArr[1]; 
    }      
}

// Get the asset Id from a given sourceUri
function getAssetIdFromUri(sourceUri) {
    const uri = sourceUri.split("#")[0]; // Remove fragment part
    const assetIdVersionArray = uri.split("?");
    const assetInfo = assetIdVersionArray[0].split("/");
    if (assetInfo != null && assetInfo.length > 1) {
        const assetIdStr = assetInfo[assetInfo.length - 1];
        const assetId = parseInt(assetIdStr, 10);
        if (!isNaN(assetId)) {
            return assetId;
        }
    }
    return -1;
}
