# Custom Toolbar

The Custom Toolbar extensibility allows you to add additional toolbar on top of the authoring canvas to accomodate additional buttons.

To add Custom Toolbar extensibility to Quark Author: 

1. Create a folder __feature__ in the directory **qaservice/ext** in the s3 bucket.
2. Create a folder __toolbar__ in the folder __feature__.
3. Add the Callback Handler plugin to the folder __toolbar__. 

The name of the folder __toolbar__ must be the same as you have specified in the file <i>qa-config.json</i> under the property "extensions."

<b>Sample <i>qa-config.json</i></b> (available for every content type)
```
"extensions": [
    "toolbar"
]
```

The folder __toolbar__ also must contain the file "manifest.json" with the key "modules" with "type" defined as "qa-extensible-toolbar" along with the related data.

<b>Sample <i>manifest.json</i></b>
```
{
    "version": "0.0.1",
    "modules": [
        {
            "type": "qa-extensible-toolbar",
            "name": "toolbar",
            "frameUrl": "toolbar.html",
            "toolbarHeight": 50
        }
    ]
}
```
---
