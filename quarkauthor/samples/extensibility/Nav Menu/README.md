# Custom Navigation Menu

The Custom Navigation Menu extensibility allows you to add menu and menu items in a navigation tree.

To add Custom Navigation Menu to Quark Author: 

1. Create a folder __feature__ in the directory **qaservice/ext** in the s3 bucket.
2. Create a folder __nav__ in the folder __feature__.
3. Add the Callback Handler plugin to the folder __nav__. 

The name of the folder __nav__ must be the same as you have specified in the file <i>qa-config.json</i> under the property "extensions."

<b>Sample <i>qa-config.json</i></b> (available for every content type)

```
"extensions": [
    "nav"
]
```
The folder __nav__ also must contain the file "manifest.json" with the key "modules" with "type" defined as "qa-nav-menu" along with the related data.

<b>Sample <i>manifest.json</i></b>
```
{
    "version": "0.0.1",
    "modules": [
        {
            "type": "qa-nav-menu",
            "displayName": "Dialog 1",
            "name": "dialog1",
            "iconUrl": "orange.png",
            "show": true,
            "actionType": "dialog",
            "frameUrl": "https://example.com/",
            "showInReadOnly": false,
            "showInAllSections": true,
            "menu": "root",
            "hasChildren": false,
            "dialogConfig": {
                "height": 500,
                "width": 700
            }
        }
    ]
}
```
---
