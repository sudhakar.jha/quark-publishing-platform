# Quark Author Configuration
You can configure Quark Author to function based on your requirements. Make the changes in the following configuration files to implement the required functions: 
* S3/qaservice/conf/app-config.json
* S3/qaservice/conf/structure/Smart Document/qa-config.json

## Configurations in app-config.json
### Map a query for document templates
```
{
"documenttemplate": [
{
"contenttype": "Document Content Type",
"templatequery": " Starter Documents” //Shared query name saved in workspace for templates
}
]}
```
### Show/hide template meta while initiating document
```
{
"documenttemplate": [
{
"contenttype": "Document Content Type",
"showTemplateMeta": false //Default value is true. Set to false for hiding meta while initiating document
}
]}
```
### Show/hide a document Content Type in new Document menu
```
{
"documenttemplate": [
{
"contenttype": "Document Content Type",
"enableNew": false //Default value is true. Set to false for hiding content type from new document menu
}
]}
```
### Enable selecting older versions of an asset in Asset Picker dialog
```
{
"applicationsettings": {
"enableVersionSelectOnAssetPicker": true //Default value is false. Set to true for linking to older versions of asset in asset picker dialog
}
}
```
## Configurations in qa-config.json
### Hide a toolbar tab or item from ribbon
```
"toolbar": {
"mainTabs": [
{
"id": "home",
"label": "Home",
"type": "main",
"mode": "any",
"items": [
"ParaStyles" //This config enables only Paragraph styles and Home tab. Toolbar tabs or items can be added or removed from this configuration to control in ribbon.
]
}
]}
```
### Specify note types and buttons to be shown in Notes pane
```
"notesPane": [
"refresh",
"FootNote",
"EndNote",
"Citation",
"table-notes", //Remove any entry from this list to hide that item from notes pane.
"section-notes",
"delete"
]
```
### Specify section bursting rules
```
"sectionburstingconfig": {
"burstingrules": [
{
"contentType": "Smart Section",
"sectionTypePath": "/document/section",
"assetBrowserId": "SMART_SECTION_BROWSER"
}
]
}
```
### Specify block bursting rules
```
"blockburstingconfig":{
"burstingrules":[
{
"contentType": "Box",
"assetBrowserId": "BOX_BROWSER",
"selectors": [{
"elementName": "region",
"elementType": "box"
}]
},{
"contentType": "Smart Table",
"assetBrowserId": "STRUCTURED_TABLE_BROWSER",
"selectors": [{
"elementName": "table"
}]
}]}
```
### Specify default proofreading language
```
"editorconfigs": {
"ckeditorconfigs":[{
"name": "proofreader_defaultLanguage",
"value": "en-US"
}]}
```
### Auto start-up for proofreading
```
"editorconfigs": {
"ckeditorconfigs":[{
"name": "proofreader_autoStartup",
"value": false //Default value is true. Set to false to disable auto startup for proofreader
}]}
```
### Auto start-up for tracking
```
"editorconfigs": {
"ckeditorconfigs":[{
"name": "tracking_autoStartup",
"value": true //Default value is false. Set to true to enable auto startup for tracking
}]}
```
### Enable DOM Optimization while inserting table from excel
```
"editorconfigs": {
"ckeditorconfigs":[{
"name": " reference_enableDOMOptimization ",
"value": true //Default value is false. Set to true to enable DOM Optimization
}]}
```
### Specify minimum number of rows to display on canvas when DOM Optimization is enabled
```
"editorconfigs": {
"ckeditorconfigs":[{
"name": " reference_minimumPlaceholderRows",
"value": “10”
}]}
```
### Hide default panes
```
"panes": [
"preview",
"trackChanges",
"notes",
“comments”,
"properties", //Remove any pane from this list to hide it in quark author
"history",
"link",
"workflow",
"statistics",
"navigation"
]
```
### Checkin an asset to default collection
```
"features": {
"attributemappings": {
"attributemapping": [
{
"contentType": "Smart Citation",
"attributes": [
{
"name": "Collection Path",
"value": "Home/Parent/Child",
"indexingOption": "INITIAL_VERSION"
}
]}]}}
```
### Map a widget for a bodydiv
```
"editorconfigs": {
"ckeditorconfigs": [
{
"name": "canvas_WidgetToBodydivMapping",
"value": [
{
"widgetName": "custom-figure",
"bodydivType": "custom-figure" //custom plugin
}
]
}]}
```
### Maintain style while copy-pasting table from Excel
```
"editorconfigs": {
"ckeditorconfigs": [
{
"name": "excel_maintainStyleOnPaste",
"value": false //Default value is true. Set to false to remove styling while copy pasting content from excel
}]}
```
### Specify changes in Meta pane for meta
```
"features": {
"metapane": {
"metaConfigs": [
{
"metaconfig": {
"name": "lang",
"displayfilters": [
{
"mandatory": true, //Default is false. Set to true to validate a meta value while saving document.
"hidden": false, //Default is false. Set to true to hide a meta at particular location in document
"selectors": [
{
"element": "section",
"elementType": "section",
"sectionTypePath": "/document/section"
}
]
}
]
}
}
```

## Add Panes, Plugins, Widgets, and Navigation menu items
To configure Quark Author to add panes, plugins, widgets, and navigation menu items:
1. Create a folder __ext__ in the directory **qaservice** in the **s3** bucket.
2. Create a folder __feature__ in the folder __ext__.
3. Create a folder for the feature you want to add in the folder feature and name it with the name of the feature. For example, if you want to add left pane, the folder structure should be - qaservice/ext/feature/left_pane. The folder <feature name> contains the manifest.json file which lists the metadata for all the modules (plugins, panes, nav menu items, and so on) for that feature. The manifest file has the properties "version" and "modules". The property "modules" contains the meta for the type of extensibility and has the following attributes:

    1. <b>type</b>: "qa-canvas-plugin" for ck-editor plugin/ "qa-extensible-pane" for the extensible side panes / "qa-nav-menu" for adding navigation menu items, "qa-extensible-toolbar" for adding extensible toolbar.
    2. <b>name</b>: A unique name of the feature. For plugins, the name attribute must match the original name of the plugin.
    3. <b>bundle</b>: The JS bundle location when the feature is a plugin, widget, or a command.
    4. <b>displayName</b>: The name of the plugin icon, the pane heading, or the menu label on the User Interface.
    5. <b>iconUrl</b>: The location of the icon for the panes. This value can be both absolute (http://test.com/icon.png) or relative (/assets/icon.png).
    6. <b>frameUrl</b>: The index URL of the frame in the pane, menu dialog or extensible toolbar on the User Interface. This value can be both - absolute (http://example.com) or relative (frame_data/index.html).
    7. <b>position</b>: The position of a pane (left or right) in the author window.
    8. <b>show</b>: A Boolean value to show/hide the navigation menu options.
    9. <b>action</b>: Name of the action; for example, ckeditor command for actionType command, API endpoint for actionType endpoint. For navigation menu option, when the attribute hasChildren is set to true for an item, you should not specify the property action.
    10. <b>actionType</b>: This attribute specifies the type of action to be performed when you select a navigation menu item.
    11. <b>dialogConfig</b>: This is an optional attribute that specifies the height and width of the dialog box to be opened when you select a navigation menu item. By default, the dimension of the dialog box is 600px X 600px.
    12. <b>showInReadOnly</b>: A Boolean value to show/hide a navigation option when the currently open section is read only.
    13. <b>showInAllSections</b>: A Boolean value to show/hide a navigation option in the all the sections.
    14. <b>menu</b>: This attribute sets the id of the menu where the navigation option should be placed
    15. <b>hasChildren</b>: A Boolean value to specify whether a navigation option has further children item(s) or not.
    16. <b>toolBarHeight</b>: This attribute specifies the height of the extensible toolbar in number of pixels

4. Add the feature name to the "extensions" property array of the qa-config for that respective content type. When you specify the "panes" property explicitly in the qa-config, add the additional pane names to panes array as well.
5. To configure navigation options that are already available, specify the array type property "navMenus" in the qa-config, which takes in menu config object with the following properties:
    * <b>id</b> (string): refers to a unique id for a navigation menu option
    * <b>show</b> (Boolean): to hide/unhide the menu option
    * <b>label</b> (string): refers to the new display name for that option

6. When you specify a plugin as a feature that utilizes a User Interface toolbar button, add the toolbar property to specify the toolbar structure and the position of the new button in the __qa-config__ file.

For more information on developing a custom CKEditor plugin, see <a href="https://ckeditor.com/docs/ckeditor4/latest/guide/plugin_sdk_sample.html">documentation</a>.

---
