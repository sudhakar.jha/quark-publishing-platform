# Custom Tags Plugin

The Custom Tags plugin adds custom tags into the authoring canvas. The plugin allows you to add button in toolbar, invoke dialog, and apply tags in the content.

To add Custom Tags plugin to Quark Author: 

1. Create a folder __feature__ in the directory **qaservice/ext** in the s3 bucket.
2. Create a folder __customTags__ in the folder __feature__.
3. Add the Custom Tags plugin to the folder __customTags__. 

The name of the folder __customTags__ must be the same as you have specified in the file <i>qa-config.json</i> under the property "extensions."

<b>Sample <i>qa-config.json</i></b> (available for every content type)

```
"extensions": [
    "customTags"
]
```

The folder __customTags__ also must contain the file "manifest.json" with the key "modules" with "type" defined as "qa-canvas-plugin" and "bundle" defined as JavaScript file path.

<b>Sample <i>manifest.json</i></b>

```
{
    "version": "0.0.1",
    "modules": [
        {
            "type": "qa-canvas-plugin",
            "name": "customTags",
            "bundle": "customTags/plugin.js"
        }
    ]
}
```
---
