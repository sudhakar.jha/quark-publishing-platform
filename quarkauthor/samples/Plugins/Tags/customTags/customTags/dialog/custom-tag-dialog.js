/**
 * CKEditor custom tag dialog definition. 
 */
CKEDITOR.dialog.add("customTagDialog", (editor) => {
    const items = [];
    // A way to get Quark Autor API Object.
    const qaEditor = editor.qaEditor;
    // Invoke getInlineTypes API to get all inline types.
    for (const _inlineType of qaEditor.getInlineTypes()) {
        const displayText = _inlineType._name;
        items.push([displayText]);
    }

    return {
        title: "Custom Tags",
        minWidth: 400,
        minHeight: 200,
        contents: [{
            id: "tag-panel",
            elements: [{
                type: "select",
                labelLayout: "horizontal",
                id: "tagDD",
                label: "Select Tag",
                widths: [50, 200],
                items: items
            }]
        }],
        onOk: function () {
            const dialog = this;
            const tag = dialog.getValueOf("tag-panel", "tagDD");
            // Invoke applyTag API to apply tag on current selection.
            dialog._.editor.qaEditor.qaStyleManager.applyTag(tag);
        }
    };
});