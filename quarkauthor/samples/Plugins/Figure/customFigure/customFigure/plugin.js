CKEDITOR.plugins.add('customFigure',
{
	init: function (editor) {

		const qaEditor = editor.qaEditor;
		const qaEngine = editor.engine;
		let assetPicker;
		const XMLEDITOR_IMAGE_URL_PREFIX = "/qaservice/attachmentservice/attachment?sourceuri=";
		
		const resourcePath = editor.engine.qaEditor.qaExtensibilityService.getResourcePath("customFigure");
		CKEDITOR.document.appendStyleSheet( CKEDITOR.getUrl( resourcePath + '/styles.css' ) );
		var url = resourcePath + '/placeholder.jpg';
		// var title = com.quark.kestrel.common.CommonStrings.xSelectDlgTitle;
		// Get the asset browser id of assets types to fetch
		// var assetPickerBrowserId = QXmlEditorEx.getAssetPickerBrowserId('PICTURE_BROWSER');
		qaEngine.qaEditor.getAssetPicker().subscribe((_assetPicker ) => {
            assetPicker = _assetPicker;
        });

		addContextMenu(editor);

		function addContextMenu(editor) {
			// Add menu Items to context menu
			if (editor.addMenuItems) {
				// create new group menu.
				editor.addMenuGroup("custom-figure");
				// Add figure title and figure description to context menu
				editor.addMenuItems({
					AddParaAfterCustomFig: {
						label: "Insert Paragraph After",
						command: "insertParaAfterCustomFigure",
						group: "custom-figure",
						order: 573
					},
					CustomFigureDelete: {
						label: "Delete",
						command: "deleteCustomFigure",
						group: "custom-figure",
						order: 3
					}
				});
			}
	
			editor.addCommand("insertParaAfterCustomFigure", {
				exec: insertParaAfterFigure.bind(this),
				readOnlyExec: true
			});

			editor.addCommand("deleteCustomFigure", {
				exec: deleteCustomFigure.bind(this),
			});
	
			if (editor.contextMenu) {
				// Registering a  listener that will be invoked on displaying context menu
				editor.contextMenu.addListener((element, selection) => {
					const widgetChildren = element.getChildren().toArray();
					let customFigureElement;
					widgetChildren.forEach(_element => {
						if (_element.type == CKEDITOR.NODE_ELEMENT && _element.getAttribute("data-qxe-type") == "custom-figure") {
							customFigureElement = _element;
						}
					});
					const menuItemsInfo = {};
					if (customFigureElement) {
						menuItemsInfo.AddParaAfterCustomFig = CKEDITOR.TRISTATE_OFF;
						menuItemsInfo.CustomFigureDelete = CKEDITOR.TRISTATE_OFF;
					}
					return menuItemsInfo;
				});
			}
		}

		function insertParaAfterFigure(editor) {
			const selection = editor.getSelection();
			const range = selection.getRanges(true)[0];
			const widgetElement = selection.getStartElement();
			const widgetChildren = widgetElement.getChildren().toArray();
			let customFigureElement;
			widgetChildren.forEach(_element => {
				if (_element.type == CKEDITOR.NODE_ELEMENT && _element.getAttribute("data-qxe-type") == "custom-figure") {
					customFigureElement = _element;
				}
			});
			if (customFigureElement) {
				const nextSibling = new CKEDITOR.dom.element("div");
				nextSibling.setAttribute("class", "p");
				nextSibling.setAttribute("data-qxe-element", "p");
				nextSibling.appendBogus();
				nextSibling.insertAfter(widgetElement);
				range.moveToElementEditablePosition(nextSibling);
				range.select();
				nextSibling.scrollIntoView(true);
			}
		}

		function getPreviousEditableElement(element) {
			while (element) {
				if (element.getPrevious()) {
					return element.getPrevious();
				}
				element = element.getParent();
			}
			return null;
		}

		function deleteCustomFigure(editor) {
			const selection = editor.getSelection();
			const range = selection.getRanges(true)[0];
			const widgetElement = selection.getStartElement();
			const widgetChildren = widgetElement.getChildren().toArray();
			let customFigureElement;
			widgetChildren.forEach(_element => {
				if (_element.type == CKEDITOR.NODE_ELEMENT && _element.getAttribute("data-qxe-type") == "custom-figure") {
					customFigureElement = _element;
				}
			});
			if (customFigureElement) {
				let prevSibling = getPreviousEditableElement(widgetElement);
				const parentNode = widgetElement.getParent();
				widgetElement.remove();
				if ((parentNode.is("td") || parentNode.is("th")) && parentNode.getChildCount() === 0) {
					// create empty para in cell if figure is only child in cell
					const paraDiv = new CKEDITOR.dom.element("div");
					paraDiv.setAttribute("class", "p");
					paraDiv.setAttribute("data-qxe-element", "p");
					paraDiv.appendBogus();
					parentNode.append(paraDiv);
					prevSibling = paraDiv;
				}
				if (prevSibling) {
					range.moveToElementEditablePosition(prevSibling, true);
					selection.selectRanges([range]);
				}
				qaEngine.qaEventManager.fireEvent("onchange", null);
				qaEngine.qaEventManager.fireEvent("blockDeleted", parentNode);
			}
		}

		 editor.widgets.add( 'custom-figure', {
	            template:
	                '<div class="custom-figure" >' +
	                	'<div data-qxe-ignore="true" class="fig-index" >Figure: </div>' +
	                    '<div data-qxe-ignore="true" class="label">Title</div><div class="sub-title" data-qxe-type="sub-title" data-qxe-element="p" >Title goes here...</div>' +
	                    '<div class="p first" data-qxe-element="p" ><img src="'+ url +'"></div>' +
	                    '<div class="p second" data-qxe-element="p" ><img src="'+ url +'"></div>' +
	                    '<div data-qxe-ignore="true" class="label">Description</div><div class="desc" data-qxe-type="desc" data-qxe-element="p">Description goes here...</div>' +
	                '</div>',

	            editables: {
	                title: {
	                    selector: '.sub-title',
	                    allowedContent: {
							span: {								
								attributes: '*',
								classes: '*'
							},
							ins: {
							    attributes: '*',
							    styles: 'color'
							},
							del: {
							    attributes: '*',
							    styles: 'color'
							},
							'b i u sub sup': true
						}
	                },
	                description: {
	                    selector: '.desc',
	                     allowedContent: {
							span: {								
								attributes: '*',
								classes: '*'
							},
							ins: {
							    attributes: '*',
							    styles: 'color'
							},
							del: {
							    attributes: '*',
							    styles: 'color'
							},
							'b i u sub sup': true
						}
	                }
	            },
	            draggable : false,
	            parts :{
	                title: 'div.sub-title',
	            	desc : 'div.desc'
	        	},
	            requiredContent: 'img',
	            upcast: function( element ) {
	                return element.name == 'div' && element.hasClass( 'custom-figure' );
	            },
	            init : function() {
	            	var indexDiv = this.element.findOne('.fig-index');
	            	if(!indexDiv) {
	            		this.element.append(createFigureIndex(), true);
	            	}
	            	var titleElement = this.element.findOne('.sub-title');
	            	var descElement = this.element.findOne('.desc');
	            	
	            	if(!this.element.findOne('.label')){
            			createLabel(titleElement, 'Title:');
            			createLabel(descElement, 'Description:');
            		}
	            	
	            	var imgList = this.element.find('div.p img').toArray();
					if(imgList.length == 0){
						return;
					}

					var parent1 = imgList[0].getParent();
            		var parent2 = imgList[1].getParent();
            		
					parent1.addClass('first');
            		parent2.addClass('second');
            		
            		parent1.on( 'click', function( evt ) {
	            		selectedElement = this.findOne('img');
	            		// QXmlEditorEx.showAssetPickerDialog(title, assetPickerBrowserId, replaceImageHandler);
						assetPicker.show({ assetFilter: "PICTURE_BROWSER" });
                		handleAssetPickerSelection();
	               	} );
            		parent2.on( 'click', function( evt ) {
	            		selectedElement = this.findOne('img');
	            		// QXmlEditorEx.showAssetPickerDialog(title, assetPickerBrowserId, replaceImageHandler);
						assetPicker.show({ assetFilter: "PICTURE_BROWSER" });
                		handleAssetPickerSelection();
	               	} );            		
	            }
	        } );
		 
		 
		editor.ui.addButton('customFigureBtn',	{
					label: 'Custom Figure',
					command: 'addCustomFigure',
					icon: resourcePath + '/icon.jpg'
			});
		
		editor.addCommand('addCustomFigure', { 
			exec: function() {
				qaEditor.insertBodyDiv('custom-figure');
		} });
		
		function createFigureIndex() {
            var infoP = qaEditor.createElement('div');
            infoP.setAttribute("data-qxe-ignore", "true");
            infoP.setHtml('<div class="fig-index" >Figure : </div>' );
            return infoP;
        }

		function createLabel(sibling, text) {
			if(!sibling) {
				return;
			}
            var label = qaEditor.createElement('div');
            label.setAttribute("data-qxe-ignore", "true");
            label.addClass('label')
            label.setText(text);
            label.insertBefore(sibling);
        }

		function handleAssetPickerSelection() {
			const subscription = assetPicker.selectedAsset.subscribe((asset) => {
				replaceImageHandler(asset.uri);
				assetPicker.close();
			});
	
			assetPicker.hide.subscribe(() => {
				subscription.unsubscribe();
			});
		}
		
		function replaceImageHandler(assetUri) {
			var imageUrl = XMLEDITOR_IMAGE_URL_PREFIX + encodeURIComponent(assetUri);
			imageUrl = imageUrl + '&_dc='+(new Date().getTime());
			// Add data-cke-saved-src otherwise CKEditor adds complete URL if image is copy pasted
			var element = CKEDITOR.dom.element.createFromHtml('<img src="'+imageUrl+'" data-cke-saved-src="'+imageUrl+'" />');
			element.insertBefore(selectedElement);
			selectedElement.remove();
			qaEngine.qaCore.markAsDirty();
		}
	}
});
