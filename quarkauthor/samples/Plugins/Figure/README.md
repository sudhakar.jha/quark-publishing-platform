# Custom Figure

The Custom Figure plugin inserts/deletes custom figure into the authoring canvas. The plugin allows you to add buttons in toolbar, to add custom styles, to insert element in the authoring canvas and to add context menu for the same.

To add Custom Figure plugin to Quark Author: 

1. Create a folder __feature__ in the directory **qaservice/ext** in the s3 bucket.
2. Create a folder __customFigure__ in the folder __feature__.
3. Add the Custom Figure plugin to the folder __customFigure__. 

The name of the folder __customFigure__ must be the same as you have specified in the file <i>qa-config.json</i> under the property "extensions."

<b>Sample <i>qa-config.json</i></b> (available for every content type)

```
"extensions": [
    "customFigure"
]
```

The folder __customFigure__ also must contain the file "manifest.json" with the key "modules" with "type" defined as "qa-canvas-plugin" and "bundle" defined as JavaScript file path.

<b>Sample <i>manifest.json</i></b>
```
{
    "version": "0.0.1",
    "modules": [
        {
            "type": "qa-canvas-plugin",
            "name": "customFigure",
            "bundle": "customFigure/plugin.js"
        }
    ]
}
```
---
