# Callback Handler

The Callback Handler plugin (window.QA.author.qaCallbackManager) registers various callbacks to allow you to perform additional tasks.

To add Callback Handler plugin to Quark Author: 

1. Create a folder __feature__ in the directory **qaservice/ext** in the s3 bucket.
2. Create a folder __callback-handler__ in the folder __feature__.
3. Add the Callback Handler plugin to the folder __callback-handler__. 

The name of the folder __callback-handler__ must be the same as you have specified in the file <i>qa-config.json</i> under the property "extensions."

<b>Sample <i>qa-config.json</i></b> (available for every content type)
    
```
"extensions": [
    "callback-handler"
]
```
The folder __callback-handler__ also must contain the file "manifest.json" with the key "modules" with "type" defined as "qa-canvas-plugin" and "bundle" defined as JavaScript file path.

<b>Sample <i>manifest.json</i></b>

```
{
    "version": "0.0.1",
    "modules": [
        {
            "type": "qa-canvas-plugin",
            "name": "statusValidation",
            "bundle": "statusValidation/plugin.js"
        }
    ]
}
```
---
