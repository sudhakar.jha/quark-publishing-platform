package com.quark.qpp.asset.services.app;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter;

/**
 * In this class Spring Security Java Configuration is created.
 * The configuration creates a Servlet Filter known as the springSecurityFilterChain which is responsible for all the security within your application. 
 * 
 * WebSecurityConfigurerAdapter provides a default security configuration. Override it's 'configure' APIs to customize security configuration.
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	private static final String AUTH_HEADER = "Authorization";
	
	/**
	 * Override this method to configure the {@link HttpSecurity}.
	 * The default configuration is:
	 * <pre>
	 * http.authorizeRequests().anyRequest().authenticated().and().formLogin().and().httpBasic();
	 * </pre>
	 * 
	 * This method can be used to configure following:
	 * - URLs on which the security configuration is to be applied.
	 * - URLs to be authenticated.
	 * - Specifying security filters to be used for authentication/authorization/errors etc.
	 * - Specify the login page.
	 * - And many others like csrf, cors etc..
	 * 
	 * More details can be found at: 
	 * https://docs.spring.io/spring-security/site/docs/4.0.3.RELEASE/apidocs/org/springframework/security/config/annotation/web/builders/HttpSecurity.html
	 * 
	 * Currently, it is configured to do following:
	 * - Configured the HttpSecurity for all URLs
	 * - Added RequestHeaderAuthenticationFilter - {@link #getJwtAuthenticationFilter()}
	 * - Specified the custom Authentication Provider which will validate the JWT token.
	 * - Disabled csrf authentication which is enabled by default. 
	 *   Enabling csrf does not work for REST APIs as inclusion of additional token in request parameter/header not possible as per current design.
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {		
		http
		.authorizeRequests().anyRequest().authenticated().and()
		.addFilter(getJwtAuthenticationFilter())
		.authenticationProvider(getJwtAuthenticationProvider())
		/*
		 *  Since REST APIs are stateless, security persistence by means of HTTP session is not required. JWT token is required to be sent along with each request Header.
		 *  Spring Security will never create an HttpSession and it will never use it to obtain the SecurityContext when the policy is set to STATELESS.
		 */
		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()  
		.csrf().disable()
		.headers().disable(); //https://www.baeldung.com/spring-security-cache-control-headers
	}
	
	/**
	 * Initializes JwtAuthenticationProvider bean. This provider is called by authentication manager to validate the JWT.
	 * @return JwtAuthenticationProvider bean.
	 */
	@Bean
	public JwtAuthenticationProvider getJwtAuthenticationProvider( ) {
		return new JwtAuthenticationProvider();
	}
	
	/**
	 * Initializes RequestHeaderAuthenticationFilter bean. This filter fetches the JWT from request's Authorization Header and
	 * sends it to authentication manager {@link ProviderManager} to authenticate it.
	 * @return RequestHeaderAuthenticationFilter bean.
	 * @throws Exception
	 */
	@Bean
	public RequestHeaderAuthenticationFilter getJwtAuthenticationFilter() throws Exception {
		RequestHeaderAuthenticationFilter jwtFilter = new RequestHeaderAuthenticationFilter();
		jwtFilter.setPrincipalRequestHeader(AUTH_HEADER);
		// While creating authentication filter, it is mandatory to explicitly specify the
		// authentication manager. So we are using default authentication manager as we
		// do not have anything to override at manager level.
		jwtFilter.setAuthenticationManager(this.authenticationManager());
		
		// If the JWT header is not there in the request then exception is not thrown
		// and the request is propagated as-is to the underlying layer for existing behavior to keep on working.
		// We may choose to throw proper exception in future. 
		jwtFilter.setExceptionIfHeaderMissing(false);
		return jwtFilter;
	}

}