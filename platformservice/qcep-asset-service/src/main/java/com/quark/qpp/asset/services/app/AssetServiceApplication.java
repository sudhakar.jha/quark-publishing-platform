package com.quark.qpp.asset.services.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@ComponentScan(basePackages={"com.quark.qpp"} )
@SpringBootApplication
public class AssetServiceApplication{
	public static void main(String[] args) {
		SpringApplication.run(AssetServiceApplication.class, args);
	}
}
