package com.quark.qpp.ext.asset.service.remote.v1;

import java.util.Map;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.quark.qpp.common.dto.NameId;
import com.quark.qpp.common.exceptions.QppServiceException;
import com.quark.qpp.core.asset.service.dto.Asset;
import com.quark.qpp.core.attribute.service.dto.AttributeValue;

@RequestMapping("/api/v1/sampleService")
public interface AssetServiceApi {

	@GetMapping("/getNameIdOfAttribute")
	NameId getNameIdOfAttribute(@RequestParam("attributeId") long attributeId) throws QppServiceException;

	@PostMapping(path = "/initCheckin", consumes = {MediaType.APPLICATION_JSON_VALUE},   produces = { MediaType.APPLICATION_JSON_VALUE})
	StringBuffer initCheckin(Asset asset,
			@RequestParam("createMinorVersion") boolean createMinorVersion);

	@GetMapping("/assetMetaData")
	StringBuffer assetMetaData(@RequestParam("assetId") long assetId, @RequestParam(value = "majorVersion",required = false, defaultValue = "0") long majorVersion, @RequestParam(value = "minorVersion", required = false,  defaultValue = "0") long minorVersion )
			throws QppServiceException;
	
	@PostMapping(path = "/checkOutAsset", consumes = {MediaType.APPLICATION_JSON_VALUE},   produces = { MediaType.APPLICATION_JSON_VALUE})
	Asset checkOutAsset(@RequestParam("assetId") long assetId, @RequestBody AttributeValue[] attributeValues)
			throws QppServiceException;
	
	@PostMapping(path = "/publishAsset", consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE},   produces = { MediaType.APPLICATION_JSON_VALUE})
	StringBuffer publishAssest( long assetId, String publishingChannelId,  Map<String, String> publishingParameters)
			throws QppServiceException;
	
	@PostMapping(path = "/deliverAsset",  consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE},   produces = { MediaType.APPLICATION_JSON_VALUE})
	StringBuffer deliverAsset(String uri, String deliveryChannelId, Map<String, String> deliveryParameters)
			throws QppServiceException;
	
	@PostMapping(path = "/publishAndDeliverBasedOnURI", consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE},   produces = { MediaType.APPLICATION_JSON_VALUE})
	StringBuffer publishAndDeliverBasedOnURI(String uri, String publishingChannelId, String deliveryChannelId, Map<String, String> deliveryParameters)
			throws QppServiceException;
	
	@PostMapping(path = "/updateAssetMetadata", consumes = {MediaType.APPLICATION_JSON_VALUE})
	StringBuffer updateAssetMetadata(@RequestParam("assetId") long assetId,@RequestBody AttributeValue[] attributeValues)
			throws QppServiceException;
	
	@PostMapping("/getAssetHires")
	StringBuffer getAssetHires(@RequestParam("assetId") long assetId, @RequestParam(value = "majorVersion",required = false, defaultValue = "0") long majorVersion, @RequestParam(value = "minorVersion", required = false,  defaultValue = "0") long minorVersion )
			throws QppServiceException;
	
	@PostMapping("/publishOnAssetChange")
	StringBuffer publishOnAssetChange(@RequestParam("assetId") long assetId, @RequestBody AttributeValue[] attributeValues)
			throws QppServiceException;
	
	@GetMapping("/getAssetHiresNew")
	StringBuffer getAssetHiresNew(@RequestParam("assetId") long assetId, @RequestParam(value = "majorVersion",required = false, defaultValue = "0") long majorVersion, @RequestParam(value = "minorVersion", required = false,  defaultValue = "0") long minorVersion)
			throws QppServiceException;
	
	@GetMapping("/getSavedSearchResult")
	StringBuffer getSavedSearchResult(@RequestParam("queryId") long queryId)
			throws QppServiceException;
	
	@GetMapping("/multiChannelPublishOnAssetChange")
	StringBuffer multiChannelPublishOnAssetChange(@RequestParam("assetId") long assetId)
			throws QppServiceException;
	
	@PostMapping(path = "/PublishAssetOnAssetChange_SOP", consumes = {MediaType.APPLICATION_JSON_VALUE})
	StringBuffer PublishAssetOnAssetChange_SOP(@RequestParam("assetId") long assetId,@RequestBody AttributeValue[] attributeValues)
			throws QppServiceException;
}
