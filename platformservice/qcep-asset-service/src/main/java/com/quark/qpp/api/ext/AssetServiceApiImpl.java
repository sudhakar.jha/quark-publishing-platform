package com.quark.qpp.api.ext;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.quark.qpp.common.dto.BooleanValue;
import com.quark.qpp.common.dto.DateTimeValue;
import com.quark.qpp.common.dto.DateValue;
import com.quark.qpp.common.dto.DomainValue;
import com.quark.qpp.common.dto.DomainValueList;
import com.quark.qpp.common.dto.ListResponse;
import com.quark.qpp.common.dto.MeasurementValue;
import com.quark.qpp.common.dto.NameId;
import com.quark.qpp.common.dto.NumericValue;
import com.quark.qpp.common.dto.TextValue;
import com.quark.qpp.common.dto.TimeValue;
import com.quark.qpp.common.dto.Value;
import com.quark.qpp.common.exceptions.QppServiceException;
import com.quark.qpp.core.asset.service.dto.Asset;
import com.quark.qpp.core.asset.service.dto.AssetUploadInfo;
import com.quark.qpp.core.asset.service.exceptions.AssetNotFoundException;
import com.quark.qpp.core.asset.service.remote.v1.AssetServiceStub;
import com.quark.qpp.core.attribute.service.constants.AttributeModificationLevels;
import com.quark.qpp.core.attribute.service.constants.AttributeValueTypes;
import com.quark.qpp.core.attribute.service.constants.DefaultAttributes;
import com.quark.qpp.core.attribute.service.dto.Attribute;
import com.quark.qpp.core.attribute.service.dto.AttributeDomain;
import com.quark.qpp.core.attribute.service.dto.AttributeValue;
import com.quark.qpp.core.attribute.service.dto.DomainValueListPreferences;
import com.quark.qpp.core.attribute.service.dto.DomainValuePreferences;
import com.quark.qpp.core.attribute.service.exceptions.InvalidAttributeException;
import com.quark.qpp.core.attribute.service.remote.v1.AttributeDomainServiceStub;
import com.quark.qpp.core.attribute.service.remote.v1.AttributeServiceStub;
import com.quark.qpp.core.collection.service.dto.JobJacket;
import com.quark.qpp.core.collection.service.remote.v1.CollectionServiceStub;
import com.quark.qpp.core.content.service.dto.ContentTypeInfo;
import com.quark.qpp.core.content.service.remote.v1.ContentStructureServiceStub;
import com.quark.qpp.core.publishing.service.dto.PublishingChannel;
import com.quark.qpp.core.publishing.service.dto.PublishingUploadInfo;
import com.quark.qpp.core.publishing.service.exceptions.InvalidChannelException;
import com.quark.qpp.core.publishing.service.exceptions.PublishingFailedException;
import com.quark.qpp.core.publishing.service.remote.v1.PublishingServiceStub;
import com.quark.qpp.core.query.service.constants.DefaultDisplayModes;
import com.quark.qpp.core.query.service.constants.ExploreModeTypes;
import com.quark.qpp.core.query.service.dto.QueryDisplay;
import com.quark.qpp.core.query.service.dto.QueryResultElement;
import com.quark.qpp.core.query.service.remote.v1.QueryServiceStub;
import com.quark.qpp.core.workflow.service.exceptions.StatusNotFoundException;
import com.quark.qpp.core.workflow.service.exceptions.WorkflowNotFoundException;
import com.quark.qpp.core.workflow.service.remote.v1.WorkflowServiceStub;
import com.quark.qpp.ext.asset.service.remote.v1.AssetServiceApi;
import com.quark.qpp.utills.Utills;

@RestController
public class AssetServiceApiImpl implements AssetServiceApi {

	@Autowired
	private AttributeServiceStub attributeServiceApi;

	@Autowired
	private AssetServiceStub assetServiceApi;

	@Autowired
	private AttributeDomainServiceStub attributeDomainServiceApi;

	@Autowired
	private WorkflowServiceStub workflowServiceApi;

	@Autowired
	private CollectionServiceStub collectionServiceApi;

	@Autowired
	private PublishingServiceStub publishingServiceApi;

	@Autowired
	private QueryServiceStub queryServiceApi;

	@Autowired
	private ContentStructureServiceStub contentStructureServiceApi;

	@Override
	public NameId getNameIdOfAttribute(long attributeId) throws QppServiceException {
		Attribute attribute = attributeServiceApi.getAttribute(attributeId);
		NameId attributeNameId = new NameId();
		attributeNameId.setId(attribute.getId());
		attributeNameId.setName(attribute.getName());
		// custom logic
		return attributeNameId;
	}

	/*********************************************************************
	 * Sample Name: CheckInNewAsset Objective : The sample will checkIn a local
	 * file, specified in variable "filePath", to QPP as new asset. Prerequisite:
	 * Collection Path, Worklflow name, Status name and route to user for the asset
	 * need to specified in the variables "collectionPath", "workflowName",
	 * "statusName" and "routeToUser" respectively. Set the createMinorVersion flag
	 * depending upon the revision control settings on specified collection. The
	 * asset will be checked in with the same name as that of file. Note: To provide
	 * any attribute value for an attribute of user defined heirarchial domain type,
	 * use ';' as separator. example - "place":"World;Asia;India;Punjab"
	 **************************************************************************/
	@Override
	public StringBuffer initCheckin(Asset asset, boolean createMinorVersion) {
		// Specify the path of the file that needs to be checked-in
		String filePath = "D:/target/Fun vacation presentation.pptx";

		// Specify complete collection path, from root collection, in which assets
		// should be checked-in.
		// Collection names in collection path should be separated by "/" .
		String collectionPath = "Home/API_Created";

		// Content type for the asset to be checked in.
		String contentType = "System;Asset";

		String assetName = "TestProject";

		// Specify name of Workflow which should be assigned to new QPP asset.
		String workflowName = "New Workflow1";

		// Specify name of the status in which file should be checked-in.
		// This status should belong to the workflow specified above.
		String statusName = "New Status 1";

		// Specify the user to whom the asset has to be routed to.
		String routeToUser = "Admin";

		// Set the flag depending on the revision control settings for the given
		// collection
		createMinorVersion = false;

		// Declare a string buffer
		StringBuffer sb = new StringBuffer();

		String filename = null;
		String filextension;
		FileInputStream fileInputStream = null;
		try {
			File file = new File(filePath);

			filename = file.getName();

			// get the file extension
			filextension = Utills.getFileExtension(filename);

			fileInputStream = new FileInputStream(file);

			Map<String, String> attributeValuesMap = new HashMap<>();
			attributeValuesMap.put("name", assetName);
			attributeValuesMap.put("workflow", workflowName);
			attributeValuesMap.put("status", statusName);
			attributeValuesMap.put("file extension", filextension);
			attributeValuesMap.put("content type", contentType);
			attributeValuesMap.put("original filename", filename);
			attributeValuesMap.put("routed to", routeToUser);
			attributeValuesMap.put("original filename", collectionPath);

			// create com.quark.qpp.core.attribute.service.dto.AttributeValue Array from the
			// above attributeValuesMap.
			AttributeValue[] attributeValuesArray = createAttributeValueArray(attributeValuesMap);

			Asset anAsset = new Asset();
			asset.setAttributeValues(attributeValuesArray);
			long assetId = 0;
			// initiate check in of new asset using assetServiceApi's initNewCheckin API.
			AssetUploadInfo assetUploadInfo = assetServiceApi.initNewCheckin(anAsset, createMinorVersion);
			if (assetUploadInfo != null) {
				sb.append("\nThe asset context =" + assetUploadInfo.getContextId() + " url = "
						+ assetUploadInfo.getUploadUrl());
				assetId = assetServiceApi.getAssetStatus(assetUploadInfo.getContextId()).getAssetId();
			}
			// blobLocalStreamingService.upload(assetUploadInfo.getUploadUrl(),
			// fileInputStream);

			sb.append("\nThe asset " + "'" + filename + "' assettId =" + assetId
					+ "  has been successfully checked into QPP Server.");
		} catch (Exception exception) {
			sb.append("\nError : The asset " + "'" + filename + "'" + " could not be checked into QPP Server.\n");
			sb.append(exception);
		} finally {
			if (fileInputStream != null) {
				try {
					fileInputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return sb;
	}

	/*********************************************************************
	 * Sample Name: GetAssetMetadata Objective : The sample will return the complete
	 * metadata of the asset with the given id and asset version. Prerequisite: -
	 * Variable "assetId" should refer to the QPP Server's Asset. - Variable
	 * "majorVersion" should refer to the required Asset's major version. Provide
	 * null to fetch metadata of latest version. - Variable "minorVersion" should
	 * refer to the required Asset's minor version. Provide null to fetch metadata
	 * of latest version.
	 **************************************************************************/
	@Override
	public StringBuffer assetMetaData(long assetId, long majorVersion, long minorVersion) throws QppServiceException {
		Asset asset = null;
		StringBuffer stringBuffer = new StringBuffer();
		try {
			if (majorVersion > 0 || minorVersion > 0) {
				asset = assetServiceApi.getAssetVersion(assetId, majorVersion, minorVersion);
			} else {
				asset = assetServiceApi.getAsset(assetId);
			}
			AttributeValue[] attributeValues = asset.getAttributeValues();
			ObjectMapper mapper = new ObjectMapper();
			// Converting the Object to JSONString
			String jsonAttributeValues = mapper.writeValueAsString(attributeValues);
			stringBuffer.append("The metadata of asset " + assetId + " :\n");
			stringBuffer.append("-------------------------------------\n");
			stringBuffer.append("" + jsonAttributeValues);
		} catch (Exception exception) {
			stringBuffer.append("Error while getting the asset metadata.");
			stringBuffer.append(exception);
		}
		return stringBuffer;
	}

	/*********************************************************************
	 * CheckOutAsset : This sample will mark the given asset as checked out.
	 * Prerequisite: - Variable "assetId" should refer to the QPP Server's Asset to
	 * be checked out. - Varibale "targetFolder" should refer to the location where
	 * the asset highres shall be copied when required. Target folder should have
	 * write permissions. /
	 *******************************************************************/
	@Override
	public Asset checkOutAsset(long assetId, AttributeValue[] attributeValues) throws QppServiceException {
		/*
		 * Specify the folder where the asset is to be copied on checkout.
		 */
		String targetFolder = "D:/target";
		String assetName = getAssetNameFromId(assetId);

		// declare attribute map
		Map<String, String> attributeValuesMap = new HashMap<>();
		attributeValuesMap.put("Checked out file path", targetFolder + "/" + assetName);
		attributeValuesMap.put("Checkedout Application", "QPP SCRIPT MANAGER");

		AttributeValue[] checkoutAttributeValuesArray = createAttributeValueArray(attributeValuesMap);

		// checkout the asset
		return assetServiceApi.checkOut(assetId, checkoutAttributeValuesArray);
	}

	/*********************************************************************
	 * Returns com.quark.qpp.core.attribute.service.dto.AttributeValue Array for the
	 * given two dimensional array of type [object Object]. attributeValuesMap : two
	 * dimensional array of type [object Object] example : var map
	 * ={"name":"sample.jpg","workflow":"abc",...} /
	 *******************************************************************/
	@SuppressWarnings("null")
	private AttributeValue[] createAttributeValueArray(Map<String, String> attributeValues)
			throws StatusNotFoundException, WorkflowNotFoundException, QppServiceException {
		HashMap<String, String> attributeValuesMap = convertKeysToUpperCase(attributeValues);
		AttributeValue attributeValueMap[] = new AttributeValue[attributeValues.size()];
		int count = 0;
		String workflowName = attributeValuesMap.get("WORKFLOW");
		String collectionPath = attributeValuesMap.get("COLLECTION PATH");
		AttributeValue attributeValue;
		for (String key : attributeValuesMap.keySet()) {
			Attribute attribute = attributeServiceApi.getAttributeByName(key);

			attributeValue = new AttributeValue();
			attributeValue.setAttributeId(attribute.getId());
			attributeValue.setType(attribute.getValueType());
			attribute.isMultiValued();
			attributeValue.setMultiValued(attribute.isMultiValued().booleanValue());
			int valueType = attribute.getValueType();
			switch (valueType) {
			case AttributeValueTypes.TEXT: {
				TextValue textValue = new TextValue();
				textValue.setValue(attributeValuesMap.get(key));
				attributeValue.setAttributeValue(textValue);
				break;
			}
			case AttributeValueTypes.DATE: {
				DateValue dateValue = new DateValue();
				dateValue.setValue(attributeValuesMap.get(key));
				attributeValue.setAttributeValue(dateValue);
				break;
			}
			case AttributeValueTypes.TIME: {
				TimeValue timeValue = new TimeValue();
				timeValue.setValue(attributeValuesMap.get(key));
				attributeValue.setAttributeValue(timeValue);
				break;
			}
			case AttributeValueTypes.NUMERIC: {
				NumericValue numericValue = new NumericValue();
				numericValue.setValue(Long.parseLong(attributeValuesMap.get(key)));
				attributeValue.setAttributeValue(numericValue);
				break;
			}
			case AttributeValueTypes.MEASUREMENT: {
				MeasurementValue measurementValue = new MeasurementValue();
				measurementValue.setValue(Double.parseDouble(attributeValuesMap.get(key)));
				attributeValue.setAttributeValue(measurementValue);
				break;
			}
			case AttributeValueTypes.BOOLEAN: {
				BooleanValue booleanValue = new BooleanValue();
				booleanValue.setValue(Boolean.parseBoolean(attributeValuesMap.get(key)));
				attributeValue.setAttributeValue(booleanValue);
				break;
			}
			case AttributeValueTypes.DOMAIN: {
				if (attribute.isMultiValued().booleanValue()) {
					attribute.getDomainValueListPreferences();
				} else {
					attribute.getDomainValuePreferences();
				}
				AttributeDomain domain = null;
				if (attribute.isMultiValued().booleanValue()) {
					DomainValueListPreferences attrPrefs = attribute.getDomainValueListPreferences();
					domain = attributeDomainServiceApi.getDomain(attrPrefs.getDomainId());
				} else {
					DomainValuePreferences attrPrefs = attribute.getDomainValuePreferences();
					domain = attributeDomainServiceApi.getDomain(attrPrefs.getDomainId());
				}
				DomainValue domainValue = new DomainValue();
				if (key.equals("STATUS")) {
					System.out.println("status inside  ");
					if (workflowName == null) {
						System.out.println("Workflow not given for status");
					} else {
						domainValue = workflowServiceApi.getStatusByName(workflowName, attributeValuesMap.get(key));
					}
				} else if (key.equals("JOB JACKET")) {
					if (collectionPath == null) {
						System.out.println("collection not provided for job jacket");
					} else {
						DomainValue collection = attributeDomainServiceApi.getDomainValueFromHierarchy(domain.getId(),
								collectionPath);
						ListResponse<JobJacket> jobJackets = collectionServiceApi
								.getCollectionJobJackets(collection.getId(), valueType);

						for (int i = 0; i < jobJackets.get_value().length; i++) {
							if (jobJackets.get_value()[i].getName().toUpperCase()
									.equals(attributeValuesMap.get(key).toUpperCase())) {
								domainValue = jobJackets.get_value()[i];
								break;
							}
						}
					}
				} else {
					domainValue = attributeDomainServiceApi.getDomainValueFromHierarchy(domain.getId(),
							attributeValuesMap.get(key));
				}

				domainValue.setDomainId(domain.getId());
				if (!attribute.isMultiValued().booleanValue()) {
					attributeValue.setAttributeValue(domainValue);
				} else {
					DomainValueList domainValueList = new DomainValueList();
					DomainValue[] domainValues = null;
					domainValues[0] = domainValue;
					domainValueList.setDomainValues(domainValues);
					attributeValue.setAttributeValue(domainValueList);
				}
				break;
			}
			case AttributeValueTypes.DATETIME: {
				DateTimeValue dateTimeValue = new DateTimeValue();
				dateTimeValue.setValue(attributeValuesMap.get(key));
				attributeValue.setAttributeValue(dateTimeValue);
				break;
			}
			}
			attributeValueMap[count] = attributeValue;
			count++;
		}
		return attributeValueMap;
	}

	/*********************************************************************
	 * converting attribute map keys to upperCase" /
	 *******************************************************************/
	private HashMap<String, String> convertKeysToUpperCase(Map<String, String> attributeValues) {
		HashMap<String, String> map = new HashMap<String, String>();

		for (String key : attributeValues.keySet()) {
			if (key != null)
				map.put(key.toUpperCase(), attributeValues.get(key));
		}
		return map;
	}

	/*********************************************************************
	 * Fetch Asset Name by Attribute Value by Name" /
	 *******************************************************************/
	private String getAssetNameFromId(long assetId)
			throws AssetNotFoundException, InvalidAttributeException, QppServiceException {
		String assetName = null;
		String nameAttributeId = "Name";
		String[] attributeIdsArray = { nameAttributeId };
		AttributeValue[] attributesArray = assetServiceApi.getAttributeValuesByName(assetId, attributeIdsArray);

		for (int i = 0; i < attributesArray.length; i++) {
			if (attributesArray[i].getAttributeName().equalsIgnoreCase(nameAttributeId)) {
				assetName = attributesArray[i].getAttributeName();
				break;
			}
		}

		return assetName;
	}

	/*********************************************************************
	 * Sample Name: PublishAnAsset Objective : The Sample will publish a QuarkXPress
	 * project as PDF. The file generated will have same name as the that of QXP
	 * with desired format as extension. Prerequisite: - Variable "assetName" refers
	 * to the QPP Asset to be published. - Variable "publishingChannelId" refers to
	 * the publishing channel to be used for publishing QXP as PDF - Variable
	 * "publishingParametersMap" refers to the publshing channel parameters -
	 * Variable "destinationFolder" refers to the file system location where
	 * published output is to be retrieved.
	 **************************************************************************/
	@Override
	public StringBuffer publishAssest(long assetId, String publishingChannelId,
			Map<String, String> publishingParameters) throws QppServiceException {
		String destinationFolder = "/tmp";
		String assetName = getAssetNameFromId(assetId);
		StringBuffer stringBuffer = new StringBuffer();
		FileOutputStream fileOutputStream = null;
		if (assetId > 0) {
			try {
				File pdfFile = new File(destinationFolder + "/" + assetName + ".pdf");
				fileOutputStream = new FileOutputStream(pdfFile);

				publishingServiceApi.publishAsset(assetId,
						publishingChannelId, publishingParameters);
				// blobLocalStreamingService.download(assetDonInfo[0].getDownloadUrl(),
				// fileOutputStream);
				stringBuffer.append("\n" + assetName + " has been published as PDF at location " + destinationFolder);
			} catch (Exception exception) {
				stringBuffer.append("Error while publishing the asset with name : " + assetName + "\n");
				stringBuffer.append(exception);
			} finally {
				if (fileOutputStream != null) {
					try {
						fileOutputStream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

		} else {
			stringBuffer.append("Error : Asset with name " + assetName + " doesn't exist in QPP Server.");
		}
		return stringBuffer;
	}

	/*********************************************************************
	 * Sample Name: DeliverAnAsset Objective : The sample will deliver a QPP Asset
	 * using the specified delivery channel. Prerequisite: - Variable "assetPath"
	 * refers to the QPP Asset on the basis of collection path. - Variable
	 * "deliveryChannelId" refers to the id of the delivery channel used to deliver
	 * specified asset. - Variables "exchangeServer", "senderMailId",
	 * "recipientMailId", "mailSubject", "mailMessage" refer to the delivery channel
	 * parameters.
	 **************************************************************************/
	@Override
	public StringBuffer deliverAsset(String uri, String deliveryChannelId, Map<String, String> deliveryParameters)
			throws QppServiceException {
		// Specify asset path of the QPP Asset that is to be delivered.
		String assetPath = "D:/target/Fun vacation presentation.pptx";
		String qppUri = "qpp://assetsbypath/" + assetPath;

		// Specify exchange server
		String exchangeServer = "exchange-server";

		// Specify mail address of the sender.
		String senderMailId = "abc@abc.com";

		// Specify mail address of user to whom the email will be sent
		String recipientMailId = "xyz@xyz.com";

		// Specify subject of the email
		String mailSubject = "QPP Asset delivered.";

		// Specify message of the email
		String mailMessage = "QPP asset : " + assetPath + " has been delivered using sendEmail deliver channel.";

		// Construct delivery channel parameter's map
		Map<String, String> deliveryParametersMap = new HashMap<String, String>();
		deliveryParametersMap.put("EXCHANGE_SERVER", exchangeServer);
		deliveryParametersMap.put("SENDER_EMAIL_ID", senderMailId);
		deliveryParametersMap.put("RECIPIENT_EMAIL_IDS", recipientMailId);
		deliveryParametersMap.put("SEND_AS_ATTACHMENT", "true");
		deliveryParametersMap.put("SUBJECT", mailSubject);
		deliveryParametersMap.put("MESSAGE", mailMessage);

		// Declare a string buffer
		StringBuffer stringBuffer = new StringBuffer();

		try {

			publishingServiceApi.deliverBasedOnURI(qppUri, deliveryChannelId, deliveryParametersMap);
			stringBuffer.append("\n Asset at path : " + assetPath + " has been delivered successfully using "
					+ deliveryChannelId + " delivery channel.");
		} catch (Exception exception) {
			stringBuffer.append("Error while delivering the asset : " + assetPath + "\n");
			stringBuffer.append(exception);
		}
		stringBuffer.toString();

		return stringBuffer;
	}

	/*********************************************************************
	 * Sample Name: PublishAndDeliverAnAsset Objective : The sample will publish a
	 * QuarkXPress project as PDF and the output is delivered using sendEmail
	 * delivery channel. Prerequisite: Variable "assetPath" refers to the QPP Asset
	 * on the basis of its collection path.
	 **************************************************************************/

	@Override
	public StringBuffer publishAndDeliverBasedOnURI(String uri, String publishingChannelId, String deliveryChannelId,
			Map<String, String> deliveryParameters) throws QppServiceException {
		String assetPath = "Home/asset.qxp";
		String qppUri = "qpp://assetsbypath/" + assetPath;

		// Specify exchange server
		String exchangeServer = "exchange-server";

		// Specify mail address of the sender.
		String senderMailId = "abc@abc.com";

		// Specify mail address of user to whom the email will be sent
		String recipientMailId = "xyz@xyz.com";

		// Specify subject of the email
		String mailSubject = "QPP Asset delivered.";

		// Specify message of the email
		String mailMessage = "PDF of QPP asset : " + assetPath + " has been delivered using sendEmail deliver channel.";

		// declare a publishing parameters map. Add parameters required by publishing
		// channel
		Map<String, String> publishingParametersMap = new HashMap<>();
		// publishingParametersMap.put("SPREAD_VIEW", "false");

		// Add parameters required by delivery channel (Use the same map to add
		// publishing and delivery channel parameters)
		publishingParametersMap.put("EXCHANGE_SERVER", exchangeServer);
		publishingParametersMap.put("SENDER_EMAIL_ID", senderMailId);
		publishingParametersMap.put("RECIPIENT_EMAIL_IDS", recipientMailId);
		publishingParametersMap.put("SEND_AS_ATTACHMENT", "true");
		publishingParametersMap.put("SUBJECT", mailSubject);
		publishingParametersMap.put("MESSAGE", mailMessage);

		// Declare a string buffer
		StringBuffer stringBuffer = new StringBuffer();

		try {
			publishingServiceApi.publishAndDeliverBasedOnURI(qppUri, publishingChannelId, deliveryChannelId,
					publishingParametersMap);
			stringBuffer.append("Asset : " + assetPath + " successfully published and delivered.\n");
		} catch (Exception exception) {
			stringBuffer.append("Error while publishing the asset : " + assetPath + "\n");
			stringBuffer.append(exception);
		}
		stringBuffer.toString();

		return stringBuffer;
	}

	/*********************************************************************
	 * Sample Name: UpdateAssetMetadata Objective : The sample will update the
	 * metadata of an asset with a given id. Prerequisite: - Variable "assetId"
	 * should refer to the QPP Server's Asset Id.
	 **************************************************************************/
	@Override
	public StringBuffer updateAssetMetadata(long assetId, AttributeValue[] attributeValues) throws QppServiceException {
		// specify the attribute values to be set
		String newAssetName = "asset.qxp";
		String collectionPath = "Education";
		String workflow = "Publishing Workflow";
		String status = "Draft";
		String routeToUserName = "Admin";

		Map<String, String> newAttributesMap = new HashMap<>();
		newAttributesMap.put("name", newAssetName);
		newAttributesMap.put("collection", collectionPath);
		newAttributesMap.put("workflow", workflow);
		newAttributesMap.put("status", status);
		newAttributesMap.put("routed to", routeToUserName);

		AttributeValue[] attributeValue = createAttributeValueArray(newAttributesMap);
		StringBuffer stringBuffer = new StringBuffer();

		try {
			assetServiceApi.setAttributeValues(assetId, attributeValue);
			stringBuffer.append("Asset metadata updated successfully. ");

		} catch (Exception exception) {
			stringBuffer.append("Error while setting asset metadata. \n");
			stringBuffer.append(exception);
		}
		stringBuffer.toString();

		return stringBuffer;
	}

	/*********************************************************************
	 * Sample Name: GetAssetHighres Objective : The sample will return the content
	 * of a QPP asset, with the given asset id, at the location specified in
	 * variable "targetFolder". Prerequisite: - Variable "assetId" should refer to
	 * the QPP Server's Asset. - Varibale "targetFolder" should refer to the
	 * location where the asset highres is to be copied. Target folder should have
	 * write permissions.
	 **************************************************************************/
	@Override
	public StringBuffer getAssetHires(long assetId, long majorVersion, long minorVersion) throws QppServiceException {
		String targetFolder = "D:/target";
		StringBuffer stringBuffer = new StringBuffer();
		FileOutputStream fileOutputStream = null;
		String assetName = null;
		try {
			assetName = getAssetNameFromId(assetId);

			File highResFile = new File(targetFolder, assetName);

			// declare an output stream where the asset highres will be written
			fileOutputStream = new FileOutputStream(highResFile);
			if (majorVersion > 0 || minorVersion > 0) {
				assetServiceApi.getAssetDownloadInfo(assetId, majorVersion, minorVersion);
			} else {
				assetServiceApi.getAssetDownloadInfo(assetId, 0, 0);
			}

			// blobLocalStreamingService.download(assetDownloadInfo.getDownloadUrl(),
			// fileOutputStream);

			stringBuffer.append(assetName + " successfully copied at location : " + targetFolder);
		} catch (Exception exception) {
			stringBuffer.append("Error : Couldnot get highres of asset:" + assetName + "\n");
			stringBuffer.append(exception);
		} finally {
			if (fileOutputStream != null) {
				try {
					fileOutputStream.close();
				} catch (IOException e) {					
					e.printStackTrace();
				}
			}
		}
		stringBuffer.toString();
		return stringBuffer;
	}

	/*********************************************************************
	 * 
	 * Sample Name: PublishAssetOnStatusChange Objective : The sample will publish
	 * an asset on status change. The provided channel will be used for publishing.
	 * The file generated will have the provided asset name. The asset status shall
	 * be changed to the status desired to be set on published asset. The published
	 * asset will be checked in the platform in the provided collection.
	 * Prerequisite: Script should be configured as event based with "OBJECT TYPE"
	 * as ASSET and "CHANGE TYPE" as ASSET CHANGED. - Variable "assetId" refers to
	 * the id of the asset to be published. - Variable "desiredSourceWorkflow"
	 * refers to the desired workflow value for an asset to be published. - Variable
	 * "desiredSourceStatusName" refers to the desired status value for an asset to
	 * be published. - Variable "publishedAssetDestinationFolder" refers to the file
	 * system location from where published output is to be retrieved for checkin. -
	 * Variable "publishingChannelId" refers to the publishing channel to be used
	 * for publishing. - Map "publishingParametersMap" refers to the map of
	 * publishing paramaters to be used while publishing. - Variable
	 * "desiredSourceContentType" refers to desired content type of the asset to be
	 * published. - Variable "publishedOutputFileExtension" refers to publishing
	 * Output's file extension.
	 * 
	 * Checkin Variables ------------------ - Variable publishedAssetStatus refers
	 * to the status value to be set on an asset after publishing. - Variable
	 * publishedAssetCollectionPath refers to the complete collection value
	 * specified by collection path separated by "/" in which asset is to be checked
	 * in after publishing. - Variable publishedAssetName refers to the name to be
	 * given to the published asset which will further be checkin in collection. -
	 * Variable publishedAssetRouteToUser refers to the user to whom the checked in
	 * asset needs to be routed. - Variable publishedAssetContentType refers to the
	 * content type of the published asset that is required attribute while checkin
	 * of the asset. - Variable publishedAssetCreateMinorVersion refers to the
	 * whether the checkin needs to be associated with a minor version or a major
	 * version.
	 * 
	 * Note: To provide any attribute value for an attribute of user defined
	 * heirarchial domain type, use ';' as separator. example -
	 * "place":"World;Asia;India;Punjab"
	 **************************************************************************/
	@SuppressWarnings("unused")
	@Override
	public StringBuffer publishOnAssetChange(long assetId, AttributeValue[] attributeValues)
			throws QppServiceException {

		// name of the status, such that when an asset is in given status of the given
		// workflow, it should be published.
		String desiredSourceWorkflow = "SOP Authoring";
		String desiredSourceStatusName = "Approved For Publishing";

		// Channel to be used for publishing
		String publishingChannelId = "smartDocPdfEx";

		// Publishing Output's file extension
		String publishedOutputFileExtension = "pdf";

		// Map of publishing channel paramaters
		Map<String, String> publishingParametersMap = new HashMap<>();

		// Checkin attributes of published asset
		// *********************************
		String publishedAssetStatus = "Published";

		// Specify complete collection path, from root collection, in which published
		// asset should be checked-in.
		// Collection names in collection path should be separated by "/" .
		String publishedAssetCollectionPath = "Home/FinGR/Published";

		// Specify the user to whom the published asset has to be routed to.
		String publishedAssetRouteToUser = "Admin@cedevsop.com";

		// Set the flag depending on the revision control settings for the given
		// collection
		boolean publishedAssetCreateMinorVersion = false;

		// Destination folder for published content
		String publishedAssetDestinationFolder = java.lang.System.getProperty("java.io.tmpdir");

		StringBuffer sb = new StringBuffer();

		long[] requiredAttributeIds = { DefaultAttributes.WORKFLOW, DefaultAttributes.STATUS, DefaultAttributes.NAME };
		AttributeValue[] publishedAssetAttributeValues = {};
		AttributeValue[] sourceAssetStatusAttributes = {};
		AttributeValue[] sourceRequiredAssetAttributeValues = assetServiceApi.getAttributeValues(assetId,
				requiredAttributeIds);
		Value sourceAssetWorkFlow = null;
		Value sourceAssetStatus = null;
		DomainValue sourceAssetContentType = null;
		Value sourceAssetName = null;
		int i;
		for (i = 0; i < sourceRequiredAssetAttributeValues.length; i++) {
			if (sourceRequiredAssetAttributeValues[i].getAttributeId() == DefaultAttributes.WORKFLOW) { // workflow
				sourceAssetWorkFlow = sourceRequiredAssetAttributeValues[i].getAttributeValue();

			} else if (sourceRequiredAssetAttributeValues[i].getAttributeId() == DefaultAttributes.STATUS) { // status
				sourceAssetStatus = sourceRequiredAssetAttributeValues[i].getAttributeValue();
			} else if (sourceRequiredAssetAttributeValues[i].getAttributeId() == DefaultAttributes.NAME) { // name
				sourceAssetName = sourceRequiredAssetAttributeValues[i].getAttributeValue();
			}
		}
		String desiredSourceContentTypeName = "Name";

		long dateTime = System.currentTimeMillis();

		if (sourceAssetContentType != null
				&& desiredSourceContentTypeName.equalsIgnoreCase(sourceAssetContentType.getName())) {
			if (sourceAssetWorkFlow != null
					&& desiredSourceWorkflow.equalsIgnoreCase(((DomainValue) sourceAssetWorkFlow).getName())
					&& sourceAssetStatus != null
					&& desiredSourceStatusName.equalsIgnoreCase(((DomainValue) sourceAssetStatus).getName())) {
				String publishedAssetName = sourceAssetName + "_" + dateTime + "." + publishedOutputFileExtension;
				File outputFile = new File(publishedAssetDestinationFolder + "/" + publishedAssetName + "."
						+ publishedOutputFileExtension);
				FileOutputStream fileOutputStream = null;

				// publish Asset
				try {
					fileOutputStream = new FileOutputStream(outputFile);
					publishingServiceApi.publishAsset(assetId,
							publishingChannelId, publishingParametersMap);
					// blobLocalStreamingService.download(assetDonInfo[0].getDownloadUrl(),
					// fileOutputStream);
					sb.append("published \n");
				} catch (Exception exception) {
					System.out.println(
							"Error while publishing the asset with name : " + publishedAssetName + "\n" + exception);
					sb.append(exception);
				} finally {
					if (fileOutputStream != null) {
						try {
							fileOutputStream.close();
						} catch (IOException e) {						
							e.printStackTrace();
						}
					}
				}

				String filename = outputFile.getName();

				String filextension = Utills.getFileExtension(filename);

				Map<String, String> attributeValuesMap = new HashMap<>();
				attributeValuesMap.put("name", publishedAssetName);
				attributeValuesMap.put("workflow", desiredSourceWorkflow);
				attributeValuesMap.put("status", publishedAssetStatus);
				attributeValuesMap.put("file extension", filextension);
				attributeValuesMap.put("original filename", filename);
				attributeValuesMap.put("routed to", publishedAssetRouteToUser);
				attributeValuesMap.put("Collection", publishedAssetCollectionPath);

				AttributeValue[] attributeValuesArray = createAttributeValueArray(attributeValuesMap);

				// get asset Attributes
				AttributeValue[] sourceAssetAttributes = assetServiceApi.getAsset(assetId).getAttributeValues();

				// initialise counter for publishedAssetAttributeValues array
				int k = 0;

				// get user defined attributes
				for (i = 0; i < sourceAssetAttributes.length; i++) {
					if (attributeServiceApi.getAttribute(sourceAssetAttributes[i].getAttributeId())
							.getModificationLevel() == AttributeModificationLevels.USER_MODIFIABLE) {
						if (sourceAssetAttributes[i].getAttributeId() != DefaultAttributes.STATUS) {
							publishedAssetAttributeValues[k] = sourceAssetAttributes[i];
							k++;
						}
					}
				}
				// getStatus AttributeValue to be marked on published asset
				for (i = 0; i < attributeValuesArray.length; i++) {
					if (attributeValuesArray[i].getAttributeId() == DefaultAttributes.STATUS) {
						// sourceAssetStatusAttributes[0] = attributeValuesArray[i];
					}
				}

				System.out.println("merge attributes");
				// get merged attributeValues to checkin published asset
				int j;
				for (i = 0; i < attributeValuesArray.length; i++) {
					for (j = 0; j < publishedAssetAttributeValues.length; j++) {
						if (attributeValuesArray[i].getAttributeId() == publishedAssetAttributeValues[j]
								.getAttributeId()) {
							publishedAssetAttributeValues[j] = attributeValuesArray[i];
							break;
						}
					}
					if (j == publishedAssetAttributeValues.length) {
						publishedAssetAttributeValues[j] = attributeValuesArray[i];
					}
				}
				FileInputStream fileInputStream = null;
				try {
					fileInputStream = new FileInputStream(outputFile);
					Asset asset = new Asset();
					asset.setAttributeValues(publishedAssetAttributeValues);

					long publishedAssetId = 0;
					// initiate check in of new asset using assetServiceApi's initNewCheckin API.
					AssetUploadInfo assetUploadInfo = assetServiceApi.initNewCheckin(asset,
							publishedAssetCreateMinorVersion);
					if (assetUploadInfo != null) {
						System.out.println("\nThe asset context =" + assetUploadInfo.getContextId() + " url = "
								+ assetUploadInfo.getUploadUrl());
						publishedAssetId = assetServiceApi.getAssetStatus(assetUploadInfo.getContextId()).getAssetId();
					}
					// blobLocalStreamingService.upload(assetUploadInfo.getUploadUrl(),
					// fileInputStream);
					System.out.println("\nThe asset " + "'" + filename + "' publishedAssetId =" + publishedAssetId
							+ "  has been successfully checked into QPP Server.");
					sb.append("\nThe asset " + "'" + filename + "' publishedAssetId =" + publishedAssetId
							+ "  has been successfully checked into QPP Server.");
				} catch (Exception exception) {
					System.out.println("\nError : The asset " + "'" + filename + "'"
							+ " could not be checked into QPP Server.\n" + exception);
				} finally {
					if (fileInputStream != null) {

						try {
							fileInputStream.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}

				try {
					// set asset Attribute Values (update status)
					assetServiceApi.setAttributeValues(assetId, sourceAssetStatusAttributes);
					System.out.println("Success : source asset's status marked to " + publishedAssetStatus + "\n");
				} catch (Exception exception) {
					System.out.println("\nError : while source asset's status marking to " + publishedAssetStatus + "\n"
							+ exception);
				}

			}
		}

		sb.toString();

		return sb;
	}

	/*********************************************************************
	 * Sample Name: GetAssetHighres Objective : The sample will return the content
	 * of a QPP asset, with the given asset id, at the location specified in
	 * variable "targetFolder". Prerequisite: - Variable "assetId" should refer to
	 * the QPP Server's Asset. - Varibale "targetFolder" should refer to the
	 * location where the asset highres is to be copied. Target folder should have
	 * write permissions.
	 **************************************************************************/
	@Override
	public StringBuffer getAssetHiresNew(long assetId, long majorVersion, long minorVersion)
			throws QppServiceException {
		StringBuffer stringBuffer = new StringBuffer();
		String targetFolder = "C:/target";
		FileOutputStream fileOutputStream = null;
		String assetName = null;
		try {
			assetName = getAssetNameFromId(assetId);
			System.out.println("assest name: " + assetName);

			File highResFile = new File(targetFolder, assetName);
			// declare an output stream where the asset highres will be written
			fileOutputStream = new FileOutputStream(highResFile);
			if (majorVersion > 0 || minorVersion > 0) {
				assetServiceApi.getAssetDownloadInfo(assetId, majorVersion, minorVersion);
			} else {
				assetServiceApi.getAssetDownloadInfo(assetId, 0, 0);
			}

			// blobLocalStreamingService.download(assetDownloadInfo.getDownloadUrl(),
			// fileOutputStream);

			stringBuffer.append(assetName + " successfully copied at location : " + targetFolder);
		} catch (Exception exception) {
			stringBuffer.append("Error : Couldnot get highres of asset:" + assetName + "\n");
			stringBuffer.append(exception);
		} finally {
			if (fileOutputStream != null) {
				try {
					fileOutputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		stringBuffer.toString();

		return null;
	}

	/*********************************************************************
	Sample Name:
		GetSavedSearchResults
	Objective :
		The sample will execute a query saved on Platform Server and return results.
	Prerequisite:
		- Variable "queryId" should refer to a saved search in Platform .
	**************************************************************************/
	@Override
	public StringBuffer getSavedSearchResult(long queryId) throws QppServiceException {
		QueryDisplay queryDisplay = new QueryDisplay();

		// Specify the display mode
		int displayMode = DefaultDisplayModes.DEFAULT;

		// Specify the explore mode.
		int exploreMode = ExploreModeTypes.PLAIN;

		queryDisplay.setDisplayMode(displayMode);
		queryDisplay.setExploreMode(exploreMode);

		;
		// declare a string buffer
		StringBuffer stringBuffer = new StringBuffer();

		QueryResultElement[] response = queryServiceApi.getQueryResult(queryId, queryDisplay);

		ObjectMapper mapper = new ObjectMapper();
		// Converting the result Object to JSONString
		String jsonResult = null;
		try {
			jsonResult = mapper.writeValueAsString(response);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("" + jsonResult);
		stringBuffer.append("" + jsonResult);
		stringBuffer.toString();

		return stringBuffer;
	}

	/*********************************************************************
	 * Sample Name: PublishAssetOnStatusChange Objective : The sample will publish
	 * an asset on status change. using the specified publishing channels. The
	 * published output will be checked in to the specified colelction. The asset
	 * status shall be changed to the status desired after the published asset is
	 * checked-in.
	 * 
	 * Prerequisite: Sample should be configured as event based with "OBJECT TYPE"
	 * as ASSET and "CHANGE TYPE" as ASSET CHANGED. - Variable "assetId" refers to
	 * the id of the asset to be published. - Variable "desiredSourceStatusName"
	 * refers to the desired status value for an asset to be published. - Variable
	 * "contentTypeToChannelsMap" refers to the content type of asset to comma
	 * separated set of publishing channels to be used for publishing. - Variable
	 * publishedAssetCollectionPath refers to the complete collection value
	 * specified by collection path separated by "/" in which asset is to be checked
	 * in after publishing. - Variable publishedAssetRouteToUser refers to the user
	 * to whom the checked in asset needs to be routed. - Variable
	 * publishedAssetStatus refers to the status value to be set on an asset after
	 * publishing. - Variable publishedAssetCreateMinorVersion refers to the whether
	 * the checkin needs to be associated with a minor version or a major version.
	 * 
	 * Note: To provide any attribute value for an attribute of user defined
	 * heirarchial domain type, use ';' as separator. example -
	 * "place":"World;Asia;India;Punjab"
	 **************************************************************************/
	@Override
	public StringBuffer multiChannelPublishOnAssetChange(long assetId) throws QppServiceException {
		String desiredSourceStatusName = "Initiate Publishing";

		// Content Type to Publishign Channels map for defining comma separated
		// publishing channels to be used for publishing
		Map<String, String> contentTypeToChannelsMap = new HashMap<>();
		contentTypeToChannelsMap.put("System;Structured Content;Smart Content;Document;Company Report",
				"smartDocHtmlZip,CompanyReportPdf");
		contentTypeToChannelsMap.put("System;Structured Content;Smart Content;Document;Smart Document",
				"smartDocPdfEx,smartDocHtmlZip");
		contentTypeToChannelsMap.put("System;Structured Content;Business Document", "busDocPdf");
		contentTypeToChannelsMap.put("System;Asset;Document;Microsoft Word", "wordDocPdf");
		contentTypeToChannelsMap.put("System;Asset;Project;QuarkXPress Project", "qxpPdf");
		contentTypeToChannelsMap.put("System;Asset;Document;Microsoft PowerPoint", "pptPdf");

		// Specify status to be set for the of published asset
		String publishedAssetStatus = "Published";

		// Map used to lookup the appropriate content type of the published output
		Map<String, String> mimeTypeToContentTypeMap = new HashMap<>();
		mimeTypeToContentTypeMap.put("application/zip", "System;Asset");
		mimeTypeToContentTypeMap.put("application/pdf", "System;Asset;Picture");
		mimeTypeToContentTypeMap.put("image/jpeg", "System;Asset;Picture");

		long[] requiredAttributeIds = { DefaultAttributes.WORKFLOW, DefaultAttributes.STATUS,
				DefaultAttributes.CONTENT_TYPE, DefaultAttributes.NAME };
		AttributeValue[] sourceRequiredAssetAttributeValues = assetServiceApi.getAttributeValues(assetId,
				requiredAttributeIds);
		Value sourceAssetWorkFlow = null;
		Value sourceAssetStatus = null;
		Value sourceAssetContentType = null;
		String sourceAssetContentTypeHierarchy = "";
		Value sourceAssetName = null;
		int i;
		for (i = 0; i < sourceRequiredAssetAttributeValues.length; i++) {
			if (sourceRequiredAssetAttributeValues[i].getAttributeId() == DefaultAttributes.WORKFLOW) { // workflow
				sourceAssetWorkFlow = sourceRequiredAssetAttributeValues[i].getAttributeValue();

			} else if (sourceRequiredAssetAttributeValues[i].getAttributeId() == DefaultAttributes.STATUS) { // status
				sourceAssetStatus = sourceRequiredAssetAttributeValues[i].getAttributeValue();
			} else if (sourceRequiredAssetAttributeValues[i].getAttributeId() == DefaultAttributes.CONTENT_TYPE) { // content
																													// type
				sourceAssetContentType = sourceRequiredAssetAttributeValues[i].getAttributeValue();
				sourceAssetContentTypeHierarchy = getcontentTypeHierarchyFromId(
						sourceRequiredAssetAttributeValues[i].getType());
			} else if (sourceRequiredAssetAttributeValues[i].getAttributeId() == DefaultAttributes.NAME) { // name
				sourceAssetName = sourceRequiredAssetAttributeValues[i].getAttributeValue();
			}
		}
		System.out.println("sourceAssetName = " + sourceAssetName + "   sourceAssetContentTypeHierarchy:"
				+ sourceAssetContentTypeHierarchy);
		contentStructureServiceApi
				.getContentTypesInfoByHierarchy("System;Asset;PicTURE").getName();
		StringBuffer sb = new StringBuffer();
		if (sourceAssetContentType != null && contentTypeToChannelsMap.containsValue(sourceAssetContentTypeHierarchy)) {

			if (sourceAssetWorkFlow != null && sourceAssetStatus != null
					&& desiredSourceStatusName.equalsIgnoreCase(((DomainValue) sourceAssetStatus).getName())) {
				// get asset Attributes
				AttributeValue[] sourceAssetAttributes = assetServiceApi.getAsset(assetId).getAttributeValues();

				String channelIds = contentTypeToChannelsMap.get(sourceAssetContentTypeHierarchy);
				String[] channelIdsArray = channelIds.split(channelIds, ',');

				for (int ii = 0; ii < channelIdsArray.length; ii++) {
					String aPublishingChannelId = channelIdsArray[ii];
					System.out.println("Publishing started for channel: " + aPublishingChannelId);
					sb = publishAndCheckinDocument(aPublishingChannelId, sourceAssetAttributes,
							mimeTypeToContentTypeMap, sourceAssetName);
					System.out.println("Publishing finished for channel: " + aPublishingChannelId);
				}
				updateStatusOfSourceAsset(sourceAssetAttributes, publishedAssetStatus, assetId);
			}
		}
		return sb;
	}

	private void updateStatusOfSourceAsset(AttributeValue[] sourceAssetAttributes, String publishedAssetStatus,
			long assetId) {
		String aWorkflowName = null;
		try {

			for (int n = 0; n < sourceAssetAttributes.length; n++) {
				if (sourceAssetAttributes[n].getAttributeId() == DefaultAttributes.WORKFLOW) {
					aWorkflowName = sourceAssetAttributes[n].getAttributeValue().getClass().getName();
					break;
				}
			}

			// set asset Attribute Values (update status) - Workflow name required later to
			// get the id of status
			Map<String, String> statusAttributeValuesMap = new HashMap<>();
			statusAttributeValuesMap.put("STATUS", publishedAssetStatus);
			statusAttributeValuesMap.put("WORKFLOW", aWorkflowName);
			;
			AttributeValue[] sourceAssetStatusAttributes = createAttributeValueArray(statusAttributeValuesMap);
			assetServiceApi.setAttributeValues(assetId, sourceAssetStatusAttributes);
			System.out.println("Success : source asset's status marked to " + publishedAssetStatus + "\n");
		} catch (Exception exception) {
			System.out.println("\nError : while source asset's status marking to " + aWorkflowName + "::"
					+ publishedAssetStatus + "\n" + exception);
		}
	}

	private StringBuffer publishAndCheckinDocument(String aChannelId, AttributeValue[] sourceAssetAttributes,
			Map<String, String> mimeTypeToContentTypeMap, Value sourceAssetName) {
		long dateTime = System.currentTimeMillis();
		String publishedAssetDestinationFolder = java.lang.System.getProperty("java.io.tmpdir");
		StringBuffer sb = new StringBuffer();
		AttributeValue[] publishedAssetAttributeValues = {};

		// Collection names in collection path should be separated by "/" .
		String publishedAssetCollectionPath = "Rivacorp/Published";

		// Specify the user to whom the published asset has to be routed to.
		String publishedAssetRouteToUser = "Admin@rivacorp.com";

		PublishingChannel _channelDef = null;
		try {
			_channelDef = publishingServiceApi.getChannel(aChannelId);
		} catch (QppServiceException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String channelMimeType = _channelDef.getOutputType();
		String publishedOutputFileExtension = Utills.getFileExtensionFromMimeType(channelMimeType);
		String publishedContentType = mimeTypeToContentTypeMap.get(channelMimeType);
		System.out.println("sourceAssetName = " + sourceAssetName + "  \n");
		String publishedAssetName = sourceAssetName + "_" + dateTime + "." + publishedOutputFileExtension;
		System.out.println("publishedAssetName = " + publishedAssetName + "  \n");
		File outputFile = new File(
				publishedAssetDestinationFolder + "/" + publishedAssetName + "." + publishedOutputFileExtension);
		FileOutputStream fileOutputStream = null;
		try {
			fileOutputStream = new FileOutputStream(outputFile);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}

		// Map of publishing channel paramaters
		Map<String, String> publishingParametersMap = new HashMap<>();

		// publish Asset
		try {
			publishingServiceApi.publishAsset(0, aChannelId,
					publishingParametersMap);
			// blobLocalStreamingService.download(assetDonInfo[0].getDownloadUrl(),
			// fileOutputStream);
			System.out.println("asset published in " + publishedAssetDestinationFolder + "\n");
			sb.append("published \n");
		} catch (Exception exception) {
			System.out.println("Error while publishing the asset with name : " + publishedAssetName + "\n" + exception);
			sb.append(exception);
		} finally {
			if (fileOutputStream != null) {
				try {
					fileOutputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		String filename = outputFile.getName();

		// get the file extension
		String filextension = Utills.getFileExtension(filename);// getFileExtension(filename);

		Map<String, String> attributeValuesMap = new HashMap<>();
		attributeValuesMap.put("name", publishedAssetName);
		attributeValuesMap.put("file extension", filextension);
		attributeValuesMap.put("content type", publishedContentType);
		attributeValuesMap.put("original filename", filename);
		attributeValuesMap.put("routed to", publishedAssetRouteToUser);
		attributeValuesMap.put("Collection", publishedAssetCollectionPath);

		// create com.quark.qpp.core.attribute.service.dto.AttributeValue Array from the
		// above attributeValuesMap.
		AttributeValue[] attributeValuesArray = {};
		try {
			attributeValuesArray = createAttributeValueArray(attributeValuesMap);
		} catch (StatusNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (WorkflowNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (QppServiceException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// get user defined attributes
		for (int i = 0; i < sourceAssetAttributes.length; i++) {
			try {
				if (attributeServiceApi.getAttribute(sourceAssetAttributes[i].getAttributeId())
						.getModificationLevel() == AttributeModificationLevels.USER_MODIFIABLE) {
					if (sourceAssetAttributes[i].getAttributeId() != DefaultAttributes.STATUS
							&& sourceAssetAttributes[i].getAttributeId() != DefaultAttributes.WORKFLOW) {
					}
				}
			} catch (QppServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		// get merged attributeValues to checkin published asset
		int j;
		for (int i = 0; i < attributeValuesArray.length; i++) {
			for (j = 0; j < publishedAssetAttributeValues.length; j++) {
				if (attributeValuesArray[i].getAttributeId() == publishedAssetAttributeValues[j].getAttributeId()) {
					publishedAssetAttributeValues[j] = attributeValuesArray[i];
					break;
				}
			}
			if (j == publishedAssetAttributeValues.length) {
				publishedAssetAttributeValues[j] = attributeValuesArray[i];
			}
		}
		FileInputStream fileInputStream = null;
		try {
			fileInputStream = new FileInputStream(outputFile);
			Asset asset = new Asset();
			asset.setAttributeValues(publishedAssetAttributeValues);

			long publishedAssetId = 0;
			// initiate check in of new asset using assetServiceApi's initNewCheckin API.
			AssetUploadInfo assetUploadInfo = assetServiceApi.initNewCheckin(asset, false);
			if (assetUploadInfo != null) {
				System.out.println("\nThe asset context =" + assetUploadInfo.getContextId() + " url = "
						+ assetUploadInfo.getUploadUrl());
				publishedAssetId = assetServiceApi.getAssetStatus(assetUploadInfo.getContextId()).getAssetId();
			}
			// blobLocalStreamingService.upload(assetUploadInfo.getUploadUrl(),
			// fileInputStream);
			System.out.println("\nThe asset " + "'" + filename + "' publishedAssetId =" + publishedAssetId
					+ "  has been successfully checked into QPP Server.");
			sb.append("\nThe asset " + "'" + filename + "' publishedAssetId =" + publishedAssetId
					+ "  has been successfully checked into QPP Server.");

		} catch (Exception exception) {
			System.out.println("\nError : The asset " + "'" + filename + "'"
					+ " could not be checked into QPP Server.\n" + exception);
		} finally {
			if (fileInputStream != null) {

				try {
					fileInputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return sb;
	}

	/*********************************************************************
	 * Sample Name: PublishAssetOnStatusChange Objective : The sample will publish
	 * an asset on status change. The provided channel will be used for publishing.
	 * The file generated will have the provided asset name. The asset status shall
	 * be changed to the status desired to be set on published asset. The published
	 * asset will be checked in the platform in the provided collection.
	 * Prerequisite: Sample should be configured as event based with "OBJECT TYPE"
	 * as ASSET and "CHANGE TYPE" as ASSET CHANGED. - Variable "assetId" refers to
	 * the id of the asset to be published. - Variable "desiredSourceWorkflow"
	 * refers to the desired workflow value for an asset to be published. - Variable
	 * "desiredSourceStatusName" refers to the desired status value for an asset to
	 * be published. - Variable "publishedAssetDestinationFolder" refers to the file
	 * system location from where published output is to be retrieved for checkin. -
	 * Variable "publishingChannelId" refers to the publishing channel to be used
	 * for publishing. - Map "publishingParametersMap" refers to the map of
	 * publishing paramaters to be used while publishing. - Variable
	 * "desiredSourceContentType" refers to desired content type of the asset to be
	 * published. - Variable "publishedOutputFileExtension" refers to publishing
	 * Output's file extension.
	 * 
	 * Checkin Variables ------------------ - Variable publishedAssetStatus refers
	 * to the status value to be set on an asset after publishing. - Variable
	 * publishedAssetCollectionPath refers to the complete collection value
	 * specified by collection path separated by "/" in which asset is to be checked
	 * in after publishing. - Variable publishedAssetName refers to the name to be
	 * given to the published asset which will further be checkin in collection. -
	 * Variable publishedAssetRouteToUser refers to the user to whom the checked in
	 * asset needs to be routed. - Variable publishedAssetContentType refers to the
	 * content type of the published asset that is required attribute while checkin
	 * of the asset. - Variable publishedAssetCreateMinorVersion refers to the
	 * whether the checkin needs to be associated with a minor version or a major
	 * version.
	 * 
	 * Note: To provide any attribute value for an attribute of user defined
	 * heirarchial domain type, use ';' as separator. example -
	 * "place":"World;Asia;India;Punjab"
	 **************************************************************************/
	@Override
	public StringBuffer PublishAssetOnAssetChange_SOP(long assetId, AttributeValue[] attributeValues)
			throws QppServiceException {
		// name of the status, such that when an asset is in given status of the given
		// workflow, it should be published.
		String desiredSourceWorkflow = "SOP Authoring";
		String desiredSourceStatusName = "Approved For Publishing";

		// Channel to be used for publishing
		String publishingChannelId = "SOPHTML";

		// Publishing Output's file extension
		String publishedOutputFileExtension = "zip";

		// Checkin attributes of published asset
		// *********************************
		String publishedAssetStatus = "Published";

		// Specify complete collection path, from root collection, in which published
		// asset should be checked-in.
		// Collection names in collection path should be separated by "/" .
		String publishedAssetCollectionPath = "Home/FinGR/Published";

		// Specify the user to whom the published asset has to be routed to.
		String publishedAssetRouteToUser = "Admin@cedevsop.com";

		// Set the flag depending on the revision control settings for the given
		// collection
		boolean publishedAssetCreateMinorVersion = false;

		// Destination folder for published content
		String publishedAssetDestinationFolder = java.lang.System.getProperty("java.io.tmpdir");

		StringBuffer sb = new StringBuffer();

		long[] requiredAttributeIds = { DefaultAttributes.WORKFLOW, DefaultAttributes.STATUS, DefaultAttributes.NAME };
		AttributeValue[] publishedAssetAttributeValues = {};
		AttributeValue[] sourceAssetStatusAttributes = {};
		AttributeValue[] sourceRequiredAssetAttributeValues = assetServiceApi.getAttributeValues(assetId,
				requiredAttributeIds);
		Value sourceAssetWorkFlow = null;
		Value sourceAssetStatus = null;
		String sourceAssetContentType = "Asset";
		Value sourceAssetName = null;
		int i;
		for (i = 0; i < sourceRequiredAssetAttributeValues.length; i++) {
			if (sourceRequiredAssetAttributeValues[i].getAttributeId() == DefaultAttributes.WORKFLOW) { // workflow
				sourceAssetWorkFlow = sourceRequiredAssetAttributeValues[i].getAttributeValue();

			} else if (sourceRequiredAssetAttributeValues[i].getAttributeId() == DefaultAttributes.STATUS) { // status
				sourceAssetStatus = sourceRequiredAssetAttributeValues[i].getAttributeValue();
			} else if (sourceRequiredAssetAttributeValues[i].getAttributeId() == DefaultAttributes.NAME) { // name
				sourceAssetName = sourceRequiredAssetAttributeValues[i].getAttributeValue();
			}
		}

		String desiredSourceContentTypeName = "Asset";

		long dateTime = System.currentTimeMillis();

		if (sourceAssetContentType != null && desiredSourceContentTypeName.equalsIgnoreCase("Asset")) {
			if (sourceAssetWorkFlow != null
					&& desiredSourceWorkflow.equalsIgnoreCase(((DomainValue) sourceAssetWorkFlow).getName())
					&& sourceAssetStatus != null
					&& desiredSourceStatusName.equalsIgnoreCase(((DomainValue) sourceAssetStatus).getName())) {
				System.out.println("html sourceAssetName = " + sourceAssetName + "  \n");
				String publishedAssetName = sourceAssetName + "_" + dateTime + "." + publishedOutputFileExtension;
				System.out.println("publishedAssetName = " + publishedAssetName + "  \n");
				File outputFile = new File(publishedAssetDestinationFolder + "/" + publishedAssetName + "."
						+ publishedOutputFileExtension);
				FileOutputStream fileOutputStream = null;
				try {
					fileOutputStream = new FileOutputStream(outputFile);
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				// publish Asset
				try {
					File cfoFile = getCFOZip(assetId, sourceAssetName);
					System.out
							.println("asset collected for output: " + cfoFile + "\n input size = " + cfoFile.length());
					FileInputStream inputStream = new FileInputStream(cfoFile);
					try {
						publish(inputStream, publishingChannelId, fileOutputStream);
					} finally {
						if (inputStream != null) {
							inputStream.close();
						}
					}
					// blobLocalStreamingService.download(assetDonInfo[0].getDownloadUrl(),
					// fileOutputStream);
					System.out.println("html asset published in " + publishedAssetDestinationFolder + "\n");
					sb.append("published html \n");
				} catch (Exception exception) {
					sb.append("html Error while publishing the asset with name : " + publishedAssetName + "\n");
					sb.append(exception);
				} finally {
					if (fileOutputStream != null) {
						try {
							fileOutputStream.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}

				String filename = outputFile.getName();

				// get the file extension
				String filextension = Utills.getFileExtension(filename);

				Map<String, String> attributeValuesMap = new HashMap<>();
				attributeValuesMap.put("name", publishedAssetName);
				attributeValuesMap.put("workflow", desiredSourceWorkflow);
				attributeValuesMap.put("status", publishedAssetStatus);
				attributeValuesMap.put("file extension", filextension);
				attributeValuesMap.put("original filename", filename);
				attributeValuesMap.put("routed to", publishedAssetRouteToUser);
				attributeValuesMap.put("Collection", publishedAssetCollectionPath);

				// create com.quark.qpp.core.attribute.service.dto.AttributeValue Array from the
				// above attributeValuesMap.
				AttributeValue[] attributeValuesArray = createAttributeValueArray(attributeValuesMap);

				// get asset Attributes
				AttributeValue[] sourceAssetAttributes = assetServiceApi.getAsset(assetId).getAttributeValues();

				// get user defined attributes
				for (i = 0; i < sourceAssetAttributes.length; i++) {
					if (attributeServiceApi.getAttribute(sourceAssetAttributes[i].getAttributeId())
							.getModificationLevel() == AttributeModificationLevels.USER_MODIFIABLE) {
						if (sourceAssetAttributes[i].getAttributeId() != DefaultAttributes.STATUS) {
						}
					}
				}
				// getStatus AttributeValue
				for (i = 0; i < attributeValuesArray.length; i++) {
					if (attributeValuesArray[i].getAttributeId() == DefaultAttributes.STATUS) {
						// sourceAssetStatusAttributes[0] = attributeValuesArray[i];
					}
				}

				// get merged attributeValues to checkin published asset
				int j;
				for (i = 0; i < attributeValuesArray.length; i++) {
					for (j = 0; j < publishedAssetAttributeValues.length; j++) {
						if (attributeValuesArray[i].getAttributeId() == publishedAssetAttributeValues[j]
								.getAttributeId()) {
							publishedAssetAttributeValues[j] = attributeValuesArray[i];
							break;
						}
					}
					if (j == publishedAssetAttributeValues.length) {
						publishedAssetAttributeValues[j] = attributeValuesArray[i];
					}
				}
				FileInputStream fileInputStream = null;
				try {
					fileInputStream = new FileInputStream(outputFile);
					Asset asset = new Asset();
					asset.setAttributeValues(publishedAssetAttributeValues);

					// initiate check in of new asset using assetServiceApi's initNewCheckin API.
					AssetUploadInfo assetUploadInfo = assetServiceApi.initNewCheckin(asset,
							publishedAssetCreateMinorVersion);
					long publishedAssetId = 0;
					if (assetUploadInfo != null) {
						sb.append("\nThe asset context =" + assetUploadInfo.getContextId() + " url = "
								+ assetUploadInfo.getUploadUrl());
						publishedAssetId = assetServiceApi.getAssetStatus(assetUploadInfo.getContextId()).getAssetId();
					}
					// blobLocalStreamingService.upload(assetUploadInfo.getUploadUrl(),
					// fileInputStream);
					System.out.println("\nThe asset " + "'" + filename + "'  publishedAssetId =" + publishedAssetId
							+ "  has been successfully checked into QPP Server.");
					sb.append("\nThe asset " + "'" + filename + "' publishedAssetId =" + publishedAssetId
							+ "  has been successfully checked into QPP Server.");
				} catch (Exception exception) {
					sb.append(
							"\nError : The asset " + "'" + filename + "'" + " could not be checked into QPP Server.\n");
					sb.append(exception);
				} finally {
					if (fileInputStream != null) {

						try {
							fileInputStream.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}

				try {
					// set asset Attribute Values (update status)
					assetServiceApi.setAttributeValues(assetId, sourceAssetStatusAttributes);
					System.out.println("\nSuccess : source asset's status marked to " + publishedAssetStatus + "\n");
				} catch (Exception exception) {
					System.out.println("\nError : while source asset's status marking to " + publishedAssetStatus + "\n"
							+ exception);
				}

			}
		}

		return null;
	}

	private PublishingUploadInfo publish(FileInputStream inputStream, String publishingChannelId,
			FileOutputStream outputStream) throws InvalidChannelException, QppServiceException {
		System.out.println("new started\n");
		PublishingUploadInfo status = publishingServiceApi.publishBasedOnStream(publishingChannelId, null);
		return status;
	}

	@SuppressWarnings("unchecked")
	private File getCFOZip(long assetId, Value sourceAssetName) throws AssetNotFoundException, InvalidChannelException,
			PublishingFailedException, QppServiceException, IOException {
		FileOutputStream fosCfoZip = null;
		String publishedAssetDestinationFolder = java.lang.System.getProperty("java.io.tmpdir");
		try {
			/*
			 * Channel parameters for publishing channel with id : SOPHTML
			 */
			Map<String, String> parametersForSmartDoc = new HashMap<>();
			parametersForSmartDoc.put("RESOLVE_QPP_RELATIONS", "true");

			/*
			 * Map of publishing channel id and publishing parameters
			 */
			Map parametersMap = new HashMap<>();
			parametersMap.put("collectSmartDocForOutputEx", parametersForSmartDoc);

			
			publishingServiceApi.publishAsset(assetId,
					"collectSmartDocForOutputEx", parametersMap);

			File cfoZipFile = new File(publishedAssetDestinationFolder + "/" + sourceAssetName + "_CFO_"
					+ System.currentTimeMillis() + ".zip");
			fosCfoZip = new FileOutputStream(cfoZipFile);

			// blobLocalStreamingService.download(publishingOutputDownloadInfo[0].getDownloadUrl(),
			// fosCfoZip);

			return cfoZipFile;
		} finally {
			if (fosCfoZip != null) {
				fosCfoZip.close();
			}
		}
	}

	private String getcontentTypeHierarchyFromId(long contentTypeId) {
		ContentTypeInfo[] contentTypeInfos = {};
		try {
			contentTypeInfos = contentStructureServiceApi.getContentTypeHierarchy(contentTypeId);
		} catch (QppServiceException e) {
			e.printStackTrace();
		}
		String hierarchy = "";
		for (int index = 0; index < contentTypeInfos.length; index++) {
			if (index == 0)
				hierarchy = contentTypeInfos[index].getName();
			else
				hierarchy = contentTypeInfos[index].getName() + ';' + hierarchy;
		}
		return hierarchy;
	}

}
