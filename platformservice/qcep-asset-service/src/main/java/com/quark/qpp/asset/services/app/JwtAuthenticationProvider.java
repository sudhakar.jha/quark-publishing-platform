package com.quark.qpp.asset.services.app;

import java.util.ArrayList;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

public class JwtAuthenticationProvider implements AuthenticationProvider {

	private static final String SECRET_KEY = "qpp-secret-key";
		
	private static String PRINCIPAL_FIELD = "userName";
			
	/**
	 * Authenticates the given PreAuthenticatedAuthenticationToken.
	 * If the principal contained in the authentication object is null, the request will be ignored to allow other providers to authenticate it.
	 * 
	 * @return an Authentication object which contains the user's details.
	 */
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		/*
		 * Retrieve the JWT from the authentication object.
		 */
		String jwt = authentication.getName();

		// Check if the authorization header is prefixed with "Bearer " which is the
		// general convention being used by many client side applications.
		if (jwt != null && jwt.startsWith("Bearer ")) {
			jwt = jwt.substring(7);
		}
	    
		if(!jwt.trim().isEmpty()) {
			
			PreAuthenticatedAuthenticationToken result = parseAndValidateJwtToken(jwt);
			if(result != null) {
				result.setDetails(authentication.getDetails());
			}
			return result;
		}
		return null;
	}

	
	/**
	 * Indicate that this provider only supports PreAuthenticatedAuthenticationToken (sub)classes.
	 */
	@Override
	public boolean supports(Class<?> authentication) {		
		return PreAuthenticatedAuthenticationToken.class.isAssignableFrom(authentication);
	}
	
	/**
	 * Validates the given json-web-token and retrieves user details from it. It returns a PreAuthenticatedAuthenticationToken which contains the userName as principal.
	 */
	private PreAuthenticatedAuthenticationToken parseAndValidateJwtToken(String jwt) {
		Claims claims = Jwts.parser()
				.setSigningKey(SECRET_KEY.getBytes())
				.parseClaimsJws(jwt).getBody();
		
		Object userName = claims.get(PRINCIPAL_FIELD);
		
		if(userName != null) {				
			return new PreAuthenticatedAuthenticationToken(userName.toString(), jwt, new ArrayList<>());
		}
		return null;
	}

}
