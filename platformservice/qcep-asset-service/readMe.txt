# QCP sample services contains sample to perform operations and apply custom logics, Below are the sample written 
  in this project with detail to perform certain operations.
   
   1.
	 #Sample Name: CheckInNewAsset
	 #Objective : The sample will checkIn a local
	  file, specified in variable "filePath", to QPP as new asset.
	 #Prerequisite:
	  Collection Path, Worklflow name, Status name and route to user for the asset
	  need to specified in the variables "collectionPath", "workflowName",
	  "statusName" and "routeToUser" respectively. Set the createMinorVersion flag
	  depending upon the revision control settings on specified collection. The
	  asset will be checked in with the same name as that of file. Note: To provide
	  any attribute value for an attribute of user defined heirarchial domain type,
	  use ';' as separator. 
	 example - "place":"World;Asia;India;Punjab"
	 
    2.
	  #Sample Name: GetAssetMetadata
	  #Objective : The sample will return the complete metadata of the asset with the given id and asset version.
	  #Prerequisite: -
	   Variable "assetId" should refer to the QPP Server's Asset. 
	    - Variable
	      "majorVersion" should refer to the required Asset's major version. Provide
	       null to fetch metadata of latest version. - Variable "minorVersion" should
	       refer to the required Asset's minor version. Provide 0 to fetch metadata
	       of latest version.
	       
	 3.
	   #Sample Name: CheckOutAsset 
	   #Objective: This sample will mark the given asset as checked out.
	   #Prerequisite: - Variable "assetId" should refer to the QPP Server's Asset to
	   be checked out.
	    - Varibale "targetFolder" should refer to the location where
	   the asset highres shall be copied when required. Target folder should have
	   write permissions. 
	
	4. 
	 #Sample Name: PublishAnAsset
	 #Objective : The Sample will publish a QuarkXPressproject as PDF. The file generated will have same name as the that of QXP
	  with desired format as extension. Prerequisite: - Variable "assetName" refers
	  to the QPP Asset to be published.
	  - Variable "publishingChannelId" refers to
	  the publishing channel to be used for publishing QXP as PDF - Variable
	  "publishingParametersMap" refers to the publshing channel parameters -
	  Variable "destinationFolder" refers to the file system location where published output is to be retrieved.
	  
	5.
	 #Sample Name: DeliverAnAsset
	 #Objective : The sample will deliver a QPP Asset
	              using the specified delivery channel.
	 #Prerequisite: - Variable "assetPath"
	  refers to the QPP Asset on the basis of collection path.
	 - Variable
	 "deliveryChannelId" refers to the id of the delivery channel used to deliver
	 specified asset. - Variables "exchangeServer", "senderMailId",
	"recipientMailId", "mailSubject", "mailMessage" refer to the delivery channel parameters.
	 
	6.
	  #Sample Name: PublishAndDeliverAnAsset
	  #Objective : The sample will publish a
	  QuarkXPress project as PDF and the output is delivered using sendEmail
	  delivery channel. 
	  #Prerequisite: Variable "assetPath" refers to the QPP Asset on the basis of its collection path.
	  
	7.
	  #Sample Name: UpdateAssetMetadata 
	  #Objective : The sample will update the
	  metadata of an asset with a given id.
	  #Prerequisite: - Variable "assetId" should refer to the QPP Server's Asset Id.
	  
	8.
	 #Sample Name: GetAssetHighres
	 #Objective : The sample will return the content of a QPP asset, with the given asset id, at the location specified in
	 variable "targetFolder". Prerequisite: - Variable "assetId" should refer tothe QPP Server's Asset. - Varibale "targetFolder" should refer to the
	 location where the asset highres is to be copied. Target folder should have write permissions.
	 
	9.
	  
	  Sample Name: PublishAssetOnStatusChange Objective : The sample will publish
	  an asset on status change. The provided channel will be used for publishing.
	  The file generated will have the provided asset name. The asset status shall
	  be changed to the status desired to be set on published asset. The published
	  asset will be checked in the platform in the provided collection.
	  Prerequisite: Script should be configured as event based with "OBJECT TYPE"
	  as ASSET and "CHANGE TYPE" as ASSET CHANGED. - Variable "assetId" refers to
	  the id of the asset to be published. - Variable "desiredSourceWorkflow"
	  refers to the desired workflow value for an asset to be published. - Variable
	  "desiredSourceStatusName" refers to the desired status value for an asset to
	  be published. - Variable "publishedAssetDestinationFolder" refers to the file
	  system location from where published output is to be retrieved for checkin. -
	  Variable "publishingChannelId" refers to the publishing channel to be used
	  for publishing. - Map "publishingParametersMap" refers to the map of
	  publishing paramaters to be used while publishing. - Variable
	  "desiredSourceContentType" refers to desired content type of the asset to be
	  published. - Variable "publishedOutputFileExtension" refers to publishing
	  Output's file extension.
	  
	  Checkin Variables ------------------ - Variable publishedAssetStatus refers
	  to the status value to be set on an asset after publishing. - Variable
	  publishedAssetCollectionPath refers to the complete collection value
	  specified by collection path separated by "/" in which asset is to be checked
	  in after publishing. - Variable publishedAssetName refers to the name to be
	  given to the published asset which will further be checkin in collection. -
	  Variable publishedAssetRouteToUser refers to the user to whom the checked in
	  asset needs to be routed. - Variable publishedAssetContentType refers to the
	  content type of the published asset that is required attribute while checkin
	  of the asset. - Variable publishedAssetCreateMinorVersion refers to the
	  whether the checkin needs to be associated with a minor version or a major
	  version.
	  
	  Note: To provide any attribute value for an attribute of user defined
	  heirarchial domain type, use ';' as separator. example -
	  "place":"World;Asia;India;Punjab"
	 
	 10.
	   Sample Name: GetAssetHighresNew Objective : The sample will return the content
	  of a QPP asset, with the given asset id, at the location specified in
	  variable "targetFolder". Prerequisite: - Variable "assetId" should refer to
	  the QPP Server's Asset. - Varibale "targetFolder" should refer to the
	  location where the asset highres is to be copied. Target folder should have
	  write permissions.
 	
 	11. 
	Sample Name:
		GetSavedSearchResults
	Objective :
		The sample will execute a query saved on Platform Server and return results.
	Prerequisite:
		- Variable "queryId" should refer to a saved search in Platform .
		
	12.
	  Sample Name: PublishAssetOnStatusChange Objective : The sample will publish
	  an asset on status change. using the specified publishing channels. The
	  published output will be checked in to the specified colelction. The asset
	  status shall be changed to the status desired after the published asset is
	  checked-in.
	  
	  Prerequisite: Sample should be configured as event based with "OBJECT TYPE"
	  as ASSET and "CHANGE TYPE" as ASSET CHANGED. - Variable "assetId" refers to
	  the id of the asset to be published. - Variable "desiredSourceStatusName"
	  refers to the desired status value for an asset to be published. - Variable
	  "contentTypeToChannelsMap" refers to the content type of asset to comma
	  separated set of publishing channels to be used for publishing. - Variable
	  publishedAssetCollectionPath refers to the complete collection value
	  specified by collection path separated by "/" in which asset is to be checked
	  in after publishing. - Variable publishedAssetRouteToUser refers to the user
	  to whom the checked in asset needs to be routed. - Variable
	  publishedAssetStatus refers to the status value to be set on an asset after
	  publishing. - Variable publishedAssetCreateMinorVersion refers to the whether
	  the checkin needs to be associated with a minor version or a major version.
	  
	  Note: To provide any attribute value for an attribute of user defined
	  heirarchial domain type, use ';' as separator. example -
	  "place":"World;Asia;India;Punjab"
	 
	13.
	 # Sample Name: PublishAssetOnStatusChange 
	 # Objective : The sample will publish an asset on status change. The provided channel will be used for publishing.
	  The file generated will have the provided asset name. The asset status shall
	  be changed to the status desired to be set on published asset. The published
	  asset will be checked in the platform in the provided collection.
	  Prerequisite: Sample should be configured as event based with "OBJECT TYPE"
	  as ASSET and "CHANGE TYPE" as ASSET CHANGED. - Variable "assetId" refers to
	  the id of the asset to be published. - Variable "desiredSourceWorkflow"
	  refers to the desired workflow value for an asset to be published. - Variable
	  "desiredSourceStatusName" refers to the desired status value for an asset to
	  be published. - Variable "publishedAssetDestinationFolder" refers to the file
	  system location from where published output is to be retrieved for checkin. -
	  Variable "publishingChannelId" refers to the publishing channel to be used
	  for publishing. - Map "publishingParametersMap" refers to the map of
	  publishing paramaters to be used while publishing. - Variable
	  "desiredSourceContentType" refers to desired content type of the asset to be
	  published. - Variable "publishedOutputFileExtension" refers to publishing
	  Output's file extension.
	  
	  Checkin Variables ------------------ - Variable publishedAssetStatus refers
	  to the status value to be set on an asset after publishing. - Variable
	  publishedAssetCollectionPath refers to the complete collection value
	  specified by collection path separated by "/" in which asset is to be checked
	  in after publishing. - Variable publishedAssetName refers to the name to be
	  given to the published asset which will further be checkin in collection. -
	  Variable publishedAssetRouteToUser refers to the user to whom the checked in
	  asset needs to be routed. - Variable publishedAssetContentType refers to the
	  content type of the published asset that is required attribute while checkin
	  of the asset. - Variable publishedAssetCreateMinorVersion refers to the
	  whether the checkin needs to be associated with a minor version or a major
	  version.
	  
	  Note: To provide any attribute value for an attribute of user defined
	  heirarchial domain type, use ';' as separator. example -
	  "place":"World;Asia;India;Punjab"
