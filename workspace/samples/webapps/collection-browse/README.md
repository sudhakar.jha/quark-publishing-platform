# Collection Browse

You can use collection browse to browse collections on a platform repository. A collection browse is a smart component that fetches collections using web APIs.

---