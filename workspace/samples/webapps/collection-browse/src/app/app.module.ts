import { APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { UserComponentsModule, CollectionComponentsModule, AuthService } from '@quark/platform-components-ng';
import { QwcModule } from '@quark/qwc';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { HttpServiceInterceptor } from '../interceptors/http-service-interceptor';
import { CollectionBrowseComponent } from './collection-browse/collection-browse.component';

@NgModule({
  declarations: [
    CollectionBrowseComponent
  ],
  imports: [
    BrowserModule, QwcModule, UserComponentsModule,
    CollectionComponentsModule, HttpClientModule],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpServiceInterceptor,
      multi: true
    },
    {
      provide: APP_INITIALIZER,
      useFactory: AuthServiceProviderFactory,
      deps: [AuthService],
      multi: true
    }
  ],
  bootstrap: [CollectionBrowseComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule { }

/**
 * Factory provider ensure that access token and query session Id has been genertated.
 */

export function AuthServiceProviderFactory(authService: AuthService) {
  return () => {
    return authService.generateAccessToken();
  };
}
