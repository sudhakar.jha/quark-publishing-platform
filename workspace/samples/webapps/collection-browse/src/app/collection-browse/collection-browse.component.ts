import { Component } from '@angular/core';

@Component({
  selector: 'app-collection-browse-component',
  templateUrl: './collection-browse.component.html',
  styleUrls: ['./collection-browse.component.scss'],
})
export class CollectionBrowseComponent {

  constructor() {
  }

  assetSelection(entityArray) {
    const assetName = entityArray[0].data[2].value;
    window.alert('You have selected ' + assetName);
  }

}
