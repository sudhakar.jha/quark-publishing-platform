# Asset Picker

You can use asset picker to filter and select an asset from a platform repository. An asset picker is a smart component that fetches data using web APIs.

---