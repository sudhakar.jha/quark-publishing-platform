import { Component } from '@angular/core';
import { AssetPickerConfig, QppNotificationService } from "@quark/platform-components-ng";

@Component({
    selector: 'asset-picker-component',
    templateUrl: 'asset-picker.component.html'
})
export class AssetPickerComponent {

    constructor(private notificationService: QppNotificationService) {
        this.notificationService.init();
    }

    assetPickerConfig = new AssetPickerConfig({
        title: "My Asset Picker",
        filterId: "OFFICE_DOCUMENTS",   // This filter will show only Microsoft office documents like Excel, Word. PowerPoint & Visio etc. You need to create your customized filter in asset-filters.json and checkin this file.
        multiSelect: true
    });
}

