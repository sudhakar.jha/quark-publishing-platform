# Getting Started

## Prerequisites

### Software
* <a href="https://github.com/angular/angular-cli" target="_blank">Angular CLI </a> 8.3.29 
* Node 10.22

### NPM Configuration
An **npm** package gets its config settings from the command line, environment variables, and npmrc files. 
A **.npmrc** file in the root of a project (a sibling of **node_modules** and **package.json**) sets config values specific to the project. So, before you install QWC & PWC, create a **.npmrc** file in the root folder of an app and paste the following code into the file:

```
@quark:registry=https://qrk.pkgs.visualstudio.com/0fe979d5-4bc4-4a46-9db2-f0a80d796161/_packaging/qrkce/npm/registry/
@quark:always-auth=true
```
### Setup Credentials
1.  To access Quark artifacts, create a **.npmrc** file in the directory **C:\Users\<user_name>** and copy the code below to the <a href="https://docs.microsoft.com/en-us/azure/devops/artifacts/npm/npmrc?view=azure-devops" target="_blank">user .npmrc</a> file.
```
; begin auth token
//qrk.pkgs.visualstudio.com/0fe979d5-4bc4-4a46-9db2-f0a80d796161/_packaging/qrkce/npm/registry/:username=qrk
//qrk.pkgs.visualstudio.com/0fe979d5-4bc4-4a46-9db2-f0a80d796161/_packaging/qrkce/npm/registry/:_password=[BASE64_ENCODED_PERSONAL_ACCESS_TOKEN]
//qrk.pkgs.visualstudio.com/0fe979d5-4bc4-4a46-9db2-f0a80d796161/_packaging/qrkce/npm/registry/:email=npm requires email to be set but doesn't use the value
//qrk.pkgs.visualstudio.com/0fe979d5-4bc4-4a46-9db2-f0a80d796161/_packaging/qrkce/npm/:username=qrk
//qrk.pkgs.visualstudio.com/0fe979d5-4bc4-4a46-9db2-f0a80d796161/_packaging/qrkce/npm/:_password=[BASE64_ENCODED_PERSONAL_ACCESS_TOKEN]
//qrk.pkgs.visualstudio.com/0fe979d5-4bc4-4a46-9db2-f0a80d796161/_packaging/qrkce/npm/:email=npm requires email to be set but doesn't use the value
; end auth token
```
2. Generate Base64 encoded personal access token:  
    1. From command/shell prompt run:
        ```
        node -e "require('readline') .createInterface({input:process.stdin,output:process.stdout,historySize:0}) .question('PAT> ',p => { b64=Buffer.from(p.trim()).toString('base64');console.log(b64);process.exit(); })"
        ```
    2. Paste your personal access token value and press **Enter/Return**.
    3. Copy the Base64 encoded value.
3. Replace both [BASE64_ENCODED_PERSONAL_ACCESS_TOKEN] values in the .npmrc file with your Base64 encoded value from Step 2.

### Install QWC and PWC
To install QWC and PWC, run the following commands:
```
$ cd PROJECT-NAME
$ npm install --save @quark/qwc
$ npm install --save @quark/platform-components-ng
```

### Add Styles
If you are using Angular CLI, add the following files in the global **styles.scss** file:

```
@import "~@quark/qwc/styles/qwc";
@import "~@quark/qwc/styles/fonts.css";
@import "~@quark/platform-components-ng/styles/_ui-components";
@import "~@quark/platform-components-ng/styles/icon-assets.css";
```

If you are not using Angular CLI, add the files to your build tooling or include files via elements in the **index.html**.

### Import QWC and PWC Modules
After you have installed QWC and PWC, import the modules to your application module file (app.module.ts). You should get a code similar to the following:
```
import {SomeModule} from '...';
import {QwcModule} from "@quark/qwc";
import { UserComponentsModule, CollectionComponentsModule, AccessControlComponentsModule, FormComponentsModule, ContentTypesComponentsModule, WorkflowsModule, QuickSearchComponentsModule, ErrorModule, PublishingModule, QueryService, AuthService, AttributeComponentModule, BackgroundTasksModule } from "@quark/platform-components-ng";

@NgModule({
     imports: [..., QwcModule, UserComponentsModule, , CollectionComponentsModule, AccessControlComponentsModule, FormComponentsModule, ContentTypesComponentsModule, WorkflowsModule, QuickSearchComponentsModule, ErrorModule, PublishingModule, QueryService, AuthService, AttributeComponentModule, BackgroundTasksModule],
     declarations: [AppComponent, ...],
     bootstrap: [AppComponent],
})
export class AppModule {}
```
`You do not have to include all the modules from the platform-components-ng library. Import modules based on the uses cased of your application.`

### Include CUSTOM_ELEMENTS_SCHEMA
To allow an **NgModule** to contain non-Angular elements with dash case or element properties named with dash case, you need to import **CUSTOM_ELEMENTS_SCHEMA**. To import **CUSTOM_ELEMENTS_SCHEMA**, add the following code to the file **app.module.ts**: 

```
import {CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

@NgModule({
     schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule {}
```

### Develop and Debug
Use the following command to run your project:
```
$ ng serve --open
```