import { APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { UserComponentsModule, CollectionComponentsModule, AuthService, QueryService} from '@quark/platform-components-ng';
import { QwcModule } from '@quark/qwc';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AssetCheckinComponent } from './asset-checkin/checkin.component';
import { HttpServiceInterceptor } from '../interceptors/http-service-interceptor';

@NgModule({
  declarations: [
    AssetCheckinComponent,
  ],
  imports: [
    BrowserModule, QwcModule, UserComponentsModule, CollectionComponentsModule, HttpClientModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpServiceInterceptor,
      multi: true
    },
    {
      provide: APP_INITIALIZER,
      useFactory: AuthServiceProviderFactory,
      deps: [AuthService, QueryService],
      multi: true
    }
  ],
  bootstrap: [AssetCheckinComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule { }

/**
 * Factory provider ensure that access token and query session Id has been genertated.
 */

export function AuthServiceProviderFactory(authService: AuthService, queryService: QueryService) {
  return () => {
    return authService.generateAccessToken().then(() => {
      return queryService.createWatchQuerySession();
    });
  };
}
