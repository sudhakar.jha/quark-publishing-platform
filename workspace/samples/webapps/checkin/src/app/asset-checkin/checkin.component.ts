import { Component } from '@angular/core';
import { CheckinStatus, QppNotificationService } from "@quark/platform-components-ng";
import { BehaviorSubject } from 'rxjs';

@Component({
    selector: 'checkin-component',
    templateUrl: 'checkin.component.html'
})
export class AssetCheckinComponent {

    uploadTypeToBeShown$ = new BehaviorSubject<string>(null);
    saveRevisionAssetId = -1;
    checkinStatus: CheckinStatus;

    constructor(private notificationService: QppNotificationService) {
        this.notificationService.init();
    }

    showCheckinProgress(checkinStatus: CheckinStatus) {
        this.checkinStatus = checkinStatus;
    }

    showFileDialog() {
        this.saveRevisionAssetId = -1;
        this.uploadTypeToBeShown$.next("files");
    }

    showFolderDialog() {
        this.saveRevisionAssetId = -1;
        this.uploadTypeToBeShown$.next("folder");
    }
}

