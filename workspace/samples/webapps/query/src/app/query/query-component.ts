import { Component, OnInit } from '@angular/core';
import { AdvanceSearchRequest } from '@quark/application-services-model';
import { QppNotificationService } from '@quark/platform-components-ng';

@Component({
  selector: 'query-component',
  templateUrl: 'query-component.html'
})
export class QueryComponent implements OnInit {

  queryId: any;

  advanceSearchRequest: AdvanceSearchRequest;

  queryResultConfig = {
    enableNotification: true,
    enableColumnModification: true,
    enableQuickActions: true
  };

  constructor(private notificationService: QppNotificationService) {
    this.notificationService.init();
  }

  ngOnInit() {
    this.initData();
  }

  initData() {

    this.queryId = -1;
    this.advanceSearchRequest = {
      searchConditions: [
        {
          logicalOperator: 1,
          comparisonOperator: 1,
          negated: false,
          parameterized: false,
          type: 19,
          contentTypeIds: [
            110452,             // Smart Document
            110451,             // Smart Section
          ],
          recursive: true,
        },
        {
          attributeId: 25,
          logicalOperator: 1,
          comparisonOperator: 1,
          negated: false,
          value: 1,
          type: 7
        }
      ],
      collectionIds: [                 // Search in Home Collection and its sub-folders
        1
      ],
      entity: 'asset',
      recursive: true,
      uiQueryDisplay: [],
      uiQuerySortColumns: []
    };
  }
}
