# Query Result

You can use a query result component to make your own custom search. A query result is a smart component that fetches data using web APIs.

---