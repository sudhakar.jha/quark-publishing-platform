## Visiblity Handler

1. The visibilityHandler is a function that gets a callback from the context in which
you perform an action through the User Interface. For example, Quark Publishing
Platform NextGen passes an entity object to the function visibilityHandler for
context menu and quick actions, and passes an asset object for tabs.  

2. To load the function visibilityHandler, the file __extensibility.json__ must be placed in the directory **applicationservice/conf** in the s3 bucket. The file extensibility.json contains "javascripts" property which is an array of the javascript file paths relative to the conf folder. For example, the path for the file entityactions.js in the folder conf is as follows:
```
{
    "javascripts": [
        "entity-actions.js"
    ]
}
```
---
