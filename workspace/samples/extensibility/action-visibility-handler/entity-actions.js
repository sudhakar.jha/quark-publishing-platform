/* 
  Attribute Id's for:
  CONTENT_TYPE = 62
  CONTENT_TYPE_HIERARCHY = 63
  COLLECTION = 55,
  COLLECTION_PATH: 57,
*/

// EntityActions namespace is used for the window object.
window.EntityActions = {
    // The function which is passed in entity-actions.json
    visibilityHandler : (entity) => {
        // return true to show the option and false to hide it
        return !(entity.type === "collection");
    }
}
