## Custom Action Submenu

This sample demonstrate how to configure a menu type action in workspace.
The menu needs to be specified in __entity-actions.json__ placed in the folder **applicationservice/conf** in the s3 bucket.

The sample code to configure a menu type action: 
```
{
    "id": "qpp-menu1",
    "type": "menu",
    "displayText": "Menu1",
    "iconUrl": "https://image.flaticon.com/icons/png/512/561/561184.png"
},
{
    "id": "qpp-menu2",
    "type": "menu",
    "displayText": "Menu2",
    "parentId": "qpp-menu1",
    "iconUrl": "https://image.flaticon.com/icons/png/512/561/561184.png"
}
```

In the above sample, action with id `qpp-menu2` is child of menu type action `qpp-menu1`.
---