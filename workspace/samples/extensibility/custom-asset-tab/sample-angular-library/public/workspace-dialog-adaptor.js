let WorkspaceDialogAdaptor = new function () {

    this.getParamValue = (paramName) => {
        let url = window.location.search.substring(1); //get rid of "?" in querystring
        let qArray = url.split('&'); //get key-value pairs
        for (let i = 0; i < qArray.length; i++) {
            let pArr = qArray[i].split('='); //split key and value
            if (pArr[0] == paramName)
                return pArr[1]; //return value
        }
    };

    this.getUserId = () => {
        return WorkspaceDialogAdaptor.getParamValue("userId")
    }

    this.getEntityId = () => {
        return WorkspaceDialogAdaptor.getParamValue("entityId")

    }

    this.closeDialog = () => {
        let dialogId = WorkspaceDialogAdaptor.getParamValue("dialogId");
        window.top.postMessage({
            "messageType": "closeDialog",
            "dialogId": dialogId
        }, "*")
    }
}
