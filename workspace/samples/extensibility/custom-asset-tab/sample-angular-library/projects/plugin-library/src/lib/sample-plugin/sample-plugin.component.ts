import { Component, OnDestroy } from "@angular/core";
import { AbstractPlugin, PluginEventSource } from "@quark/platform-components-ng";
import { Subscription } from "rxjs";

@Component({
  template: `
    <h5>Custom Angular Library - Asset Version {{entity.majorVersion}}.{{entity.minorVersion}}</h5>
    <hr/>
    <qpp-asset-workflow *ngIf="entity && entity.id" [collectionId]="entity?.collection?.id" [assetId]="entity.id" [isEditable]="true"
    (failure)="assetSaveFailureHandler($event)" (saved)="assetSaveHandler($event)"
    (action)="assetWorkflowActionHandler($event)">
    </qpp-asset-workflow>
    `
})
export class SamplePluginComponent implements AbstractPlugin, OnDestroy {

  subs = new Subscription();
  entity: any;
  entityId: any;

  constructor() { }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  onPluginActivated(pluginEventSources: PluginEventSource[]) {
    for (const pluginEventSource of pluginEventSources) {
      if (pluginEventSource.eventName === "entity") {
        this.subs.add(pluginEventSource.source.subscribe((entity) => {
          this.entity = entity;
        }));
      }
    }
  }

  assetSaveFailureHandler(event) {
    console.log(event);
  }

  assetSaveHandler(event) {
    console.log(event);
  }

  assetWorkflowActionHandler(event) {
    console.log(event);
  }
}

