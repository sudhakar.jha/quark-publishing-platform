import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { QwcModule } from "@quark/qwc";
import { SamplePluginComponent } from "./sample-plugin/sample-plugin.component";
import { CollectionComponentsModule } from "@quark/platform-components-ng";
import { CommonModule } from '@angular/common';
@NgModule({
  declarations: [SamplePluginComponent],
  imports: [QwcModule, CollectionComponentsModule, CommonModule],
  entryComponents: [SamplePluginComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PluginModule { }
