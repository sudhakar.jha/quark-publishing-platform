## Custom Asset Tabs

In workspace we have extensibility to add a custom tab in asset detail pane. The __entity-actions.json__ file contains a JSON object with key assetTabs defining metadata for the tabs in the asset detail view.

The following are some basic examples of each of the tab type:
* iframe
```
{
    "id": "qpp-iframe-tab",
    "type": "iframe",
    "frameUrl": "http://localhost:9001/custom-actiondialog/index.html",
    "hidden": false,
    "displayText": "Send to Sharepoint",
    "iconUrl":"http://icons.iconarchive.com/icons/dakirby309/simplystyled/32/Microsoft-SharePoint-2013-icon.png"
}
```
* library
```
{
    "id": "qpp-library-tab",
    "type": "library",
    "extensionPoint": "workspace/asset-detail/qppextensible-tab",
    "hidden": false,
    "displayText": "Email",
    "iconUrl": "https://1000logos.net/wpcontent/uploads/2021/04/Facebook-logo.png"
}
```
---