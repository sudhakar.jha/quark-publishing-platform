/*
  // Register callback and return false to stop operation.
  // Can update/change payload while submission.

  // Called before asset/folder checkin
  BEFORE_SAVE_SUBMIT

  // Called after asset/folder checkin
  AFTER_SAVE_SUBMIT

  // Called before asset details open like info/detail
  BEFORE_ASSET_DETAIL_ACTION

  // Called before asset preview action
  BEFORE_ASSET_PREVIEW_ACTION

  // Called before asset links/refernces action
  BEFORE_ASSET_LINKS_ACTION

  // Called before asset properties action
  BEFORE_ASSET_PROPERTIES_ACTION

  // Called before collection details
  BEFORE_COLLECTION_DETAIL_ACTION

  // Called before asset publish action
  BEFORE_PUBLISH_ASSET_ACTION

  // Called before asset submit/done for publishing
  BEFORE_PUBLISH_ASSET_SUBMIT

  // Called after asset published successfully
  AFTER_PUBLISH_ASSET_SUBMIT

  // Called before save revision action
  BEFORE_SAVE_REVISION_ACTION

  // Called before save revision submit/done
  BEFORE_SAVE_REVISION_SUBMIT

  // Called after revision saved
  AFTER_SAVE_REVISION_SUBMIT

  // Called before asset checkout/edit action
  BEFORE_EDIT_ASSET_ACTION

  // Called after asset checkout/edit action
  AFTER_EDIT_ASSET_ACTION

  // Called before asset download action
  BEFORE_DOWNLOAD_ASSET_ACTION

  // Called after asset download action
  AFTER_DOWNLOAD_ASSET_ACTION

  // Called before/after asset cancel checkout/discard action
  BEFORE_DISCARD_ASSET_ACTION
  AFTER_DISCARD_ASSET_ACTION

  // Called before/after asset redindex action
  BEFORE_REINDEX_ASSET_SUBMIT
  AFTER_REINDEX_ASSET_SUBMIT

  // Called before asset delete action
  BEFORE_DELETE_ASSET_ACTION

  // Called before/after asset delete submit/done
  BEFORE_DELETE_ASSET_SUBMIT
  AFTER_DELETE_ASSET_SUBMIT

  // Called before collection delete action
  BEFORE_DELETE_COLLECTION_ACTION

  // Called before/after collection delete submit/done
  BEFORE_DELETE_COLLECTION_SUBMIT
  AFTER_DELETE_COLLECTION_SUBMIT

  // Called before collection move action
  BEFORE_MOVE_COLLECTION_ACTION

  // Called before/after collection move submit/done
  BEFORE_MOVE_COLLECTION_SUBMIT
  AFTER_MOVE_COLLECTION_SUBMIT

  // Called before collection copy action
  BEFORE_COPY_COLLECTION_ACTION

  // Called before/after collection copy submit/done
  BEFORE_COPY_COLLECTION_SUBMIT
  AFTER_COPY_COLLECTION_SUBMIT

  // Called before/after asset duplicate action
  BEFORE_DUPLICATE_ASSET_ACTION
  AFTER_DUPLICATE_ASSET_ACTION

  // Called before collection duplicate action
  BEFORE_DUPLICATE_COLLECTION_ACTION

  // Called before/after collection duplicate submit/done
  BEFORE_DUPLICATE_COLLECTION_SUBMIT
  AFTER_DUPLICATE_COLLECTION_SUBMIT

  // Called before edit collection properties action
  BEFORE_EDIT_COLLECTION_PROPERTIES_ACTION

  // Called before/after edit collection properties submit/done
  BEFORE_EDIT_COLLECTION_PROPERTIES_SUBMIT
  AFTER_EDIT_COLLECTION_PROPERTIES_SUBMIT

  // Called before edit asset properties action
  BEFORE_EDIT_ASSET_PROPERTIES_ACTION 

  // Called before/after edit asset properties submit/done
  BEFORE_ASSET_PROPERTIES_SUBMIT
  AFTER_ASSET_PROPERTIES_SUBMIT

  // Called before/after workflow asset submit/done
  BEFORE_WORKFLOW_ASSET_SUBMIT
  AFTER_WORKFLOW_ASSET_SUBMIT

  // Called before file upload action
  BEFORE_ASSET_UPLOAD_ACTION

  // Called before folder upload action
  BEFORE_UPLOAD_FOLDER_ACTION

  // Called before workflow dialog is open when uploading file/folder
  // and workflow is set to mandatory.
  BEFORE_WORKFLOW_DIALOG_OPEN
*/

// Example: Prevent checkin in collection.
window.CallbackService.registerCallback("BEFORE_SAVE_SUBMIT", (data) => {
  console.log("BEFORE_SAVE_SUBMIT", data);
  if (data.assetInfo.collectionId === 1) {
    window.alert("Not allowed");
    return false;
  }
});
// Example: Perform additional actions on newely checkin asset bases upon asset id.
window.CallbackService.registerCallback("AFTER_SAVE_SUBMIT", (data) => {
  console.log("AFTER_SAVE_SUBMIT", data.assetId);
});

window.CallbackService.registerCallback("BEFORE_DELETE_ASSET_ACTION", (data) => {
  console.log("BEFORE_DELETE_ASSET_ACTION", data);
});
window.CallbackService.registerCallback("BEFORE_DELETE_ASSET_SUBMIT", (data) => {
  console.log("BEFORE_DELETE_ASSET_SUBMIT", data);
});
window.CallbackService.registerCallback("AFTER_DELETE_ASSET_SUBMIT", (data) => {
  console.log("AFTER_DELETE_ASSET_SUBMIT", data);
});
window.CallbackService.registerCallback("BEFORE_DELETE_COLLECTION_ACTION", (data) => {
  console.log("BEFORE_DELETE_COLLECTION_ACTION", data);
});
window.CallbackService.registerCallback("BEFORE_DELETE_COLLECTION_SUBMIT", (data) => {
  console.log("BEFORE_DELETE_COLLECTION_SUBMIT", data);
});
window.CallbackService.registerCallback("AFTER_DELETE_COLLECTION_SUBMIT", (data) => {
  console.log("AFTER_DELETE_COLLECTION_SUBMIT", data);
});
window.CallbackService.registerCallback("BEFORE_DUPLICATE_ASSET_ACTION", (data) => {
  console.log("BEFORE_DUPLICATE_ASSET_ACTION", data);
});
window.CallbackService.registerCallback("AFTER_DUPLICATE_ASSET_ACTION", (data) => {
  console.log("AFTER_DUPLICATE_ASSET_ACTION", data);
});
window.CallbackService.registerCallback("BEFORE_EDIT_COLLECTION_PROPERTIES_SUBMIT", (data) => {
  console.log("BEFORE_EDIT_COLLECTION_PROPERTIES_SUBMIT", data);
});
window.CallbackService.registerCallback("AFTER_EDIT_COLLECTION_PROPERTIES_SUBMIT", (data) => {
  console.log("AFTER_EDIT_COLLECTION_PROPERTIES_SUBMIT", data);
});
window.CallbackService.registerCallback("BEFORE_EDIT_ASSET_ACTION", (data) => {
  console.log("BEFORE_EDIT_ASSET_ACTION", data);
});
window.CallbackService.registerCallback("AFTER_EDIT_ASSET_ACTION", (data) => {
  console.log("AFTER_EDIT_ASSET_ACTION", data);
});
window.CallbackService.registerCallback("BEFORE_DUPLICATE_COLLECTION_ACTION", (data) => {
  console.log("BEFORE_DUPLICATE_COLLECTION_ACTION", data);
});
window.CallbackService.registerCallback("BEFORE_DUPLICATE_COLLECTION_SUBMIT", (data) => {
  console.log("BEFORE_DUPLICATE_COLLECTION_SUBMIT", data);
});
window.CallbackService.registerCallback("AFTER_DUPLICATE_COLLECTION_SUBMIT", (data) => {
  console.log("AFTER_DUPLICATE_COLLECTION_SUBMIT", data);
});
window.CallbackService.registerCallback("BEFORE_COPY_COLLECTION_ACTION", (data) => {
  console.log("BEFORE_COPY_COLLECTION_ACTION", data);
});
window.CallbackService.registerCallback("BEFORE_COPY_COLLECTION_SUBMIT", (data) => {
  console.log("BEFORE_COPY_COLLECTION_SUBMIT", data);
});
window.CallbackService.registerCallback("AFTER_COPY_COLLECTION_SUBMIT", (data) => {
  console.log("AFTER_COPY_COLLECTION_SUBMIT", data);
});
window.CallbackService.registerCallback("BEFORE_MOVE_COLLECTION_ACTION", (data) => {
  console.log("BEFORE_MOVE_COLLECTION_ACTION", data);
});
window.CallbackService.registerCallback("BEFORE_MOVE_COLLECTION_SUBMIT", (data) => {
  console.log("BEFORE_MOVE_COLLECTION_SUBMIT", data);
});
window.CallbackService.registerCallback("AFTER_MOVE_COLLECTION_SUBMIT", (data) => {
  console.log("AFTER_MOVE_COLLECTION_SUBMIT", data);
});
window.CallbackService.registerCallback("BEFORE_WORKFLOW_ASSET_SUBMIT", (data) => {
  console.log("BEFORE_WORKFLOW_ASSET_SUBMIT", data);
});
window.CallbackService.registerCallback("AFTER_WORKFLOW_ASSET_SUBMIT", (data) => {
  console.log("AFTER_WORKFLOW_ASSET_SUBMIT", data);
});
window.CallbackService.registerCallback("BEFORE_SAVE_REVISION_SUBMIT", (data) => {
  console.log("BEFORE_SAVE_REVISION_SUBMIT", data);
});
window.CallbackService.registerCallback("AFTER_SAVE_REVISION_SUBMIT", (data) => {
  console.log("AFTER_SAVE_REVISION_SUBMIT", data);
});
window.CallbackService.registerCallback("BEFORE_REINDEX_ASSET_SUBMIT", (data) => {
  console.log("BEFORE_REINDEX_ASSET_SUBMIT", data);
});
window.CallbackService.registerCallback("AFTER_REINDEX_ASSET_SUBMIT", (data) => {
  console.log("AFTER_REINDEX_ASSET_SUBMIT", data);
});
window.CallbackService.registerCallback("BEFORE_DOWNLOAD_ASSET_ACTION", (data) => {
  console.log("BEFORE_DOWNLOAD_ASSET_ACTION", data);
});
window.CallbackService.registerCallback("AFTER_DOWNLOAD_ASSET_ACTION", (data) => {
  console.log("AFTER_DOWNLOAD_ASSET_ACTION", data);
});
window.CallbackService.registerCallback("BEFORE_DISCARD_ASSET_ACTION", (data) => {
  console.log("BEFORE_DISCARD_ASSET_ACTION", data);
});
window.CallbackService.registerCallback("AFTER_DISCARD_ASSET_ACTION", (data) => {
  console.log("AFTER_DISCARD_ASSET_ACTION", data);
});
window.CallbackService.registerCallback("BEFORE_EDIT_COLLECTION_PROPERTIES_ACTION", (data) => {
  console.log("BEFORE_EDIT_COLLECTION_PROPERTIES_ACTION", data);
});
window.CallbackService.registerCallback("BEFORE_EDIT_ASSET_PROPERTIES_ACTION", (data) => {
  console.log("BEFORE_EDIT_ASSET_PROPERTIES_ACTION", data);
});
window.CallbackService.registerCallback("BEFORE_PUBLISH_ASSET_ACTION", (data) => {
  console.log("BEFORE_PUBLISH_ASSET_ACTION", data);
});
window.CallbackService.registerCallback("BEFORE_PUBLISH_ASSET_SUBMIT", (data) => {
  console.log("BEFORE_PUBLISH_ASSET_SUBMIT", data);
});
window.CallbackService.registerCallback("AFTER_PUBLISH_ASSET_SUBMIT", (data) => {
  console.log("AFTER_PUBLISH_ASSET_SUBMIT", data);
});
window.CallbackService.registerCallback("BEFORE_ASSET_UPLOAD_ACTION", (data) => {
  console.log("BEFORE_ASSET_UPLOAD_ACTION", data);
});
window.CallbackService.registerCallback("BEFORE_UPLOAD_FOLDER_ACTION", (data) => {
  console.log("BEFORE_UPLOAD_FOLDER_ACTION", data);
});
window.CallbackService.registerCallback("BEFORE_ASSET_PROPERTIES_SUBMIT", (data) => {
  console.log("BEFORE_ASSET_PROPERTIES_SUBMIT", data);
});
window.CallbackService.registerCallback("AFTER_ASSET_PROPERTIES_SUBMIT", (data) => {
  console.log("AFTER_ASSET_PROPERTIES_SUBMIT", data);
});
window.CallbackService.registerCallback("BEFORE_ASSET_DETAIL_ACTION", (data) => {
  console.log("BEFORE_ASSET_DETAIL_ACTION", data);
});
window.CallbackService.registerCallback("BEFORE_COLLECTION_DETAIL_ACTION", (data) => {
  console.log("BEFORE_COLLECTION_DETAIL_ACTION", data);
});
window.CallbackService.registerCallback("BEFORE_SAVE_REVISION_ACTION", (data) => {
  console.log("BEFORE_SAVE_REVISION_ACTION", data);
});
window.CallbackService.registerCallback("BEFORE_ASSET_PREVIEW_ACTION", (data) => {
  console.log("BEFORE_ASSET_PREVIEW_ACTION", data);
});
window.CallbackService.registerCallback("BEFORE_ASSET_LINKS_ACTION", (data) => {
  console.log("BEFORE_ASSET_LINKS_ACTION", data);
});
window.CallbackService.registerCallback("BEFORE_ASSET_PROPERTIES_ACTION", (data) => {
  console.log("BEFORE_ASSET_PROPERTIES_ACTION", data);
});
window.CallbackService.registerCallback("BEFORE_WORKFLOW_DIALOG_OPEN", (data) => {
  console.log("BEFORE_WORKFLOW_DIALOG_OPEN", data);
  
  data.attributes.revisionComments = "Updated from callback"

  // Used To silently checkin file without UI.
  data.silentSubmit = false;

  // To create majorVersion
  data.attributes.isMinorVersion = true;
  data.attributes.collection = { id: 6877 }
  data.attributes.status = { "id": 2664, "workflowId": 2627 }
  data.attributes.routedTo = [{ "id": 1 }, { "id": 1124 }, { "id": 2433 }, { "id": 2 }]
  data.attributes.collectionClass = { id: 1103 }
  data.attributes.assetAttributes = [
    {
      "attributeId": 4592,
      "attributeValue": {
        "value": false
      },
    },
    {
      "attributeId": 4594,
      "attributeValue": {
        "value": [
          {
            "id": 3,
            "name": "PB",
            "domainId": 1001,
            "sourceReference": null,
            "childDomainValues": null,
            "hasChildren": false
          },
          {
            "id": 5,
            "name": "HR",
            "domainId": 1001,
            "sourceReference": null,
            "childDomainValues": null,
            "hasChildren": false
          },
          {
            "id": 4,
            "name": "Mohali",
            "domainId": 1001,
            "sourceReference": null,
            "childDomainValues": null,
            "hasChildren": false
          },
          {
            "id": 9,
            "name": "Amritsar",
            "domainId": 1001,
            "sourceReference": null,
            "childDomainValues": null,
            "hasChildren": false
          },
          {
            "id": 8,
            "name": "Panhkula",
            "domainId": 1001,
            "sourceReference": null,
            "childDomainValues": null,
            "hasChildren": false
          },
          {
            "id": 2,
            "name": "India",
            "domainId": 1001,
            "sourceReference": null,
            "childDomainValues": null,
            "hasChildren": false
          },
          {
            "id": 1,
            "name": "US",
            "domainId": 1001,
            "sourceReference": null,
            "childDomainValues": null,
            "hasChildren": false
          }
        ]
      }
    },
    {
      "attributeId": 5301,
      "attributeValue": {
        "value": "sample checkin"
      },
    },
    {
      "attributeId": 5306,
      "attributeValue": {
        "value": 18874368.00176
      },
    },
    {
      "attributeId": 5302,
      "attributeValue": {
        "value": 12
      },
    },
    {
      "attributeId": 5303,
      "attributeValue": {
        "value": "2021-09-30T07:07:23Z"
      },
    },
    {
      "attributeId": 5304,
      "attributeValue": {
        "value": "2020-09-30"
      },
    },
    {
      "attributeId": 5305,
      "attributeValue": {
        "value": "00:00:00"
      },
    },
    {
      "attributeId": 5309,
      "attributeValue": {
        "value": {
          "id": 1120,
          "userName": "alpha",
          "firstName": "",
          "lastName": null,
          "fullName": "alpha",
          "role": {
            "id": 1110,
            "name": "Administrator Copy"
          },
          "enabled": true,
          "email": null,
          "phoneNumber": null,
          "native": true,
          "user": true,
          "deleted": false,
          "licenseType": null,
          "domainId": 3,
          "sourceReference": null,
          "childDomainValues": null,
          "hasChildren": false
        }
      },
    }
  ]
  return data
});
