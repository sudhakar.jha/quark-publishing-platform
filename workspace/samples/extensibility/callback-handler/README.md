## Callback Handler

A custom JavaScript file that registers callback hooks/events. The javascript file needs to be specified in __extensibility.json__ placed in the folder **applicationservice/conf** in the
s3 bucket.  

The sample code to register an event is as follows:
Purpose: Prevent check-in in collection.  

```
window.CallbackService.registerCallback("BEFORE_SAVE_SUBMIT",(data) => {
    console.log("BEFORE_SAVE_SUBMIT", data);
    if (data.assetInfo.collectionId === 1) {
        window.alert("Not allowed");
        return false;
    }
});
```
---