## Custom Action Dialog

This sample demonstrate how to configure a dialog type action in workspace.
The dialog needs to be specified in __entity-actions.json__ placed in the folder **applicationservice/conf** in the s3 bucket.

The sample code to configure a dialog type action: 
```
{
    "id": "qpp-send-email-id",
    "type": "dialog",
    "hidden": false,
    "displayText": "Email",
    "iconUrl":
    "https://icons.iconarchive.com/icons/graphicloads/100-flat/48/email-2-icon.png",
    "dialogUrl": "http://localhost:9000/custom-actiondialog/index.html",
    "dialogConfig": {
    "height": 200,
    "width": 500
}
```
---
