# Extensibility in Workspace

## Extensibility in Entity Actions

To add extensible actions to entities:  
1. Create a folder __conf__ in the directory **applicationservice** in the s3 bucket.  
![s3 location](./images/config-path.png)
2. Add the file **entity-actions.json** to this folder. The file **entity-actions.json**
contains the following extensible entity actions and blade actions:
    * **assetContextMenuActions**: These are the actions that Quark Publishing  
    Platform NextGen displays when you select the ellipses icon associated to an entity - asset or collection.  
    ![asset context menu](./images/asset-context-menu-actions.png)
    * **collectionContextMenuActions**: These are the actions that Quark Publishing  
    Platform NextGen displays when you select the ellipses icon associated to a collection  
    ![collection context menu](./images/collection-context-menu-actions.png)
    * **assetQuickActions**: These are the actions that Quark Publishing  
    Platform NextGen displays next to an asset.  
    ![asset quick actions](./images/asset-quick-actions.png)
    * **collectionQuickActions**: These are the actions that Quark Publishing  
    Platform NextGen displays next to a collection.  
    ![collection quick actions](./images/collection-quick-actions.png)  
    * **otherActions**: These are the actions that Quark Publishing Platform NextGen displays across the Workspace User Interface, which could be asset/action specific or universal.
    ![other actions](./images/other-actions.png)

      ![other actions 1](./images/other-actions-1.png)

### Action Objects
An action object can have the following attributes based on the type of action:  

| Attribute      | Applicable to action type | Description     |
| :---        |    :----   |          :--- |
| id      | All       | A unique id of an action   |
| type   | All        | An action type specifies the function an action performs. There are four action types:|
| displayText | All | The name of the action on the User Interface. |
| hidden | All | A Boolean value to show/hide the action button. |
| iconUrl | All | The URL of the icon for an action button | 
| VisibilityHandler | All | Name of the function to handle visibility for actions.|
| actionUrl | endpoint | The URL of the API endpoint. |
| dialogUrl | dialog | The location of the iframe index page.|
| dialogConfig {height; width;}| dialog | The attribute to specify the height and width of the dialog box. When you do not specify these dimensions, Quark Publishing. Platform NextGen assign a default dimension of 600px X 600px.  |
| tabId | tab | The id of the tab that you select in the **accessQuickActions.** |
| parentId | menu | The Id of the menu type to which an action in the context menu belongs to. |
| context | All (but specific to topBarViewActions)| Context defines the OTB actions that Quark Publishing. Platform NextGen displays based on the tab you select. In the image below, each of the tabs - Home, Favorites, My Assignments, Continue Editing, Searches, and Browse - has its own context which decides the actions that appear on the top bar. ![diff-blades](./images/diff-blades.png) |

**NOTE:**
`An otherActions action entity only has two attributes - id and visibilityHandler.`
### Action Type Examples
The following are some basic examples of each of the action types  
* **Endpoint**
```
{
    “id”: “qpp-send-to-sharepoint-id”,
    “type”: “endpoint”,
    “hidden”: true,
    “displayText”: “Send to Sharepoint”,
    “iconUrl”:
    “http://icons.iconarchive.com/icons/dakirby309/simplystyled/32/Microsoft-SharePoint-2013-icon.png”,
    “actionUrl”: “/customer/api/sendToSharepoint”
}
```
* **Dialog**
```
{
    “id”: “qpp-send-email-id”,
    “type”: “dialog”,
    “hidden”: false,
    “displayText”: “Email”,
    “iconUrl”:
    “https://icons.iconarchive.com/icons/graphicloads/100-
    flat/48/email-2-icon.png”,
    “dialogUrl”: “http://localhost:9000/custom-actiondialog/index.html”,
    “dialogConfig”: {
        “height”: 200,
        “width”: 500
    }
}
```
[Download](./custom-action-dialog/sample.zip) sample for dialog.
    
---
* **Tab**
```
{
    “id”: “qpp-library-action”,
    “displayText”: “Library”,
    “hidden”: false,
    “iconUrl”: “https://1000logos.net/wpcontent/uploads/2021/04/Facebook-logo.png”,
    “type”: “tab”,
    “tabId”: “qpp-library-tab”
}
```
* **Menu**
```
{
    "id": "qpp-menu1",
    "type": "menu",
    "displayText": "Menu1",
    "iconUrl": "https://image.flaticon.com/icons/png/512/561/561184.png"
},
{
    "id": "qpp-menu2",
    "type": "menu",
    "displayText": "Menu2",
    "parentId": "qpp-menu1",
    "iconUrl": "https://image.flaticon.com/icons/png/512/561/561184.png"
}
```
`Here qpp-menu2 is a child of qpp-menu1.`

[Download](./custom-action-submenu/sample.zip) sample for menu.

---
### Visibility Handler
The visibilityHandler is a function that gets a callback from the context in which
you perform an action through the User Interface. For example, Quark Publishing
Platform NextGen passes an entity object to the function visibilityHandler for
context menu and quick actions, and passes an asset object for tabs.  
<br/>To load the function visibilityHandler, the file extensibility.json must be the folder **conf** in the directory **applicationservice** in the s3 bucket. The file **extensibility.json** contains JavaScript property, which is an array of the paths of
JavaScript relative to the folder **conf**. For example, the path for the file **entity-actions.js** in the folder **conf** is as follows:
```
{
    “javascripts”: [
        “entity-actions.js”
    ]
}
```

[Download](./action-visibility-handler/sample.zip) sample for visibilityHandler.

---    
For iconUrl, you can use an absolute URL or you can add a custom image to the
folder **conf/assets** and then use the protocol “s3://” to load the image from relative
path. For example, the iconUrl “s3://people.png” loads the image **people.png**
available at the location **applicationservice/conf/assets**.
The entity-actions config is merged with the default action config listed below.  
<br/> **The Default entity-actions.json**
```
{
    "version": "0.0.1",
    "assetContextMenuActions": [
        {
            "id": "qpp-checkout-action",
            "displayText": "Edit",
            "hidden": false,
            "iconUrl": "assets/icons/action-icons/ico-dropdown-edit.png"
        },
        {
            "id": "qpp-read-only-action",
            "displayText": "Read Only",
            "hidden": false,
            "iconUrl": "assets/icons/action-icons/ico-read-only.png"
        },
        {
            "id": "qpp-save-revision-action",
            "displayText": "Update Revision",
            "hidden": false,
            "iconUrl": "assets/icons/action-icons/ico-update-version.png"
        },
        {
            "id": "qpp-download-action",
            "displayText": "Download",
            "hidden": false,
            "iconUrl": "assets/icons/action-icons/ico-download.png"
        },
        {
            "id": "qpp-reindex-action",
            "displayText": "ReIndex",
            "hidden": false,
            "iconUrl": "assets/icons/action-icons/ico-reindex.png"
        },
        {
            "id": "qpp-duplicate-action",
            "displayText": "Duplicate",
            "hidden": false,
            "iconUrl": "assets/icons/action-icons/ico-duplicate.png"
        },
        {
            "id": "qpp-publish-action",
            "displayText": "Publish",
            "hidden": false,
            "iconUrl": "assets/icons/action-icons/ico-publish.png"
        },
        {
            "id": "qpp-discard-changes-action",
            "displayText": "Discard",
            "hidden": false,
            "iconUrl": "assets/icons/action-icons/ico-discard.png"
        },
        {
            "id": "qpp-delete-action",
            "displayText": "Delete",
            "hidden": false,
            "iconUrl": "assets/icons/action-icons/ico-delete.png"
        }
    ],
    "collectionContextMenuActions": [
        {
            "id": "qpp-properties-action",
            "displayText": "Properties",
            "hidden": false,
            "iconUrl": "assets/icons/action-icons/ico-details.png"
        },
        {
            "id": "qpp-duplicate-action",
            "displayText": "Duplicate",
            "hidden": false,
            "iconUrl": "assets/icons/action-icons/ico-duplicate.png"
        },
        {
            "id": "qpp-copy-collection-action",
            "displayText": "Copy",
            "hidden": false,
            "iconUrl": "assets/icons/action-icons/collection-duplicate.png"
        },
        {
            "id": "qpp-move-collection-action",
            "displayText": "Move",
            "hidden": false,
            "iconUrl": "assets/icons/action-icons/collection-move.png"
        },
        {
            "id": "qpp-delete-action",
            "displayText": "Delete",
            "hidden": false,
            "iconUrl": "assets/icons/action-icons/ico-delete.png"
        }
    ],
    "assetQuickActions": [
        {
            "id": "qpp-checkout-action",
            "displayText": "Edit",
            "hidden": false,
            "iconUrl": "assets/icons/action-icons/ico-dropdown-edit.png"
        },
        {
            "id": "qpp-preview-action",
            "type": "tab",
            "tabId": "qpp-preview-tab",
            "displayText": "Preview",
            "hidden": false,
            "iconUrl": "assets/icons/action-icons/ico-preview.png"
        },
        {
            "id": "qpp-properties-action",
            "type": "tab",
            "tabId": "qpp-properties-tab",
            "displayText": "Properties",
            "hidden": false,
            "iconUrl": "assets/icons/action-icons/ico-info.png"
        },
        {
            "id": "qpp-links-action",
            "type": "tab",
            "tabId": "qpp-links-tab",
            "displayText": "Links",
            "hidden": false,
            "iconUrl": "assets/icons/action-icons/ico-links.png"
        }
    ],
    "collectionQuickActions": [
        {
            "id": "qpp-properties-action",
            "displayText": "Properties",
            "hidden": false,
            "iconUrl": "assets/icons/action-icons/ico-info.png"
        }
    ],
    "assetTabs": [
        {
            "id": "qpp-preview-tab",
            "displayText": "Preview",
            "hidden": false,
            "iconUrl": "assets/icons/action-icons/ico-preview.png"
        },
        {
            "id": "qpp-history-tab",
            "displayText": "History",
            "hidden": false,
            "iconUrl": "assets/icons/action-icons/ico-history.png"
        },
        {
            "id": "qpp-properties-tab",
            "displayText": "Properties",
            "hidden": false,
            "iconUrl": "assets/icons/action-icons/ico-info.png"
        },
        {
            "id": "qpp-links-tab",
            "displayText": "Links",
            "hidden": false,
            "iconUrl": "assets/icons/action-icons/ico-links.png"
        },
        {
            "id": "qpp-audit-tab",
            "displayText": "Audit",
            "hidden": false,
            "iconUrl": "assets/icons/action-icons/ico-audit.png"
        }
    ],
    "topbarViewActions": [
        {
            "id": "qpp-upload-action",
            "context": ["favorite","assignment","checkout","search","browse"]
        },
        {
            "id": "qpp-upload-file-action",
            "context": ["favorite","checkout","search","browse"]
        },
        {
            "id": "qpp-upload-folder-action",
            "context": ["favorite","assignment","checkout","search","browse"]
        },
        {
            "id": "qpp-go-to-location-action",
            "context": ["favorite","assignment","checkout","search"]
        },
        {
            "id": "qpp-more-favorite-action", 
            "context": ["favorite"]
        },
        {
            "id": "qpp-remove-from-favorites-action",
            "context": ["favorite"]
        },
        {
            "id": "qpp-more-asset-action", 
            "context": ["assignment", "checkout","search","browse","asset-detail"]
        },
        {
            "id": "qpp-checkout-action",
            "context": ["assignment","checkout","search","browse","asset-detail"]
        },
        {
            "id": "qpp-save-revision-action",
            "context": ["assignment","checkout","search","browse"]
        },
        {
            "id": "qpp-download-action",
            "context": ["assignment","checkout","search","browse","asset-detail"]
        },
        {
            "id": "qpp-export-action",
            "context": ["assignment","checkout"]
        },
        {
            "id": "qpp-reindex-action",
            "context": ["assignment","checkout","search","browse","asset-detail"]
        },
        {
            "id": "qpp-duplicate-action",
            "context": ["assignment","checkout","search","browse"]
        },
        {
            "id": "qpp-update-data-action",
            "context": ["assignment","checkout","search","browse","asset-detail"]
        },
        {
            "id": "qpp-discard-changes-action",
            "context": ["assignment","checkout","search","browse","asset-detail"]
        },
        {
            "id": "qpp-delete-action",
            "context": ["assignment","checkout","search","browse"]
        },
        {
            "id": "qpp-edit-properties-action", 
            "context": ["assignment","checkout","search","browse"]
        },
        {
            "id": "qpp-new-search-action",
            "context": ["search"]
        },
        {
            "id": "qpp-duplicate-search-action",
            "context": ["search"]
        },
        {
            "id": "qpp-edit-search-action",
            "context": ["search"]
        },
        {
            "id": "qpp-discard-search-action",
            "context": ["search"]
        },
        {
            "id": "qpp-share-search-action",
            "context": ["search"]
        },
        {
            "id": "qpp-more-search-action",
            "context": ["search"]
        },
        {
            "id": "qpp-export-search-action",
            "context": ["search"]
        },
        {
            "id": "qpp-delete-search-action",
            "context": ["search"]
        },
        {
            "id": "qpp-new-collection-dropdown-action",
            "context": ["browse"]
        },
        {
            "id": "qpp-new-collection-action",
            "context": ["browse"]
        },
        {
            "id": "qpp-new-collection-template-action",
            "context": ["browse"]
        },
        {
            "id": "qpp-new-collection-from-template-action",
            "context": ["browse"]
        },
        {
            "id": "qpp-collection-properties-action",
            "context": ["browse"]
        },
        {
            "id": "qpp-collection-duplicate-action",
            "context": ["search","browse"]
        },
        {
            "id": "qpp-more-collection-action", 
            "context": ["search","browse"]
        },
        {
            "id": "qpp-copy-collection-action",
            "context": ["search","browse"]
        },
        {
            "id": "qpp-move-collection-action",
            "context": ["search","browse"]
        },
        {
            "id": "qpp-delete-collection-action",
            "context": ["search","browse"]
        },
        {
            "id": "qpp-start-workflow-action",
            "context": ["asset-detail"]
        },
        {
            "id": "qpp-change-workflow-action",
            "context": ["asset-detail"]
        }
    ],
    "otherActions": [
        {
            "id": "qpp-open-in-read-only-action"
        },
        {
            "id": "qpp-revert-to-version-action"
        }
    ]
}
```

## Extensibility in Tabs

The entity-actions file contains a JSON object that has the key **assetTabs**, which
contains metadata for the tabs in the asset detail view.

![asset tabs](./images/asset-tabs.png)  
Based on the type, tab objects can have the following attributes:

| Attribute      | Applicable to action type | Description     |
| :---        |    :----   |          :--- |
| id      | All       | A unique id of an tab.   |
| type   | All        | A tab can be of the following two types:|
| displayText | All | The name of the tab on the User Interface. |
| hidden | All | A Boolean value to show/hide the tab. |
| frameUrl | iframe | The URL of the destination that you want to open in the iframe.  |
| extensionPoint | library | The extension point to use to load a custom angular library plugin. | 
| VisibilityHandler | All | Name of the function to handle visibility for tabs.|

### Tab Type Examples
The following are some basic examples of each of the tab type:  
**iframe**
```
{
    “id”: “qpp-iframe-tab”,
    “type”: “iframe”,
    “frameUrl”: “http://localhost:9001/custom-actiondialog/index.html”,
    “hidden”: false,
    “displayText”: “Send to Sharepoint”,
    “iconUrl”:”http://icons.iconarchive.com/icons/dakirby309/simplystyled/32/Microsoft-SharePoint-2013-icon.png”
}
```
**library**
```
{
    “id”: “qpp-library-tab”,
    “type”: “library”,
    “extensionPoint”: “workspace/asset-detail/qppextensible-tab”,
    “hidden”: false,
    “displayText”: “Email”,
    “iconUrl”: “https://1000logos.net/wpcontent/uploads/2021/04/Facebook-logo.png”
}
```

[Download](./custom-asset-tab/sample.zip) sample for tab.

---
### Custom Angular Library Plugin
To create a custom angular library plugin
1. Create a new angular library module A1
2. Add the following dependencies to A1:
    1. @quark/platform-components-ng
    2. @quark/qwc
3. Create a new class component that implements the class component **AbstractPlugin** in the module platform-components-ng.
4. Override the method **onPluginActivated** that listens for the callback from
the custom tab component and carries **BehaviorSubject** for the event data.  
![library code](./images/library-code.png)  
5. After you have created an angular library, host the JavaScript UMD file at a
location of your choice. You can find the location in the distribution folder of your angular project.  
![library bundle](./images/library-bundle.png)  
6. Create the file **plugins-config.json** and store it in the folder **conf** in the
directory **applicationservice** in the **s3** bucket. The file **plugins-config.json**
contains an array of plugin objects.  

A plugin object has the following attributes:  

| Attribute| Description |
| :--- |:--- |
| id | A unique id of a plugin.|
| name | The name of a plugin.|
| description | The description of a plugin.|
| extensionPoint | A custom extenstionPoint tells where to load a plugin. For custom library type, you should use the same extensionPoint.|
| url | The complete URL to the single JavaScript UMD bundle in an angular library module|
| ngLibrary | This contains the following three fields: | 
| enabled | attribute to enable/disable a plugin.|

**Sample plugins-config.json**
```
[
    {
        “id”: 1,
        “name”: “Sample Library”,
        “description”: “Plugin load custom angular library plugin.”,
        “extensionPoint”: “workspace/asset-detail/qpp-extensibletab”,
        “url”: “http://localhost:9000/pluginlibrary/bundles/plugin-library.umd.js”,
        “ngLibrary”: {
            “name”: “plugin-library”,
            “module”: “PluginModule”,
            “component”: “SamplePluginComponent”
        },
        “enabled”: true
    }
]
```

**NOTE:**  
`To call any application REST API from the iframe, the frame URL must have the same origin (domain + port) as the host application.`

`When you edit or add any plugin through admin portal, Quark Publishing Platform
NextGen reverts the changes.`

## Callback Hooks/Events
A custom JavaScript file that registers callback hooks/events must be available in the
file **extensibility.json** in the folder **conf** in the directory **applicationservice** in the **s3** bucket.  
The sample code to register an event is as follows:  (Purpose: Prevent check-in in collection.)

```
window.CallbackService.registerCallback(“BEFORE_SAVE_SUBMIT”,
    (data) => {
        console.log(“BEFORE_SAVE_SUBMIT”, data);
        if (data.assetInfo.collectionId === 1) {
        window.alert(“Not allowed”);
        return false;
    }
});
```
[Download](./callback-handler/sample.zip) sample for callback hooks.

---    
The following events are available in Workspace:
* BEFORE_SAVE_SUBMIT  
    Triggers before asset/folder checkin  
    **Event data:**  
    * assetInfo: CheckinEventData - user modifiable
    * isFolder: boolean – readOnly
    * assetId: number - readOnly

    __Return__ { assetInfo } to update payload or false to cancel the event

* AFTER_SAVE_SUBMIT  
    Triggers after asset/folder checkin  
    **Event data:**  
    * files: Set<File> - Set of files checked in
    * assetInfo: CheckinEventData
    * isFolder: boolean – isFolder or not
    * assetIds: number[] - Updated/Created Asset ids
    * collectionIds: number[] - Created Collection ids
    * assetId: number - Asset Id - only applicable for save resvision.
    * attributes: Object - Attributes - only applicable for save resvision.

* BEFORE_ASSET_DETAIL_ACTION  
    Triggers before asset details open like info/detail  
    **Event data:**  
    * entity: Entity - Entity of Asset
    * assetId: number - Asset Id
    * tabId: string - Id of the tab to be opened
    * actionId: string - Id of the action
    
    __Return__ false to cancel the event

* BEFORE_ASSET_PREVIEW_ACTION  
    Triggers before asset preview action  
    **Event data:**  
    * entity: Entity - Entity of Asset
    * assetId: number - Asset Id
    * tabId: string - Id of the tab to be opened
    * actionId: string - Id of the action
    
    __Return__ false to cancel the event

* BEFORE_ASSET_LINKS_ACTION  
    Triggers before asset links/refernces action  
    **Event data:**  
    * entity: Entity - Entity of Asset
    * assetId: number - Asset Id
    * tabId: string - Id of the tab to be opened
    * actionId: string - Id of the action
    
    __Return__ false to cancel the event

* BEFORE_ASSET_PROPERTIES_ACTION  
    Triggers before asset properties action  
    **Event data:**  
    * entity: Entity - Entity of Asset
    * assetId: number - Asset Id
    * tabId: string - Id of the tab to be opened
    * actionId: string - Id of the action
    
    __Return__ false to cancel the event

* BEFORE_COLLECTION_DETAIL_ACTION  
    Triggers before collection details  
    **Event data:**  
    * entity: Entity - Entity of Collection
    * collectionId: number - Collection Id 
    
    __Return__ false to cancel the event

* BEFORE_PUBLISH_ASSET_ACTION  
    Triggers before asset publish action  
    **Event data:**  
    * entity: Entity - Entity of Asset 
    
    __Return__ false to cancel the event

* BEFORE_PUBLISH_ASSET_SUBMIT  
    Triggers before asset submit/done for publishing  
    **Event data:**  
    * entity: Entity - Entity of Asset 
    * publishingChannels: object[] - Information about selected publishing channels       

    __Return__ { entity, publishingChannels } to update payload or false to cancel the event

* AFTER_PUBLISH_ASSET_SUBMIT  
    Triggers after asset published successfully  
    **Event data:**  
    * entity: Entity - Entity of Asset 
    * downloadInfo: object[] - Information about published content like downloadUrl.

* BEFORE_SAVE_REVISION_ACTION  
    Triggers before save revision action  
    **Event data:**  
    * entity: Entity - Entity of Asset
     
    __Return__ false to cancel the event

* BEFORE_SAVE_REVISION_SUBMIT  
    Triggers before save revision submit/done  
    **Event data:**  
    * assetId: number - Asset Id
    * attributes: object - Contains attributes information

    __Return__ { attributes } to update payload or false to cancel the event

* BEFORE_EDIT_ASSET_ACTION  
    Triggers before asset checkout/edit action  
    **Event data:**  
    *	assetId: number - Asset Id
    *	contentType: string - Content Type of Asset
    *	entity: Entity - Entity of Asset

    __Return__ false to cancel the event

* AFTER_EDIT_ASSET_ACTION  
    Triggers after asset checkout/edit action  
    **Event data:**  
    *	assetId: number - Asset Id
    *	contentType: string - Content Type of Asset
    *	entity: Entity - Entity of Asset
    *	editDirectly: boolean - Whether the asset is checkedout directly or in context of document

* BEFORE_DOWNLOAD_ASSET_ACTIO  
    Triggers before asset download action  
    **Event data:**  
    * assetIds: number[] - Asset Ids

    __Return__ false to cancel the event

* AFTER_DOWNLOAD_ASSET_ACTION  
    Triggers after asset download action  
    **Event data:**  
    * assetIds: number[] - Asset Ids
    * downloadUrl: string - Url to download asset

* BEFORE_DISCARD_ASSET_ACTION  
    Triggers before asset cancel checkout/discard action  
    **Event data:**  
    * entities: Entity[] - Array of Entity objects<br/>OR
    * assetInfo: Object - Asset Info

    __Return__ false to cancel the event

* AFTER_DISCARD_ASSET_ACTION  
    Triggers after asset cancel checkout/discard action  
    **Event data:**  
    * entities: Entity[] - Array of Entity objects
    * assetInfo: Object - Asset Info

* BEFORE_REINDEX_ASSET_ACTION  
    Triggers before asset redindex action  
    **Event data:**  
    * assetInfos: Object[] - Asset Infos

    __Return__ false to cancel the event

* AFTER_REINDEX_ASSET_ACTION  
    Triggers after asset redindex action  
    **Event data:**  
    * assetInfos: Object[] - Asset Infos

* BEFORE_DELETE_ASSET_ACTION  
    Triggers before asset delete action  
    **Event data:**  
    * entities: Entity[] - Array of Entity objects

    __Return__ false to cancel the event

* BEFORE_DELETE_ASSET_SUBMIT  
    Triggers before asset delete submit/done  
    **Event data:**  
    * entities: Entity[] - Array of Entity objects      

    __Return__ false to cancel the event

* AFTER_DELETE_ASSET_SUBMIT  
    Triggers after asset delete submit/done  
    **Event data:**  
    * entities: Entity[] - Array of Entity objects

* BEFORE_DELETE_COLLECTION_ACTION  
    Triggers before collection delete action  
    **Event data:**  
    * entity: Entity - Collection Entity object     

    __Return__ false to cancel the event

* BEFORE_DELETE_COLLECTION_SUBMIT  
    Triggers before collection delete submit/done  
    **Event data:**  
    * entity: Entity - Collection Entity object     

    __Return__ false to cancel the event

* AFTER_DELETE_COLLECTION_SUBMIT  
    Triggers after collection delete submit/done  
    **Event data:**  
    * entity: Entity - Collection Entity object


* BEFORE_MOVE_COLLECTION_ACTION  
    Triggers before collection move action  
    **Event data:**  
    * entities: Entity[] - Array of Entity objects      

    __Return__ false to cancel the event

* BEFORE_MOVE_COLLECTION_SUBMIT  
    Triggers before collection move submit/done  
    **Event data:**  
    * sourceCollectionIds: number[] - Array of source collection ids
    * targetParentCollectionId: number - Target collection id       
      
    __Return__ false to cancel the event

* AFTER_MOVE_COLLECTION_SUBMIT  
    Triggers after collection move submit/done  
    **Event data:**  
    * sourceCollectionIds: number[] - Array of source collection ids
    * targetParentCollectionId: number - Target collection id

* BEFORE_COPY_COLLECTION_ACTION  
    Triggers before collection copy action  
    **Event data:**  
    * entity: Entity - Collection Entity object  
      
    __Return__ false to cancel the event

* BEFORE_COPY_COLLECTION_SUBMIT  
    Triggers before collection copy submit/done  
    **Event data:**  
    * copyWithAssets: boolean - Copy with assets or not
    * sourceCollection: Entity - Source collection entity
    * newCollection: Object - Collection info like name.
        
    __Return__ { newCollection } to update payload or false to cancel the event

* AFTER_COPY_COLLECTION_SUBMIT  
    Triggers after collection copy submit/done  
    **Event data:**  
    * copyWithAssets: boolean - Copy with assets or not
    * sourceCollection: Entity - Source collection entity
    * newCollection: Object - Collection info like name.

* BEFORE_DUPLICATE_ASSET_ACTION  
    Triggers before asset duplicate action  
    **Event data:**  
    * entity: Entity - Asset Entity object
        
    __Return__ false to cancel the event

* AFTER_DUPLICATE_ASSET_ACTION  
    Triggers before asset duplicate action  
    **Event data:**  
    * entity: Entity - Asset Entity object
    * newAssetInfo: Object - Contains information about newly created asset

* BEFORE_DUPLICATE_COLLECTION_ACTION  
    Triggers before collection duplicate action  
    **Event data:**  
    * entity: Entity - Collection Entity object
        
    __Return__ false to cancel the event

* BEFORE_DUPLICATE_COLLECTION_SUBMIT  
    Triggers before collection duplicate submit/done  
    **Event data:**  
    * collection: Object - Collection to be created with inputs
        
    __Return__ { collection } to update payload or false to cancel the event

* AFTER_DUPLICATE_COLLECTION_SUBMIT  
    Triggers after collection duplicate submit/done  
    **Event data:**  
    * collection: Object - Data about Newly created collection
    
* BEFORE_EDIT_COLLECTION_PROPERTIES_ACTION  
    Triggers before edit collection properties action  
    **Event data:**  
    * collectionMeta: Object - Data about collection
        
    __Return__ false to cancel the event

* BEFORE_EDIT_COLLECTION_PROPERTIES_SUBMIT  
    Triggers before edit collection properties submit/done  
    **Event data:**  
    * attributes: Object[] - Array of collection attributes
        
    __Return__ { attributes } to update payload or false to cancel the event

* AFTER_EDIT_COLLECTION_PROPERTIES_SUBMIT  
    Triggers after edit collection properties submit/done  
    **Event data:**  
    * attributes: Object[] - Array of collection attributes

* BEFORE_EDIT_ASSET_PROPERTIES_ACTION  
    Triggers before edit asset properties action  
    **Event data:**  
    * assetMeta: Object - Data about asset - when invoked from asset detail <br/>OR
    * entities: Entities[] - Array of Entity objects

    __Return__ false to cancel the event

* BEFORE_ASSET_PROPERTIES_SUBMIT  
    Triggers before edit asset properties submit/done  
    **Event data:**  
    * entities: Entities[] - Array of Entity objects, only in case of multi select
    * attributes: Object[] - Array of asset attributes

    __Return__ { attributes } to update payload or false to cancel the event

* AFTER_ASSET_PROPERTIES_SUBMIT  
    Triggers after edit asset properties submit/done  
    **Event data:**  
    * entities: Entities[] - Array of Entity objects, only in case of multi select
    * attributes: Object[] - Array of asset attributes
    * successEntities: Entities[] - Array of Entity objects, only in case of multi select
    * failureEntities: Entities[] - Array of Entity objects, only in case of multi select

* BEFORE_WORKFLOW_ASSET_SUBMIT  
    Triggers before workflow asset submit/done  
    **Event data:**  
    * assetId: number - Asset Id
    * attributes: Object - Asset attributes

    __Return__ { attributes } to update payload or false to cancel the event

* AFTER_WORKFLOW_ASSET_SUBMIT  
    Triggers after workflow asset submit/done  
    **Event data:**  
    * assetId: number - Asset Id
    * attributes: Object - Asset attributes

* BEFORE_ASSET_UPLOAD_ACTION  
    Triggers on file upload action

    __Return__ false to cancel the event

* BEFORE_UPLOAD_FOLDER_ACTION  
    Triggers on folder upload action

    __Return__ false to cancel the event

* BEFORE_ASSET_PREVIEW_TAB_ACTION  
    Triggers on Preview tab select  
    **Event data:**  
    * assetMeta: Object - Data about asset

    __Return__ false to cancel the event

* BEFORE_ASSET_HISTORY_TAB_ACTION  
    Triggers on History tab select  
    **Event data:**  
    * assetMeta: Object - Data about asset

    __Return__ false to cancel the event

* BEFORE_ASSET_LINK_TAB_ACTION  
    Triggers on Links tab select  
    **Event data:**  
    * assetMeta: Object - Data about asset

    __Return__ false to cancel the event

* BEFORE_ASSET_PROPERTIES_TAB_ACTION  
    Triggers on Properties tab select  
    **Event data:**  
    * assetMeta: Object - Data about asset

    __Return__ false to cancel the event

* BEFORE_ASSET_AUDIT_TAB_ACTION  
    Triggers on Audit tab select  
    **Event data:**  
    * assetMeta: Object - Data about asset  

    __Return__ false to cancel the event

* BEFORE_WORKFLOW_DIALOG_OPEN  
    Triggers before workflow dialog is opened on uploading new file/folder.  
    **Event data:**  
    * attributes: Object - Data about asset
    * silentSubmit: Boolean - Default false - Set true to silently checkin the file.

    __Additional Option that can be set:__
    * attributes.assetAttributes: Array[Object]- Pass this when asset form attributes needs to be updated.

        Format -  { “attributeId”: 1XX, “attributeValue”: { “value”: “”}}

        Example for different type of attributes the value key will contain different value. For

        Domain – If multi type then array of domainValue else object the of the domainValue.  
        Text - It will contain the text string.  
        Date, DateTime, Time - It will contain date, datetime, time string respectively.  
        Boolean – It will contain true, false.  
        Number, Measurement – It will contain Int, double type of value.

    * attributes.isMinorVersion: Boolean - Default true –This option is used to create minor version or major version.
 
    __Return__ false to cancel the event

* BEFORE_READ_ONLY_ACTION   
    Triggers before read only action  
    **Event data:**  
    * assetId: number - Id of the asset
    * contentType: string - Content type of asset 
    * majorVersion: number - Major version of asset
    * minorVersion: number - Minor version of asset

    __Return__ false to cancel the event

* BEFORE_REVERT_TO_VERSION_ACTION  
    Triggers before revert to version action  
    **Event data:**  
    * assetId: number - Id of the asset
    * revertVersion: Object - Contains major and minor version of the revert version 
    * latestVersion: Object - Contains major and minor version of the current latest version 
    * createMinorVersion: boolean - whether to create major or minor version

    __Return__ { createMinorVersion } to update the payload false to cancel the event

* AFTER_REVERT_TO_VERSION_ACTION  
    Triggers after revert to version action  
    **Event data:**  
    * attributes: Object[] - attributes of new create version

## Configuration
You can configure OTB actions in Quark Publishing Platform NextGen through the
file **application-config.json** at the location application/conf in the **s3** bucket. If you
are doing the configuration for the first time, you may have to create the folder conf
and the file **application-config.json**  
<br/>The file **application-config.json** allows the following configurations:
* To create revision on property update:
    * createRevisionOnPropertyChange: **true/false** (default value is **true**)
* Tableau based audit tab for asset in asset detail view
    * showAssetAuditTab: **true/false** (default value is **false**)
    * assetAuditWordbook: Path of the Tableau worksheet (default value is “**views/CE-Audit-Workspace-Dashboard/Audit_Asset_Dashboard**”)
* To display all the links to an asset version: 
    * showAllLinksWithVersion: **true/false** (default value is **false**)
* To make workflow mandatory while checking in an asset:
    * workflowMandatory: **true/false** (default value is **false**)
* To enable the hide collection feature:
    * enableHideCollection: **true/false** (default value is **false**)
* To configure custom date format in Properties pane:
    * propertiesDateFormat: string (default value is '**M dd, yy**')
    * propertiesDateTimeSeparator: string (default value is '**,**')  

    Date format can have the following notations:
    ```
    d - day of month (no leading zero)  
    dd - day of month (two digits)  
    o - day of the year (no leading zeros)  
    oo - day of the year (three digits)  
    D - day name short  
    DD - day name long  
    m - month of year (no leading zero)  
    mm - month of year (two digits)  
    M - month name short  
    MM - month name long
    y - year (two digits)  
    yy - year (four digits)  
    @ - Unix timestamp (ms since 01/01/1970)  
    ! - Windows ticks (100ns since 01/01/0001)  
    '...' - literal text  
    '' - single quote  
    anything else - literal text  
    ```

* To disable check-in through drag and drop action:
    * disableCheckinDragAndDrop: **true/false** (defaukt value is **false**)




