#!/usr/bin/env bash
echo $PWD
currentdir=$PWD
echo $currentdir
for f in $PWD/quarkauthor/samples/Plugins/*; do
    if [ -d "$f" ]; then
        # Will not run if no directories are available
        echo "$f"
        cd "$f"
	zip -r sample.zip **
	cd ../
    fi
done
echo "currentdir=" $currentdir
for f in $currentdir/quarkauthor/samples/extensibility/*; do
    if [ -d "$f" ]; then
        # Will not run if no directories are available
        echo "$f"
        cd "$f"
        zip -r sample.zip **
        cd ../
    fi
done

echo "currentdir=" $currentdir
for f in $currentdir/workspace/samples/extensibility/*; do
    if [ -d "$f" ]; then
        # Will not run if no directories are available
        echo "$f"
        cd "$f"
        zip -r sample.zip **
        cd ../
    fi
done

echo "currentdir=" $currentdir
for f in $currentdir/workspace/samples/webapps/*; do
    if [ -d "$f" ]; then
        # Will not run if no directories are available
        echo "$f"
        cd "$f"
        zip -r sample.zip **
        cd ../
    fi
done
