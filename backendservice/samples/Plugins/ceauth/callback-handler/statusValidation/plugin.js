
CKEDITOR.plugins.add("statusValidation", {
	init: function(editor) {
	// Get author instance.
        var qaAuthor = QA.author;
        // Get callback manager instance from the author instance
        var qaCallbackManager = qaAuthor.qaCallbackManager;
        // Register a callback for beforeSubmit Event.
	    qaCallbackManager.registerCallback("beforeSubmit", (event) => {
            if (event) {
				// Find saveAssetVersion attribute, which is true in case of submit and 
				// false in case of done editing.
				var saveAssetVersion = event.saveAssetVersion;
                // Find the status attribute from event attributes.
                var status = event.status;
                if (status) {
				// For handling the beforeSubmit callback from submit action.
					if (saveAssetVersion === true){
	                    if (status.name === "Copy Edit") {
	                        const msg = "Status Copy Edit is an invalid selection.";
							// Show Error Dialog.
	                        const msgBox = qaAuthor.showDialog("qa-msg-box", {
	                            msgType: "error",
	                            title: "Error",
	                            msgText: msg
	                        });
	                        msgBox.on("close", () => {
	                            msgBox.close();
	                        });
							// return false in case of invalid status. 
	                        return false;
	                    }
					} else {
						// For handling the beforeSubmit callback from done editing action.
                		if (status.name === "Copy Edit") {
	                        const msg = "Status Copy Edit is an invalid selection.";
							// Show Error Dialog.
	                        const msgBox = qaAuthor.showDialog("qa-msg-box", {
	                            msgType: "error",
	                            title: "Error",
	                            msgText: msg
	                        });
	                        msgBox.on("close", () => {
	                            msgBox.close();
	                        });
							// return false in case of invalid status. 
	                        return false;
	                    }
						
					}
                }
            }
	    }, this);

		// After Submit callback handling.
		qaCallbackManager.registerCallback("afterSubmit", (event) => {
            if (event) {
				// Find saveAssetVersion attribute, which is true in case of submit action and 
				// false in case of done editing action.
				var saveAssetVersion = event.saveAssetVersion;
                // Find the status attribute from event.
                var status = event.status;
                if (status) {
				// For handling the afterSubmit callback from submit action.
					if (saveAssetVersion === true){				
	                    if (status.name === "Review") {
							// Return false in case of an invalid status.
							// save and close the document(similar to done editing action).
							qaAuthor.qaCore.closeDocument();
	                    }
		            } else {
						// For handling the afterSubmit callback from done editing action.
					}
				}
            }
		}, this);
	}
});