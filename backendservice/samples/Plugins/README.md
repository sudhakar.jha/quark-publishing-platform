## CE Auth - Refresh and Access tokens
### Authentication Flow
After successful login to CE, via the login page, a pair of tokens will be generated as credentials – an Access Token, and a Refresh Token. This is applicable for authentication via form based username/password and SAML based SSO. After successful authentication, the refresh token in set as cookie in the browser/http agent.

Acquiring Refresh Token:
After successful authentication, the refresh token is set as HTTP cookie named ‘ce-auth’. This cookie is expected and validated whenever request to get/renew access token is processed. Based on user’s choice of ‘Remember Me’ option in login page, this cookie is either set as a session cookie or a long lived persistent cookie. 



Acquiring Access token:
After authentication, the access token may be obtained by invoking following API

<b>GET /auth/accessToken</b> 
<br>
In response, following data is returned :
1. A JSON formatted object containing
        a. token: A JWT based access token
        b. duration: Duration for which the token is valid
        c. rememberMe : a flag to identify if the user had requested the option of ‘Remember Me” in authentication flow.
2. The access token is also set as a cookie named ‘pwt’ 

This access token is expected to be validated before access to any protected resource. To enable validation, based on the resource the token must be passed along request either as a HTTP cookie or header. 

### Renew Access token
Access token expiry and cookie expiry is rather short, in order of minutes ( currently defaults to 60 min.). To enable user to continue working without re-login, the access token must be renewed by invoking the same API as that used for acquiring the token. Clients and apps which need to frequently access CE resources should either

    • Periodically renew the access token
    • OR acquire an access token before every batch of requests to API.

The access token can be acquired without explicit need of authentication flow till the refresh token is valid. If the refresh token itself is invalid/expired, the request to get access token will fail with HTTP 401. Typically in response to such failure, the login page must be presented and authentication floe should be repeated.

### Get token as text
Following URL may be used to get the refresh token as text response. The token is presented as text after authentication. The token contains credentials of the authenticated user.

    • /auth/token

NOTE: The token returned is refresh token. It cannot be directly used to access resources but may be used to acquire access token.


### Access token format:
##### Following information is available in the access token :
```
{
// Unique ID to track tokens and their relations
    "uuid": string;
    // User ID as in Platform
    "id": string;
    // Type of token
    "type": “access” | “refresh”;
    // login name of user in Platform
    "userName": string;
    // CE license assigned to user.
    "licenseType": “author” | “access” | null;
    // The tenant ID
    "tenant": string;
    // version of token format. Value may be used to identify depcrecated or unsupported tokens.
    // 2.0 Version introduces access and refresh token pairs, uuid
    // 1.0 tokens are still valid as access tokens.
    "version"= “2.0”;
    // refresh UUId from which access token is generated
   "refreshTokenId?": string;
}
```
### Default Configuration: 

###### Refresh token default expiry : till browser open; 1 Day max.  
###### Refresh token expiry with Remember me option : 7 days 
###### Access token expiry : 1 Hr. 
