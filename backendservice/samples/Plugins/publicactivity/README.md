## DOCUMENTATION: HOW TO CREATE AND DEPLOY CUSTOM PUBLISHING ACTIVITY  (sample: deliver to S3) 

#### Pre-requisites: 

*  Java 8 installed, and maven installed. 

*  Have access to download quark maven artifacts. Update settings.xml in .m2 folder to register quark maven repository like: - 

        <server> 
         <id>qrk-visualstudio-com-qrk-contentplatform</id> 
         <username>contentplatform</username> 
         <password>XXXXXXX</password> 
        </server> 

*  Eclipse installed on the system. 

 

1. Open Eclipse and create new maven project. 
2. Add the dependencies required by your custom activity along with the following dependencies in pom.xml. 

    a. pom.xml
        <dependency> 
            <groupId>com.quark.qcep.contentservices</groupId> 
            <artifactId>qcep-contentservices-remoting-stubs</artifactId> 
            <version>1.0-SNAPSHOT</version> 
        </dependency> 
        <dependency> 
            <groupId>com.quark.qcep.automation</groupId> 
            <artifactId>qcep-automation-activities</artifactId> 
            <version>1.0-SNAPSHOT</version> 
        </dependency>
    b. Add any third-party dependencies required by your activity. 

3. Create a package (“com.quark.qcp.cas.publishing.activity.ext”) and add your activity.java file in it. 

4. Add your channel definition in ChannelConfig-ext.xml file which is present in ext folder inside s3 bucket of already running automation services). See the following example of ChannleConfig-ext.xml, for reference.  

 

  The following channel Sends the asset content to S3 bucket and particular folder. 

```
<channel id="sendToS3" name="Send to S3" publishingProcess="sendToS3" type="deliver"> 

            <description>Sends the content to S3</description> 

                  <param name="BUCKET_NAME" ask="true"/> 

            <param name="FOLDER_NAME" ask="true"/> 

            <param name="REGION_NAME" ask="true"/> 

            <param name="ACCESS_KEY" ask="true"/> 

            <param name="SECRET_KEY" ask="true"/> 

            <!-- <param name="PASSWORD" ask="true"/> 

                  <param name="OVERWRITE_EXISTING" ask="true">TRUE</param> 

                  <param name="NAME" ask="true"/> --> <!-- Name with which the desired  resource would be copied to the given S3 folder location--> 

    </channel> 

```
5. Create a file which will have your process definition. For example, see the following ‘SendToS3ProcessConfig.xml’. 

 
```
<?xml version="1.0" encoding="UTF-8"?> 

<beans xmlns="http://www.springframework.org/schema/beans" 

      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:context="http://www.springframework.org/schema/context" 

      xmlns:util="http://www.springframework.org/schema/util" xmlns:pub="http://www.quark.com/qpp/publishing/schema" 

      xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd 

  http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd  

  http://www.springframework.org/schema/util http://www.springframework.org/schema/util/spring-util.xsd 

  http://www.quark.com/qpp/publishing/schema http://www.quark.com/qpp/publishing/schema/publishingprocess.xsd"> 

 

      <!-- PROCESS DEFINITIONS --> 
      <pub:process id="sendToS3"> 
            <!-- A process to send file(s)/folder(s) to S3 location. --> 
            <pub:activity class="com.quark.qcp.cas.publishing.activity.SendToS3"> 
                  <description>An activity that sends (i.e. adds / updates) content to S3 at the specified location</description> 
                  <property name="activityName"> 
                        <value>SendToS3</value> 
                  </property> 
                  <property name="bucketName"> 
                        <value>PARAM:BUCKET_NAME</value> 
                  </property> 
                  <property name="folderName"> 
                        <value>PARAM:FOLDER_NAME</value> 
                  </property> 
                  <property name="regionName"> 
                        <value>PARAM:REGION_NAME</value> 
                  </property> 
                  <property name="accessKey"> 
                        <value>PARAM:ACCESS_KEY</value> 
                  </property> 
                  <property name="secretKey"> 
                        <value>PARAM:SECRET_KEY</value> 
                  </property> 
            </pub:activity> 
            <pub:activity-stream-mapper> 
                  <property name="activityName"> 
                        <value>SendToS3</value> 
                  </property> 
                  <property name="inputStreamNames"> 
                        <map> 
                              <entry key="SourceContent" value="RequestContent"/> 
                        </map> 
                  </property> 
            </pub:activity-stream-mapper> 
      </pub:process> 
</beans> 
```
 

6. Now add the ProcessDefinition in your classpath by adding import for the process definition file in ProcessConfig-ext.xml. This is also present in ext folder inside s3 bucket of already running automation services 

Example:  

    <import resource="classpath:SendToS3ProcessConfig.xml" /> 

 

7. Package this project using maven package command.  

    _D:\....\qcp-cas-custom-activity> mvn package_

                

This will create a jar file in the target directory inside the project’s source directory. 

<img src="../../../../assets/images/im1.png"/>

8. Copy the files creates/updated in step 4, 5, 6 and 7 in ext folder in s3 bucket of already running automation service. 

<img src="../../../../assets/images/im2.png"/>

9. Add all the third-party dependencies like aws-java-sdk-core-1.11.795.jar (including transitive dependencies) inside the same ext folder of S3 bucket of already running automation service as shown in above screenshot. 

 

10. Restart the automation services container: - 

        docker restart qcpautomationservice  

 
11. Verify whether the new channel has been deployed by executing following GET REST request: - 

 
        {{PROTOCOL}}://{{MACHINE_NAME}}:{{PORT}}/api/v1/publishingService/getChannel?channelId=sendToS3 
 

<img src="../../../../assets/images/im3.png"/>

12. Remote debugging of custom activity: - 

        a. You can connect to this updated automation services remotely for debugging by attaching to it at port 61411. Attached are the remote debug configurations.  

        b. Import the debug configurations and update the project to be debugged. 

 

13. This is how we can create and deploy custom activity/process/channel. In case there are issues while executing activity, fix the code and update the files associated in the ext folder. 

 
