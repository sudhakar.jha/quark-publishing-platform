/**
 * A sample plugin to demonstrate how to configure CKEditor plugin,  dialog and Quark Author APIs. 
 * This sample plugin launches dialog which conatins drop down with possible set of Tags and it will
 * apply selected Tag on current selection on pressing Dialog 'OK' button.
 */
CKEDITOR.plugins.add("customTags", {

    init: function (editor) {
        // A way to get path of plugin folder.
        const resourcePath = editor.engine.qaEditor.qaExtensibilityService.getResourcePath("customTags");
        //Register Dialog
        CKEDITOR.dialog.add("customTagDialog", `${resourcePath}/dialog/custom-tag-dialog.js`);
        // Register Ckeditor Button
        editor.ui.addButton("customTagBtn", {
            label: "Tags",
            icon: `${resourcePath}/icon.png`,
            command: "openTagDialog"
        });

        // Register Command
        editor.addCommand("openTagDialog", new CKEDITOR.dialogCommand("customTagDialog"));
    }

});