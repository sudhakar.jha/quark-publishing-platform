## DOCUMENTATION: HOW TO CREATE, RUN AND DEPLOY CUSTOM SERVICE 

### Pre-requisites 

1. Java 8 installed 

2. Maven installed 

3. Eclipse installed 


### App Creation 

1. Create a new spring boot app. 

2. Include spring-boot-starter-web and spring-boot-starter-security starter projects. 

3. Add the following necessary dependencies in pom.xml along with your third party dependencies. 

```
<dependency> 
    <groupId>io.jsonwebtoken</groupId> 
    <artifactId>jjwt</artifactId> 
    <version>0.7.0</version> 
</dependency> 
<dependency> 
    <groupId>com.quark.qpp</groupId> 
    <artifactId>qpp-content-services-remoting-stubs</artifactId> 
    <version>15.0-SNAPSHOT</version> 
</dependency> 
``` 

 

4. Add the attached files JwtAuthenticationProvider.java and SecurityConfig.java in any package scannable while building Spring application context. 

5. Add your Services and their implementation code as desired.  

### App Packaging 

#### We shall use Maven assembly plugin and maven-ant-plugin for building packaging 

1. Create a Build folder(in your workspace folder) and create a pom.xml in it. 

2. Create Assembly folder inside Build folder. Create pom.xml and assembly.xml files in it. 

3. Add the relative path to the app created above and Assembly folder in  “Build/pom.xml”.  Take reference from the content below. 

 
```
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd"> 
      <modelVersion>4.0.0</modelVersion> 
      <groupId>com.quark.qpp</groupId> 
      <artifactId>qpp-sample-service-main</artifactId> 
      <version>15.0-SNAPSHOT</version> 
      <packaging>pom</packaging> 
      <modules> 
            <module>../SampleService</module> 
            <module>Assembly</module> 
      </modules>   
</project> 

```

4. Configure assembly.xml 

 
```
<assembly 
      xmlns="http://maven.apache.org/plugins/maven-assembly-plugin/assembly/1.1.3" 
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
      xsi:schemaLocation="http://maven.apache.org/plugins/maven-assembly-plugin/assembly/1.1.3 http://maven.apache.org/xsd/assembly-1.1.3.xsd"> 
      <id>bundle</id> 
      <formats> 
            <format>dir</format> 
      </formats> 
      <includeBaseDirectory>false</includeBaseDirectory> 
      <fileSets> 
            <fileSet> 
                  <directory>../../SampleService</directory> 
                  <includes> 
                        <include>**/src/main/resources/*.*</include> 
                  </includes> 
                  <outputDirectory>tempconf</outputDirectory> 
            </fileSet> 
      </fileSets> 
      <dependencySets> 
            <dependencySet> 
                  <useProjectArtifact>false</useProjectArtifact> 
                  <excludes> 
                        <exclude>com.quark.qpp:*</exclude> 
                  </excludes> 
                  <outputDirectory>Build/dependencies</outputDirectory> 
            </dependencySet> 
            <dependencySet> 
                  <useProjectArtifact>false</useProjectArtifact> 
                  <includes> 
                        <include>com.quark.qpp:*</include> 
                  </includes> 
                  <outputDirectory>Build/lib</outputDirectory> 
            </dependencySet> 
      </dependencySets> 
</assembly> 
```
 

5. Configure <artifactId>maven-assembly-plugin</artifactId> and <artifactId>maven-antrun-plugin</artifactId> In Plugins section of “Build/Assembly/pom.xml”. Add the sample app created as dependency.  

```
      <dependencies> 
            <dependency> 
                  <groupId>com.quark.qpp</groupId> 
                  <artifactId>qpp-sample-service</artifactId> 
                  <version>15.0-SNAPSHOT</version> 
            </dependency> 
      </dependencies> 
      <build> 
            <plugins> 
                  <plugin> 
                        <artifactId>maven-assembly-plugin</artifactId> 
                        <version>3.1.1</version> 
                        <executions> 
                              <execution> 
                                    <phase>package</phase> 
                                    <configuration> 
                                          <descriptors> 
                                                <descriptor>assembly.xml</descriptor> 
                                          </descriptors> 
                                    </configuration> 
                                    <goals> 
                                          <goal>single</goal> 
                                    </goals> 
                              </execution> 
                        </executions> 
                  </plugin> 
                  <plugin> 
                        <artifactId>maven-antrun-plugin</artifactId> 
                        <executions> 
                              <execution> 
                                    <phase>install</phase> 
                                    <configuration> 
                                          <tasks> 
                                                <copy todir="./target/${project.artifactId}-${project.version}-bundle/Build/conf" flatten="true"> 
<fileset dir="./target/${project.artifactId}-${project.version}-bundle/tempconf" includes="**/src/main/resources/*.*" /> 
                                                </copy>   
                                                <delete      dir="./target/${project.artifactId}-${project.version}-bundle/tempconf" /> 
                                                <copy todir="${ShuttleFolder}"> 
                                                      <fileset dir="./target/${project.artifactId}-${project.version}-bundle" includes="**" /> 
                                                </copy> 
                                                <copy todir="${ShuttleFolder}/Build"> 
                                                      <fileset dir="../" includes="Start.bat"/> 
                                                </copy> 
                                                <copy todir="${ShuttleFolder}/Build"> 
                                                      <fileset dir="../" includes="Start.sh"/> 
                                                </copy> 
                                          </tasks> 
                                    </configuration> 
                                    <goals> 
                                          <goal>run</goal> 
                                    </goals> 
                              </execution> 
                        </executions> 
                  </plugin> 
            </plugins> 
      </build> 
```

6. Run “mvn clean install”  in command prompt from the Build folder. 

7. Now copy the ${ShuttleFolder} to a linux ubuntu machine having docker installed. 


### Creating Docker Image 

1. Create a file with name ‘dockerfile’ parallel to the Build folder  where the contents of ${ShuttleFolder} Were copied on linux ubuntu machine. Add the following content in it. 

 
```
#Dockerfile for Sample Services 

#Indicates that custom base image is being used, this image has Java(TM) SE Runtime Environment (build 1.8.0_201-b09) and imagemagick pre installed. 

FROM amazoncorretto:8 

#Set the Environment variables 

ENV \ 

content.platform.services.url=qcpcontentservice:61400 


#Copy server binaries to sever folder in container 

COPY . server 

#Open ports needed in container, this does not map them to host machine 

EXPOSE 8080 

#Install bash, needed for ease of use in case any error in container needs to be debugged 

RUN apk add --update bash 

RUN yum -y install procps 

#Change permissions for Start.sh 

RUN chmod 777 /server/Build/Start.sh 

#Start server with docker container, the default command that will be executed 

#can be overridden with docker run command 

CMD ["bash", "-c", "./server/Build/Start.sh"] 

 ```

 

 

2. Create the docker image by executing the command inside the folder where dockerfile resides: 

        docker build -t <name/tag of the image> . 

        eg. docker build -t qcpsampleservice . 

 

3. Check if the image is successfully listed in docker images list by executing: 

        docker images | grep -i qcpsampleservice 

 

### Docker image execution 

1. Create a YAML file. 

        sudo nano docker-compose.yml 

2. Paste the following contents and save the file (with the same indentation). 

```
qcpsampleservice: 

    image: qcpsampleservice:latest 

    container_name : qcpsampleservice 

    environment: 

            - content.platform.services.url=https://rivacorp.app.quark.com/ 

    restart: always 

    ports: 

      - "8080:8080"  
```

3. Now you can execute the following command to run the image from the same folder as docker-compose.yml . (-d option will run the container in background) 

        docker-compose up -d 
